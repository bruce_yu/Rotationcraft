package cn.tohsaka.factory.rotationcraft.init;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundEntity;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public enum Sounds {
    JETSTART("jetstart"),
    JETAFTERBURNER("afterburner"),
    JETRUNNING("jetengine"),
    MICROENGINE("microengine"),
    GASENGINE("gasengine"),
    HYDROENGINE("hydroengine"),
    STEAMENGINE("steamengine"),

    CRAFT("craft");








    public String soundid;
    Sounds(String id){
        soundid = id;
    }
    private static HashMap<Sounds,SoundEvent> sounds = new HashMap<>();
    public static void init(){
        for(Sounds id:Sounds.values()){
            sounds.put(id,registerSoundEvent(id.soundid));
        }
    }

    private static SoundEvent registerSoundEvent(String id) {
        ResourceLocation loc = new ResourceLocation(RotationCraft.MOD_ID + ":" + id);
        SoundEvent sound = new SoundEvent(loc).setRegistryName(loc);
        ForgeRegistries.SOUND_EVENTS.register(sound);
        return sound;
    }

    public SoundEvent getEvent(){
        return sounds.get(this);
    }
    public SoundEntity getEntity(boolean repeat){
        return new SoundEntity(sounds.get(this),repeat);
    }
}
