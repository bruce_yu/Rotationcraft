package cn.tohsaka.factory.rotationcraft.config;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import net.minecraftforge.common.config.Config;

@Config(modid = RotationCraft.MOD_ID, type = Config.Type.INSTANCE, name = "Rotationcraft",category = "general")
public class GeneralConfig {
    public static boolean book_nocache = true;
    public static boolean buildoptimizer = false;
    public static boolean turbin_nodelay = false;
    public static boolean jetfail_terrainbreak = true;
    public static int wailapacketrate = 20;
    public static float FE_converter_ratio = 520f;
    public static int BEMINER_MAXOFFSET = 0;
    public static int ENET_TICKRATE = 20;
    public static float STEAM_CONVERT_RATIO = 1.5f;
    public static int boiler_explode_level = 8;
}
