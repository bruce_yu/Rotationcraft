package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

public class RenderUtils {
    public static final RenderUtils INSTANCE = new RenderUtils();
    public CopyOnWriteArraySet<IOutBoxDrawable> items = new CopyOnWriteArraySet<>();
    public void renderBlockInWorld() {
        try {

            if (items.size()>0)
            {
                GlStateManager.pushMatrix();
                GL11.glPushAttrib(GL11.GL_LIGHTING_BIT);
                GlStateManager.disableDepth();
                GlStateManager.disableTexture2D();
                GlStateManager.enableBlend();
                NonNullList<RenderItem> drawables = NonNullList.create();
                Iterator<IOutBoxDrawable> it = items.iterator();
                while (it.hasNext()){
                    IOutBoxDrawable drawable = it.next();
                    if(drawable instanceof TileEntity && ((TileEntity)drawable).hasWorld() && !((TileEntity)drawable).isInvalid()){
                        drawable.getOutBoxList(drawables);
                    }
                }
                for(RenderItem item : drawables){
                    if(item instanceof RenderItemBigBox){
                        renderBigboxOutlineAt(((RenderItemBigBox)item).bigBox, item.renderColor,item.thick);
                        //renderBlockOutlineAt(new BlockPos(((RenderItemBigBox)item).bigBox.minX,((RenderItemBigBox)item).bigBox.minY,((RenderItemBigBox)item).bigBox.minZ),item.renderColor,item.thick);
                    }else {
                        renderBlockOutlineAt(item.renderPos, item.renderColor,item.thick);
                    }
                }
                GlStateManager.enableDepth();
                GlStateManager.enableTexture2D();
                GlStateManager.disableBlend();
                GL11.glPopAttrib();
                GlStateManager.popMatrix();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private static void renderBigboxOutlineAt(AxisAlignedBB aabb, int color, float thickness) {
        double renderPosX = Minecraft.getMinecraft().getRenderManager().viewerPosX;
        double renderPosY = Minecraft.getMinecraft().getRenderManager().viewerPosY;
        double renderPosZ = Minecraft.getMinecraft().getRenderManager().viewerPosZ;

        GlStateManager.pushMatrix();
        GlStateManager.translate(aabb.minX - renderPosX, aabb.minY - renderPosY, aabb.minZ - renderPosZ + 1);
        Color colorRGB = new Color(color);
        GL11.glColor4ub((byte) colorRGB.getRed(), (byte) colorRGB.getGreen(), (byte) colorRGB.getBlue(), (byte) 200);

        World world = Minecraft.getMinecraft().world;

        drawWireframe : {
                AxisAlignedBB axis = aabb;

                if(axis == null)
                    break drawWireframe;

                axis = axis.offset(-aabb.minX, -aabb.minY, -(aabb.minZ + 1));

                GlStateManager.scale(1F, 1F, 1F);

                GL11.glLineWidth(thickness);
                renderBlockOutline(axis);

                GL11.glLineWidth(thickness + 3F);
                GL11.glColor4ub((byte) colorRGB.getRed(), (byte) colorRGB.getGreen(), (byte) colorRGB.getBlue(), (byte) 64);
                renderBlockOutline(axis);

        }

        GL11.glColor4ub((byte) 255, (byte) 255, (byte) 255, (byte) 255);
        GlStateManager.popMatrix();
    }


    private static void renderBlockOutlineAt(BlockPos pos, int color, float thickness) {
        double renderPosX = Minecraft.getMinecraft().getRenderManager().viewerPosX;
        double renderPosY = Minecraft.getMinecraft().getRenderManager().viewerPosY;
        double renderPosZ = Minecraft.getMinecraft().getRenderManager().viewerPosZ;

        GlStateManager.pushMatrix();
        GlStateManager.translate(pos.getX() - renderPosX, pos.getY() - renderPosY, pos.getZ() - renderPosZ + 1);
        Color colorRGB = new Color(color);
        GL11.glColor4ub((byte) colorRGB.getRed(), (byte) colorRGB.getGreen(), (byte) colorRGB.getBlue(), (byte) 200);

        World world = Minecraft.getMinecraft().world;
        IBlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        drawWireframe : {
            if(block != null) {
                AxisAlignedBB axis = state.getSelectedBoundingBox(world, pos);

                if(axis == null)
                    break drawWireframe;

                axis = axis.offset(-pos.getX(), -pos.getY(), -(pos.getZ() + 1));

                GlStateManager.scale(1F, 1F, 1F);

                GL11.glLineWidth(thickness);
                renderBlockOutline(axis);

                GL11.glLineWidth(thickness + 3F);
                GL11.glColor4ub((byte) colorRGB.getRed(), (byte) colorRGB.getGreen(), (byte) colorRGB.getBlue(), (byte) 64);
                renderBlockOutline(axis);
            }
        }

        GL11.glColor4ub((byte) 255, (byte) 255, (byte) 255, (byte) 255);
        GlStateManager.popMatrix();
    }

    private static void renderBlockOutline(AxisAlignedBB aabb) {
        Tessellator tessellator = Tessellator.getInstance();

        double ix = aabb.minX;
        double iy = aabb.minY;
        double iz = aabb.minZ;
        double ax = aabb.maxX;
        double ay = aabb.maxY;
        double az = aabb.maxZ;
        tessellator.getBuffer().begin(GL11.GL_LINES, DefaultVertexFormats.POSITION);
        tessellator.getBuffer().pos(ix, iy, iz).endVertex();
        tessellator.getBuffer().pos(ix, ay, iz).endVertex();

        tessellator.getBuffer().pos(ix, ay, iz).endVertex();
        tessellator.getBuffer().pos(ax, ay, iz).endVertex();

        tessellator.getBuffer().pos(ax, ay, iz).endVertex();
        tessellator.getBuffer().pos(ax, iy, iz).endVertex();

        tessellator.getBuffer().pos(ax, iy, iz).endVertex();
        tessellator.getBuffer().pos(ix, iy, iz).endVertex();

        tessellator.getBuffer().pos(ix, iy, az).endVertex();
        tessellator.getBuffer().pos(ix, ay, az).endVertex();

        tessellator.getBuffer().pos(ix, iy, az).endVertex();
        tessellator.getBuffer().pos(ax, iy, az).endVertex();

        tessellator.getBuffer().pos(ax, iy, az).endVertex();
        tessellator.getBuffer().pos(ax, ay, az).endVertex();

        tessellator.getBuffer().pos(ix, ay, az).endVertex();
        tessellator.getBuffer().pos(ax, ay, az).endVertex();

        tessellator.getBuffer().pos(ix, iy, iz).endVertex();
        tessellator.getBuffer().pos(ix, iy, az).endVertex();

        tessellator.getBuffer().pos(ix, ay, iz).endVertex();
        tessellator.getBuffer().pos(ix, ay, az).endVertex();

        tessellator.getBuffer().pos(ax, iy, iz).endVertex();
        tessellator.getBuffer().pos(ax, iy, az).endVertex();

        tessellator.getBuffer().pos(ax, ay, iz).endVertex();
        tessellator.getBuffer().pos(ax, ay, az).endVertex();

        tessellator.draw();
    }

    public static class RenderItem{
        int renderColor;
        BlockPos renderPos;
        float thick;
        public RenderItem(int color,BlockPos pos,float th){
            renderColor = color;
            renderPos=pos;
            thick=th;
        }
    }

    public static class RenderItemBigBox extends RenderItem{
        public AxisAlignedBB bigBox;
        public RenderItemBigBox(int color,AxisAlignedBB axisAlignedBB,float th){
            super(color,null,th);
            bigBox = axisAlignedBB;
        }
    }

    @SubscribeEvent
    public void onWorldRenderLast(RenderWorldLastEvent event) {
        Minecraft mc = Minecraft.getMinecraft();
        if(mc.player != null && items.size()>0) {
            renderBlockInWorld();
        }
    }
    public static void addRender(IOutBoxDrawable drawable) {
        INSTANCE.items.add(drawable);
    }
    public static void removeRender(IOutBoxDrawable drawable) {
        INSTANCE.items.remove(drawable);
    }
    public static boolean containsRender(IOutBoxDrawable drawable) {
        return INSTANCE.items.contains(drawable);
    }



    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onPlayerJoin(EntityJoinWorldEvent event){
        if(event.getEntity() instanceof EntityPlayer && event.getEntity() == Minecraft.getMinecraft().player) {
            RenderUtils.INSTANCE.items.clear();
        }
    }

    public static interface IOutBoxDrawable{
        void getOutBoxList(NonNullList<RenderItem> drawables);
    }



    public static void rotate(EnumFacing facing, float north, float south, float west, float east) {
        switch (facing) {
            case NORTH:
                GlStateManager.rotate(north, 0, 1, 0);
                break;
            case SOUTH:
                GlStateManager.rotate(south, 0, 1, 0);
                break;
            case WEST:
                GlStateManager.rotate(west, 0, 1, 0);
                break;
            case EAST:
                GlStateManager.rotate(east, 0, 1, 0);
                break;
        }
    }
}
