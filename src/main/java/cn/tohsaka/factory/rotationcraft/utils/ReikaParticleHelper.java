/*******************************************************************************
 * @author Reika Kalseki
 * 
 * Copyright 2017
 * 
 * All rights reserved.
 * Distribution of the software in any form is only allowed with
 * explicit, prior permission from the owner.
 ******************************************************************************/
package cn.tohsaka.factory.rotationcraft.utils;

import java.awt.*;
import java.util.HashMap;
import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;


public class ReikaParticleHelper {


	private static final Random rand = new Random();
	private static final HashMap<String, ReikaParticleHelper> names = new HashMap();


	public static ReikaParticleHelper getByString(String name) {
		return names.get(name);
	}

	public void spawnAt(EnumParticleTypes type,Entity e) {
		spawnAt(type,e.world, e.posX, e.posY, e.posZ);
	}

	public void spawnAround(EnumParticleTypes type,Entity e, int n, double r) {
		for (int i = 0; i < n; i++) {
			double dx = getRandomPlusMinus(e.posX, r);
			double dy = getRandomPlusMinus(e.posY, r);
			double dz = getRandomPlusMinus(e.posZ, r);
			spawnAt(type,e.world, dx, dy, dz);
		}
	}
	private static Random r = new Random();
	public static double getRandomPlusMinus(double base, double range) {
		double add = -range+r.nextDouble()*range*2;
		return (base+add);
	}

	public static void spawnAt(EnumParticleTypes type,World world, double x, double y, double z, double vx, double vy, double vz) {
		world.spawnParticle(type, x, y, z, vx, vy, vz);
	}

	public static void spawnAt(EnumParticleTypes type,World world, double x, double y, double z) {
		spawnAt(type,world, x, y, z, 0, 0, 0);
	}

	public static void spawnAroundBlock(EnumParticleTypes type,World world, int x, int y, int z, int number) {
		spawnAroundBlock(type,world, x, y, z, 0, 0, 0, number);
	}

	public static void spawnAroundBlockWithOutset(EnumParticleTypes type,World world, int x, int y, int z, int number, double outset) {
		spawnAroundBlockWithOutset(type,world, x, y, z, 0, 0, 0, number, outset);
	}

	public static void spawnAroundBlock(EnumParticleTypes type,World world, int x, int y, int z, double vx, double vy, double vz, int number) {
		for (int i = 0; i < number; i++) {
			world.spawnParticle(type, x+rand.nextDouble(), y+rand.nextDouble(), z+rand.nextDouble(), vx, vy, vz);
		}
	}

	public static void spawnAroundBlockWithOutset(EnumParticleTypes type,World world, int x, int y, int z, double vx, double vy, double vz, int number, double outset) {
		for (int i = 0; i < number; i++) {
			double rx = getRandomPlusMinus(x+0.5, 0.5+outset);
			double ry = getRandomPlusMinus(y+0.5, 0.5+outset);
			double rz = getRandomPlusMinus(z+0.5, 0.5+outset);
			world.spawnParticle(type, rx, ry, rz, vx, vy, vz);
		}
	}

	public static void spawnColoredParticles(World world, int x, int y, int z, Color color, int number) {
		spawnAroundBlock(EnumParticleTypes.REDSTONE,world, x, y, z, color.getRed(), color.getGreen(), color.getBlue(), number);
	}

	public static void spawnColoredParticles(World world, int x, int y, int z, double r, double g, double b, int number) {
		spawnAroundBlock(EnumParticleTypes.REDSTONE,world, x, y, z, r, g, b, number);
	}

	public static void spawnColoredParticlesWithOutset(World world, int x, int y, int z, double r, double g, double b, int number, double outset) {
		spawnAroundBlockWithOutset(EnumParticleTypes.REDSTONE,world, x, y, z, r, g, b, number, outset);
	}

	public static void spawnColoredParticleAt(World world, double x, double y, double z, int color) {
		Color c = new Color(color);
		spawnAt(EnumParticleTypes.REDSTONE,world, x, y, z, c.getRed(), c.getGreen(), c.getBlue());
	}

	public static void spawnColoredParticleAt(World world, double x, double y, double z, double r, double g, double b) {
		spawnAt(EnumParticleTypes.REDSTONE,world, x, y, z, r, g, b);
	}


}