package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;

public class ModUtils {
    public static boolean isModLoaded(String modName){
        return Loader.isModLoaded(modName);
    }

    public static boolean isServerSide(){
        return FMLCommonHandler.instance().getSide() == Side.SERVER;
    }
}
