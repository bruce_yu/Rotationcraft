package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;

public class NBTUtils {
    public static String toString(ItemStack itemStack) {
        NBTTagCompound tagCompound = itemStack.getTagCompound();
        if(tagCompound==null){
            return null;
        }
        return tagCompound.toString();
    }
    public static NBTTagCompound toTagCompound (String tag) {
        try {
            return JsonToNBT.getTagFromJson(tag);
        } catch (NBTException e) {
            e.printStackTrace();
        }
        return null;
    }
}
