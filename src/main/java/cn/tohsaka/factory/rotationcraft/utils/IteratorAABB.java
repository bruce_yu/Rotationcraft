package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

import java.util.Iterator;

public class IteratorAABB implements Iterator<BlockPos> {
    AxisAlignedBB aabb;
    int maxcount;
    BlockPos start;
    int index = 0;
    int cX;
    int cY;
    int cZ;
    boolean t = false;
    public IteratorAABB(BlockPos s,int aX,int aY,int aZ){
        maxcount = (aX*aZ*aY);
        start = s;
        cX=aX;
        cY=aY;
        cZ=aZ;
    }

    @Override
    public boolean hasNext() {
        return index<maxcount;
    }

    @Override
    public BlockPos next() {
        BlockPos pos = start.add((int)((index / cY) % cX),(int)(index % cY),(int)(((index / cY) / cX) % cZ));
        index++;
        return pos;
    }
    public int getCount(){
        return maxcount;
    }
    public BlockPos get(int i){
        return start.add((int)(i%cX),(int)(((i / cX) / cZ) % cY),(int)((i / cX) % cZ));
    }

    public NBTTagCompound toNBTTagCompound(){
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("x",start.getX());
        tag.setInteger("y",start.getY());
        tag.setInteger("z",start.getZ());

        tag.setInteger("ax",cX);
        tag.setInteger("ay",cY);
        tag.setInteger("az",cZ);
        return tag;
    }
    public static IteratorAABB readFromNBT(NBTTagCompound tag){
        return new IteratorAABB(new BlockPos(tag.getInteger("x"),tag.getInteger("y"),tag.getInteger("z")),tag.getInteger("ax"),tag.getInteger("ay"),tag.getInteger("az"));
    }
    public boolean contains(BlockPos pos){
        return new AxisAlignedBB(start,start.add(cX,cY,cZ)).contains(new Vec3d(pos));
    }
    public AxisAlignedBB getAabb(){
        return new AxisAlignedBB(start,start.add(cX,cY,cZ));
    }
    public AxisAlignedBB getAabb(EnumFacing facing, FacingTool.Position position,int offset){
        return new AxisAlignedBB(FacingTool.getRelativePos(start,facing,position,offset),FacingTool.getRelativePos(start.add(cX,cY,cZ),facing,position,offset));
    }
}
