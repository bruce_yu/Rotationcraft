package cn.tohsaka.factory.rotationcraft.utils;

public class AxlsUtils {
    public static float calcAngle(float angle,long speed){
        double pow = 1.05;
        double mult = 1;
        float a = (float) (angle + ReikaMathLibrary.doubpow(ReikaMathLibrary.logbase((int)(mult*Math.min(speed,524288)+1), 2), pow));
        if(a>3600){
            a-=3600;
        }
        return a;
    }
}
