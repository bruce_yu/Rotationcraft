package cn.tohsaka.factory.rotationcraft.worldgen;


import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.LinkedList;
import java.util.Random;

public class RcWorldGen implements IWorldGenerator {
    private static LinkedList<OreGenDefinition> ores = new LinkedList<OreGenDefinition>();;


    public static void add(OreGenDefinition def){
        ores.add(def);
    }


    private void addOreSpawn(World world, Random random, int xPos, int zPos, int maxX, int maxZ, OreGenDefinition def){
        for(int i = 0; i < def.spawnChance; i++){
            int x = xPos + random.nextInt(maxX);
            int yRange=def.maxY - def.minY;
            int y = def.minY + random.nextInt((yRange > 0)?yRange:1);
            int z = zPos + random.nextInt(maxZ);
            WorldGenMinable minable = (new WorldGenMinable(def.ore.getDefaultState(), Math.max(3,random.nextInt(def.veinSize))));
            minable.generate(world, random, new BlockPos(x, y, z));
        }
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (world.provider.getDimension() == 0) {
            for(OreGenDefinition d : ores) {
                addOreSpawn(world, random, chunkX * 16, chunkZ * 16, 16, 16, d);
            }
        }
    }
}