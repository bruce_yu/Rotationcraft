package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.extractor;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.ExtractorRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileExtractor;
import mezz.jei.api.recipe.IStackHelper;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ExtractorRecipeMaker {
    private ExtractorRecipeMaker() {
    }

    public static List<ExtractorRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<ExtractorRecipeWrapper> recipes = new ArrayList();
        for(CraftingPattern s : Recipes.INSTANCE.getList(TileExtractor.class)) {
            if(s!=null && s instanceof ExtractorRecipe && !((ExtractorRecipe)s).test(ItemStack.EMPTY)){
                addWrapperToList(stackHelper,s,recipes);
            }
        }
        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, CraftingPattern recipe, List<ExtractorRecipeWrapper> recipes) {
        recipes.add(new ExtractorRecipeWrapper(recipe));
    }
}
