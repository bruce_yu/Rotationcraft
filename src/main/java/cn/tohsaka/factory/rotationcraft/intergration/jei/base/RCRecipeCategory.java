//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.base;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.recipe.IRecipeCategory;
import mezz.jei.api.recipe.IRecipeWrapper;
import mezz.jei.util.Translator;

public abstract class RCRecipeCategory<T extends IRecipeWrapper> implements IRecipeCategory<T> {
    private final IDrawable background;
    private final String localizedName;

    public RCRecipeCategory(IDrawable background, String unlocalizedName) {
        this.background = background;
        this.localizedName = Translator.translateToLocal(unlocalizedName);
    }

    public String getTitle() {
        return this.localizedName;
    }

    public String getModName() {
        return RotationCraft.MOD_NAME;
    }

    public IDrawable getBackground() {
        return this.background;
    }
}
