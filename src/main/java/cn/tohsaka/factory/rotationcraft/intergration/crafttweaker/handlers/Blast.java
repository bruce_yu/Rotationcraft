package cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.handlers;

import cn.tohsaka.factory.rotationcraft.crafting.BlastRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.crafting.scripts.BlastRecipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBlast;
import crafttweaker.annotations.ModOnly;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.Arrays;

import static cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.InputHelper.toStack;
import static cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.InputHelper.toStacks;

@ZenClass("mods.rotationcraft.blast")
@ModOnly("rc")
@ZenRegister
public class Blast {
    @ZenMethod
    public static void addRecipe(IItemStack output,IItemStack[] input,IItemStack[] catalyst,int temp) {
        if(catalyst==null || catalyst.length==0){
            throw new IllegalArgumentException("catalyst must not be null");
        }
        if(catalyst.length>3){
            throw new IllegalArgumentException("catalyst maxium length must be 3");
        }
        if(input.length>9){
            throw new IllegalArgumentException("input maxium length must be 3");
        }

        ItemStack[] ii = new ItemStack[9];
        Arrays.fill(ii,ItemStack.EMPTY);
        for(int i=0;i<input.length;i++){
            ii[i] = toStack(input[i]);
        }

        BlastRecipe recipe = new BlastRecipe(toStacks(catalyst),ii,toStack(output),temp);
        Recipes.INSTANCE.addPattern(TileBlast.class,recipe);
    }

    @ZenMethod
    public static void addShapelessRecipe(IItemStack output,IItemStack input,IItemStack[] catalyst,float bound,int temp) {
        if(catalyst==null || catalyst.length==0){
            throw new IllegalArgumentException("catalyst must not be null");
        }
        if(catalyst.length>3){
            throw new IllegalArgumentException("catalyst maxium length must be 3");
        }
        BlastRecipe recipe = new BlastRecipe(toStacks(catalyst),toStack(input),toStack(output),bound,temp);
        Recipes.INSTANCE.addPattern(TileBlast.class,recipe);
    }

    @ZenMethod
    public static void delRecipe(IItemStack output) {
        BlastRecipes.stacksToRemove.add(toStack(output));
    }
}
