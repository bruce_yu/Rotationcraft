//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.grinder;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.GrinderRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileGrinder;
import mezz.jei.api.recipe.IStackHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GrinderRecipeMaker {
    private GrinderRecipeMaker() {
    }

    public static List<GrinderRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<GrinderRecipeWrapper> recipes = new ArrayList();
        Iterator var2 = Recipes.INSTANCE.getList(TileGrinder.class).iterator();

        while(var2.hasNext()) {
            CraftingPattern recipe = (CraftingPattern)var2.next();
            addWrapperToList(stackHelper, recipe, recipes);
        }

        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, CraftingPattern recipe, List<GrinderRecipeWrapper> recipes) {
        recipes.add(new GrinderRecipeWrapper(recipe));
    }
}
