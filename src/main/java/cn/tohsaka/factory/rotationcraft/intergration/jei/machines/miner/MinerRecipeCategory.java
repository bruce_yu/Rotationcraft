//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.miner;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockVoidMiner;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class MinerRecipeCategory extends RCRecipeCategory<MinerRecipeWrapper>{
    public static String tex_path = "textures/gui/minerjei.png";
    public static String gui_tex = RotationCraft.MOD_ID+":"+tex_path;
    private static final ResourceLocation guiTexture = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    public MinerRecipeCategory(IGuiHelper guiHelper) {
        super(guiHelper.createDrawable(guiTexture, 0, 0, 106, 64), RotationCraft.blocks.get(BlockVoidMiner.NAME).getUnlocalizedName()+".name");

    }

    public String getUid() {
        return RCRecipeCategoryUid.MINER;
    }

    public void drawExtras(Minecraft minecraft) {

    }

    public void setRecipe(IRecipeLayout recipeLayout, MinerRecipeWrapper recipeWrapper, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, false, 72, 23);
        guiItemStacks.init(1,true,14,23);
        guiItemStacks.set(ingredients);
        guiItemStacks.set(1,new ItemStack(RotationCraft.blocks.get(BlockVoidMiner.NAME)));
    }

}
