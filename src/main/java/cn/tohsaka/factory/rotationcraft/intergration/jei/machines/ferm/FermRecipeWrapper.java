//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.ferm;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class FermRecipeWrapper extends RCRecipeWrapper<CraftingPattern> {
    public static HashSet<FermRecipeWrapper> recipes = new HashSet<>();
    public FermRecipeWrapper(CraftingPattern recipe) {
        super(recipe);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        List<List<ItemStack>> input = new ArrayList<>();
        for(String item : this.getRecipe().getInput()){
            input.add(Arrays.asList(CraftingPattern.parseOreDict(item)));
        }
        NBTTagCompound tag = this.getRecipe().getData();
        if(CraftingPattern.hasFluids(tag)>0){
            ingredients.setInput(VanillaTypes.FLUID,CraftingPattern.getFluid(tag)[0]);
        }
        ingredients.setInputLists(VanillaTypes.ITEM, input);
        ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(new ItemStack[]{this.getRecipe().getOutput()}));
    }

    @Override
    public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY) {
        if(this.getRecipe().getData().hasKey("redstone")){
            RenderHelper.disableStandardItemLighting();
            RenderHelper.enableGUIStandardItemLighting();
            minecraft.getRenderItem().renderItemIntoGUI(new ItemStack(Blocks.REDSTONE_BLOCK),150,2);
        }
    }

    @Override
    public List<String> getTooltipStrings(int mouseX, int mouseY) {
        if(mouseX>=150 && mouseY >=2 && mouseX<165 && mouseY<19 && this.getRecipe().getData().hasKey("redstone")){
            return Arrays.asList(StringHelper.localize("info.rc.requirers"));
        }
        return super.getTooltipStrings(mouseX, mouseY);
    }
}
