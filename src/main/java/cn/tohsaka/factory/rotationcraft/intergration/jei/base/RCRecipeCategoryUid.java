//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.base;

public class RCRecipeCategoryUid {
    public static final String FERM = "rc.recipes.ferm";
    public static final String WORKTABLE = "rc.recipes.worktable";
    public static final String GRINDER = "rc.recipes.grinder";
    public static final String BLAST = "rc.recipes.blast";
    public static final String PULSE = "rc.recipes.pulse";
    public static final String MINER = "rc.recipes.miner";
    public static final String EXTRACTOR = "rc.recipes.extractor";
    public static final String MAGNETIZER = "rc.recipes.magnetizer";

    public RCRecipeCategoryUid() {
    }
}
