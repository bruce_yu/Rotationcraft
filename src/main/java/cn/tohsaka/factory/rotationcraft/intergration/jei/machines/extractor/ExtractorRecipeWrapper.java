//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.extractor;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.ExtractorRecipe;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;

import java.util.Arrays;
import java.util.HashSet;

public class ExtractorRecipeWrapper extends RCRecipeWrapper<CraftingPattern> {
    public static HashSet<ExtractorRecipeWrapper> recipes = new HashSet<>();
    public ExtractorRecipeWrapper(CraftingPattern stack) {
        super(stack);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        ingredients.setInputs(VanillaTypes.ITEM, Arrays.asList(((ExtractorRecipe)this.getRecipe()).igd.getMatchingStacks()));
        ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(((ExtractorRecipe)this.getRecipe()).output));
    }
}
