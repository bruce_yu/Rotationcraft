package cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.handlers;

import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.crafting.WorktableRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.scripts.WorktableRecipes;
import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import crafttweaker.annotations.ModOnly;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.List;

import static cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.InputHelper.toStack;

@ZenClass("mods.rotationcraft.worktable")
@ModOnly("rc")
@ZenRegister
public class Worktable {
    @ZenMethod
    public static void addRecipe(IItemStack output,IItemStack[] input) {
        List<Ingredient> ingredients = NonNullList.create();
        for(IItemStack stack:input){
            ingredients.add(Ingredient.fromStacks(toStack(stack)));
        }

        WorktableRecipe recipe = new WorktableRecipe(ingredients.toArray(new Ingredient[]{}),toStack(output),new ItemStack[]{},new NBTTagCompound(),false);
        Recipes.INSTANCE.addPattern(TileWorktable.class,recipe);
    }

    @ZenMethod
    public static void delRecipe(IItemStack output) {
        WorktableRecipes.stacksToRemove.add(toStack(output));
    }
}
