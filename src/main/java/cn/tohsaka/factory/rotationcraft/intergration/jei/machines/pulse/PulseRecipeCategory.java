//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.pulse;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockPulse;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiPulse;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.*;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;

import java.util.List;

public class PulseRecipeCategory extends RCRecipeCategory<PulseRecipeWrapper> implements ITooltipCallback<FluidStack> {
    private static final ResourceLocation guiTexture = GuiPulse.tex;
    private final IDrawableAnimated progressBar0;
    private final IDrawableAnimated progressBar1;

    public PulseRecipeCategory(IGuiHelper guiHelper) {
        super(guiHelper.createDrawable(guiTexture, 4, 4, 168, 72), RotationCraft.blocks.get(BlockPulse.NAME).getUnlocalizedName()+".name");

        IDrawableStatic progressBarDrawable0 = guiHelper.createDrawable(guiTexture, 176,56, 38, 40);
        this.progressBar0 = guiHelper.createAnimatedDrawable(progressBarDrawable0, 40, IDrawableAnimated.StartDirection.BOTTOM, false);

        IDrawableStatic progressBarDrawable1 = guiHelper.createDrawable(guiTexture, 214,54, 6, 12);
        this.progressBar1 = guiHelper.createAnimatedDrawable(progressBarDrawable1, 40, IDrawableAnimated.StartDirection.TOP, false);
    }

    public String getUid() {
        return RCRecipeCategoryUid.PULSE;
    }

    public void drawExtras(Minecraft minecraft) {
        progressBar0.draw(minecraft,110, 18);
        progressBar1.draw(minecraft,126, 31);
    }

    public void setRecipe(IRecipeLayout recipeLayout, PulseRecipeWrapper recipeWrapper, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, true, 120, 11);
        guiItemStacks.init(1, false, 120, 47);

        IGuiFluidStackGroup guiFluidStacks = recipeLayout.getFluidStacks();
        int fluidcap = recipeWrapper.getRecipe().getData().getInteger("fluid");
        guiFluidStacks.init(0,true,87,12,5,52,fluidcap*5,true,null);
        guiFluidStacks.addTooltipCallback(this::onTooltip);
        guiFluidStacks.set(0,new FluidStack(RotationCraft.fluids.get("jetfuel"),fluidcap));
        guiItemStacks.set(ingredients);
    }


    @Override
    public void onTooltip(int slotIndex, boolean input, FluidStack ingredient, List<String> tooltip) {
        tooltip.add(1,ingredient.amount+" mB");
    }
}
