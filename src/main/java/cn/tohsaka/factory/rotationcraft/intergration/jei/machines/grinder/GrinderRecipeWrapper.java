//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.grinder;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.GrinderRecipe;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class GrinderRecipeWrapper extends RCRecipeWrapper<CraftingPattern> {
    public static HashSet<GrinderRecipeWrapper> recipes = new HashSet<>();
    public GrinderRecipeWrapper(CraftingPattern recipe) {
        super(recipe);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        NBTTagCompound tag = this.getRecipe().getData();
        if(CraftingPattern.hasFluids(tag)>0){
            ingredients.setOutput(VanillaTypes.FLUID,CraftingPattern.getFluid(tag)[0]);
        }
        List<ItemStack> stackList = Arrays.asList(((GrinderRecipe)this.getRecipe()).igd.getMatchingStacks());
        for(ItemStack stack:stackList){
            stack.setCount(tag.getInteger("input_count"));
        }
        ingredients.setInputs(VanillaTypes.ITEM, stackList);
        ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(new ItemStack[]{this.getRecipe().getOutput()}));
    }
}
