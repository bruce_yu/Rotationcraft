package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameInitializer
public class ItemWorker extends ItemBase implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"worker");
    public static ItemStack item;
    public static ItemStack fluid;
    public static ItemStack energy;
    public ItemWorker(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setHasSubtypes(true);
        this.setMaxStackSize(1);
        ForgeRegistries.ITEMS.register(this);
    }
    public static void init(){
        ItemWorker itemWorker = new ItemWorker();
        RotationCraft.INSTANCE.items.put(NAME,itemWorker);
        item = new ItemStack(itemWorker,1,0);
        fluid = new ItemStack(itemWorker,1,1);
        energy = new ItemStack(itemWorker,1,2);
        RecipeHelper.addShapelessRecipe(item,new ItemStack(Blocks.PISTON),new ItemStack(Blocks.CHEST));
        RecipeHelper.addShapelessRecipe(fluid,new ItemStack(Blocks.PISTON),new ItemStack(Items.BUCKET));
        RecipeHelper.addShapelessRecipe(energy,new ItemStack(Blocks.PISTON),new ItemStack(Items.REDSTONE));
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if(isInCreativeTab(tab)){
            items.add(item);
            items.add(fluid);
            items.add(energy);
        }
    }



    @Override
    public String getUnlocalizedName(ItemStack stack) {
        return super.getUnlocalizedName(stack)+"."+stack.getMetadata();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(NAME,"item"));
        ModelLoader.setCustomModelResourceLocation(this, 1, new ModelResourceLocation(NAME,"fluid"));
        ModelLoader.setCustomModelResourceLocation(this, 2, new ModelResourceLocation(NAME,"energy"));
    }
}
