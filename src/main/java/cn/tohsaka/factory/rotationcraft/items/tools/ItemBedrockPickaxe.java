package cn.tohsaka.factory.rotationcraft.items.tools;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockSpawner;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import com.google.common.collect.Multimap;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.BlockGlowstone;
import net.minecraft.block.BlockMobSpawner;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.MobSpawnerBaseLogic;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import java.util.Random;

@GameInitializer(after = ItemMaterial.class)
public class ItemBedrockPickaxe extends ItemPickaxe implements IModelRegister {
    public static ToolMaterial material;
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"bedrock_pickaxe");
    protected ItemBedrockPickaxe() {
        super(material);
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }

    public static void init(){
        material = EnumHelper.addToolMaterial("bedrocktools",10,Integer.MAX_VALUE,100,18,20);
        ItemBedrockPickaxe item = new ItemBedrockPickaxe();
        RotationCraft.INSTANCE.items.put(NAME,item);
        ItemBedrockAxe.init();
        ItemBedrockHoe.init();
        ItemBedrockShovel.init();
        ItemBedrockShears.init();
        ItemBedrockSword.init();
    }

    @Override
    public boolean canHarvestBlock(IBlockState block) {
        if(block.getBlock() instanceof BlockFalling){
            return true;
        }
        if(block.getBlock() instanceof BlockGlowstone){
            return true;
        }
        return super.canHarvestBlock(block);
    }

    @Override
    public boolean isDamageable() {
        return false;
    }


    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack) {
        Multimap<String, AttributeModifier> map = super.getAttributeModifiers(slot, stack);
        if (slot == EntityEquipmentSlot.MAINHAND) {
            map.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Tool modifier", 10D, 0));
            }

        return map;
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":bedrock_pickaxe"));
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        super.setDamage(stack, 0);
    }


    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player) {
        IBlockState state = player.getEntityWorld().getBlockState(pos);
        World world = player.getEntityWorld();
        if(!world.isRemote){
            if(state.getBlock().equals(Blocks.STONE) || state.getBlock() instanceof BlockFalling){
                return world.destroyBlock(pos,player.isSneaking());
            }

            if(state.getBlock() instanceof BlockMobSpawner){
                TileEntityMobSpawner spawner = (TileEntityMobSpawner) world.getTileEntity(pos);
                MobSpawnerBaseLogic logic = spawner.getSpawnerBaseLogic();
                forceSpawn(spawner,(state.getBlock() instanceof BlockSpawner)?5:20);
                EntityItem item = new EntityItem(world);
                item.setItem(getSpawnerStack(logic));
                item.setPosition(pos.getX(),pos.getY()+0.5,pos.getZ());
                world.spawnEntity(item);
                return world.destroyBlock(pos,false);
            }
        }
        return super.onBlockStartBreak(itemstack, pos, player);
    }



    /**
     * Called when the equipped item is right clicked.
     *
     * @param worldIn
     * @param playerIn
     * @param handIn
     */
    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        if(playerIn.isSneaking()){
            ItemStack stack = playerIn.getHeldItem(handIn);
            int mode = 0;
            if(stack.hasTagCompound() && stack.getTagCompound().hasKey("mode")){
                mode+=stack.getTagCompound().getInteger("mode");
            }
            mode++;
            if(mode>1){
                mode = 0;
            }
            if(!stack.hasTagCompound()){
                stack.setTagCompound(new NBTTagCompound());
            }
            stack.getTagCompound().setInteger("mode",mode);
        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }




    @Override
    public float getDestroySpeed(ItemStack stack, IBlockState state) {
        if(state.getBlock() instanceof BlockGlowstone || state.getBlock() instanceof BlockFalling){
            return efficiency;
        }
        return super.getDestroySpeed(stack, state);
    }

    private ItemStack getSpawnerStack(MobSpawnerBaseLogic spawner){
        ItemStack stack = new ItemStack(BlockSpawner.itemBlock);
        NBTTagCompound tag = spawner.spawnData.getNbt();
        setSpawnerItemNBT(stack,spawner);
        return stack;
    }
    public static void setSpawnerItemNBT(ItemStack is, MobSpawnerBaseLogic lgc) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setString("entity", lgc.spawnData.getNbt().getString("id"));
        is.setTagCompound(tag);
    }


    public static void forceSpawn(TileEntityMobSpawner spw, int num, PotionEffect... potions) {
        World world = spw.getWorld();
        if (world.isRemote)
            return;
        String name = spw.getSpawnerBaseLogic().spawnData.getNbt().getString("id");
        for (int i = 0; i < num; i++) {
            Entity e = EntityList.createEntityByIDFromName(new ResourceLocation(name), world);
            double ex = getRandomPlusMinus(spw.getPos().getX()+0.5, 3.5D,world.rand);
            double ez = getRandomPlusMinus(spw.getPos().getZ()+0.5, 3.5D,world.rand);
            double ey = getRandomPlusMinus(spw.getPos().getY()+0.5, 1.5D,world.rand);
            e.setPositionAndRotation(ex, ey, ez, 0, 0);
            if (e instanceof EntityLivingBase && potions != null) {
                for (int m = 0; m < potions.length; m++)
                    ((EntityLivingBase)e).addPotionEffect(potions[m]);
            }
            world.spawnEntity(e);
        }
    }

    public static double getRandomPlusMinus(double base, double range, Random r) {
        double add = -range+r.nextDouble()*range*2;
        return (base+add);
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }


    public static ItemStack getEnchantStack(){
        ItemStack stack = new ItemStack(RotationCraft.items.get(NAME));
        stack.addEnchantment(Enchantments.FORTUNE,10);
        return stack;
    }
}
