package cn.tohsaka.factory.rotationcraft.items.tools;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import com.google.common.collect.Multimap;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.init.Enchantments;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ItemBedrockSword extends ItemSword implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"bedrock_sword");

    protected ItemBedrockSword() {
        super(ItemBedrockPickaxe.material);
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }

    public static void init(){
        ItemBedrockSword item = new ItemBedrockSword();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }


    @Override
    public boolean isDamageable() {
        return false;
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":bedrock_sword"));
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        super.setDamage(stack, 0);
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack) {
        Multimap<String, AttributeModifier> map = super.getAttributeModifiers(slot, stack);
        if (slot == EntityEquipmentSlot.MAINHAND) {
            map.clear();
            map.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", 60D, 0));
            map.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", 2D, 0));
        }
        return map;
    }

    public static ItemStack getEnchantStack(){
        ItemStack stack = new ItemStack(RotationCraft.items.get(NAME));
        stack.addEnchantment(Enchantments.LOOTING,5);
        stack.addEnchantment(Enchantments.SMITE,5);
        stack.addEnchantment(Enchantments.BANE_OF_ARTHROPODS,5);
        return stack;
    }
}
