package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.BlockOre;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.oredict.OreDictionary;

import java.util.LinkedList;

public class ItemResource extends Item implements IModelRegister {

    public final int color;
    public final String model;
    public ItemResource(ResourceLocation NAME,int color,String model){
        this.color = color;
        this.model = model;
        setRegistryName(NAME);
        setHasSubtypes(true);
        setUnlocalizedName(NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }


    public static void registerOreExtended(BlockOre block, String oreName, int color){
        ItemResource itemIngot = new ItemResource(new ResourceLocation(RotationCraft.MOD_ID,"ingot"+oreName),color,"ingot");
        ItemResource itemDust = new ItemResource(new ResourceLocation(RotationCraft.MOD_ID,"dust"+oreName),color,"dust2");
        RotationCraft.items.put(itemIngot.getRegistryName(),itemIngot);
        RotationCraft.items.put(itemDust.getRegistryName(),itemDust);
        OreDictionary.registerOre("ingot"+oreName,itemIngot);
        OreDictionary.registerOre("dust"+oreName,itemDust);
        FurnaceRecipes.instance().addSmeltingRecipeForBlock(block,new ItemStack(itemIngot),1F);
        resources.add(itemIngot);
        resources.add(itemDust);
    }

    public static LinkedList<ItemResource> resources = new LinkedList<>();

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":ore/"+model));
    }
}
