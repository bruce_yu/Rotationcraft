package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.oredict.OreDictionary;

@GameInitializer
public class ItemCrushedOre extends Item implements IModelRegister, INeedPostInit {
    protected static ItemCrushedOre INSTANCE;
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"crushedore");
    public ItemCrushedOre(){
        setRegistryName(NAME);
        setHasSubtypes(true);
        setUnlocalizedName(NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
        INSTANCE = this;
    }

    public static void init(){
        ItemCrushedOre item = new ItemCrushedOre();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":ore/dust"));
        ModelLoader.setCustomModelResourceLocation(this, 1, new ModelResourceLocation(RotationCraft.MOD_ID+":ore/dirty"));
        ModelLoader.setCustomModelResourceLocation(this, 2, new ModelResourceLocation(RotationCraft.MOD_ID+":ore/pure"));
        ModelLoader.setCustomModelResourceLocation(this, 3, new ModelResourceLocation(RotationCraft.MOD_ID+":ore/crushed"));
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if(this.isInCreativeTab(tab)){
            items.add(new ItemStack(this, 1, 0));
            items.add(new ItemStack(this, 1, 1));
            items.add(new ItemStack(this, 1, 2));
            items.add(new ItemStack(this, 1, 3));
        }
    }

    public static ItemStack createByOre(ItemStack orein,int count,int meta){
        if(orein.isEmpty()){
            return ItemStack.EMPTY;
        }
        ItemStack stack = new ItemStack(INSTANCE,count,meta);
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setTag("ore",orein.writeToNBT(new NBTTagCompound()));
        stack.setTagCompound(nbt);

        return stack;
    }

    public static ItemStack createBySelf(ItemStack orein,int count,int meta){
        ItemStack stack = new ItemStack(INSTANCE,count,meta);
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setTag("ore",orein.getTagCompound().getCompoundTag("ore"));
        stack.setTagCompound(nbt);
        return stack;
    }

    public static boolean isOreEqual(ItemStack stack,ItemStack stack2){
        if(!stack.hasTagCompound() || !stack2.hasTagCompound()){
            return false;
        }
        if(!stack.getTagCompound().hasKey("ore") || !stack2.getTagCompound().hasKey("ore")){
            return false;
        }
        ItemStack ore1 = new ItemStack(stack.getTagCompound().getCompoundTag("ore"));
        ItemStack ore2 = new ItemStack(stack2.getTagCompound().getCompoundTag("ore"));
        if(ore1.isItemEqual(ore2)){
            return true;
        }

        return false;
    }

    public static String getOreName(ItemStack stack){
        if(stack.isEmpty()){
            return null;
        }
        if(!stack.hasTagCompound()){
            return null;
        }
        if(!stack.getTagCompound().hasKey("ore")){
            return null;
        }
        ItemStack ore = new ItemStack(stack.getTagCompound().getCompoundTag("ore"));
        int[] ids = OreDictionary.getOreIDs(ore);
        if(ids!=null && ids.length>0){
            for(int id:ids){
                if(OreDictionary.getOreName(id).toLowerCase().contains("ore")){
                    return OreDictionary.getOreName(id);
                }
            }
        }
        return null;
    }


    @Override
    public String getItemStackDisplayName(ItemStack stack) {
        if(!stack.hasTagCompound() || !stack.getTagCompound().hasKey("ore")){
            return StringHelper.localize(String.format("item.ore.%d.name",stack.getMetadata()));
        }
        ItemStack ore = new ItemStack(stack.getTagCompound().getCompoundTag("ore"));
        String name="%s";
        switch (stack.getMetadata()){
            case 0:
                name="rc.ore.dust.name";
                break;
            case 1:
                name="rc.ore.dirty.name";
                break;
            case 2:
                name="rc.ore.pure.name";
                break;
            case 3:
                name="rc.ore.crushed.name";
                break;
        }
        return String.format(StringHelper.localize(name),ore.getDisplayName());
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        ItemStack stack = playerIn.getHeldItem(handIn);
        if(!stack.hasTagCompound() || !stack.getTagCompound().hasKey("ore")){
            return new ActionResult(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
        }
        ItemStack ore = new ItemStack(stack.getTagCompound().getCompoundTag("ore"));

        if(ore.getItem() instanceof ItemBlock){
            Block b = ((ItemBlock)ore.getItem()).getBlock();
            IBlockState state = b.getStateFromMeta(ore.getMetadata());
            NonNullList drops = NonNullList.create();
            b.getDrops(drops,worldIn,new BlockPos(0,0,0),state,10);
            System.out.print("ok");
        }

        return new ActionResult(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
        //return super.onItemRightClick(worldIn, playerIn, handIn);
    }
}
