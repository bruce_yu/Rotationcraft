package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IScrewable;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cofh.api.block.IDismantleable;
import cofh.api.item.IToolHammer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameInitializer
public class ItemScrewer extends ItemBase implements IModelRegister, IToolHammer {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"screwer");
    public ItemScrewer(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }
    public static void init(){
        ItemScrewer item = new ItemScrewer();
        RotationCraft.INSTANCE.items.put(NAME,item);

        //ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation("Minecraft:stick"));
    }
    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntity te = world.getTileEntity(pos);
        if(te == null){
            return super.onItemUse(player, world, pos, hand, facing, hitX, hitY, hitZ);
        }
        if(player.isSneaking()){
            if(te instanceof IScrewable){
                return ((IScrewable) te).screw(facing,player)?EnumActionResult.SUCCESS:EnumActionResult.PASS;
            }


            if(te instanceof IModeProvider){
                if(((IModeProvider)te).onChangingMode(world,player,pos)){
                    return EnumActionResult.SUCCESS;
                }
            }

            if(player!=null && player.world!=null && pos != null && player.getHeldItem(hand).getItem() instanceof IToolHammer){
                IBlockState state = world.getBlockState(pos);
                if(state!=null && state.getBlock() instanceof IDismantleable && ((IDismantleable) state.getBlock()).canDismantle(world,pos,state,player)){
                    if(!world.isRemote){
                        ((IDismantleable) state.getBlock()).dismantleBlock(world,pos,state,(EntityPlayer) player,false);
                    }
                    return EnumActionResult.SUCCESS;
                }
            }

        }else{
            if(te instanceof TileMachineBase){
                ((TileMachineBase) te).rotateBlock(player);
                return EnumActionResult.SUCCESS;
            }
        }

        return super.onItemUse(player, world, pos, hand, facing, hitX, hitY, hitZ);
    }
    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":screwer"));
    }

    public static void postinit(){

    }

    @Override
    public boolean isUsable(ItemStack itemStack, EntityLivingBase entityLivingBase, BlockPos blockPos) {
        return true;
    }

    @Override
    public boolean isUsable(ItemStack itemStack, EntityLivingBase entityLivingBase, Entity entity) {
        return true;
    }

    @Override
    public void toolUsed(ItemStack itemStack, EntityLivingBase entityLivingBase, BlockPos blockPos) {

    }

    @Override
    public void toolUsed(ItemStack itemStack, EntityLivingBase entityLivingBase, Entity entity) {

    }
}
