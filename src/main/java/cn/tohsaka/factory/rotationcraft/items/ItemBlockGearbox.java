package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

public class ItemBlockGearbox extends ItemBlockModelBased {
    public ItemBlockGearbox(Block block) {
        super(block);
        setHasSubtypes(true);
        //setMaxDamage(0);
        setNoRepair();
    }

    /**
     * returns a list of items with the same ID, but different meta (eg: dye returns 16 items)
     *
     * @param tab
     * @param items
     */
    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        super.getSubItems(tab, items);
    }

    public ItemStack setDefaultTag(ItemStack stack)
    {
        return stack;
    }

    /**
     * Returns the unlocalized name of this item. This version accepts an ItemStack so different stacks can have
     * different names based on their damage or NBT.
     *
     * @param stack
     */
    @Override
    public String getUnlocalizedName(ItemStack stack) {
        return super.getUnlocalizedName(stack)+"."+stack.getMetadata();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if(stack.hasTagCompound() && stack.getTagCompound().hasKey("lubricant")){
            FluidStack fluid = FluidStack.loadFluidStackFromNBT(stack.getTagCompound().getCompoundTag("lubricant"));
            if(fluid!=null && fluid.amount>0){
                tooltip.add(fluid.getLocalizedName()+" "+fluid.amount+"mB");
            }
        }
        if(stack.hasTagCompound() && stack.getTagCompound().hasKey("damage") && stack.getTagCompound().getFloat("damage")>0){
            tooltip.add(StringHelper.localize("info.rc.machinedamaged")+":"+String.format("%.3f",stack.getTagCompound().getFloat("damage")));
        }

    }
}
