package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.oredict.OreDictionary;

@GameInitializer
public class ItemReactorMaterial extends ItemBase implements IModelRegister {
    public static ResourceLocation triso = new ResourceLocation(RotationCraft.MOD_ID,"triso");
    public static ResourceLocation triso_used = new ResourceLocation(RotationCraft.MOD_ID,"triso_used");
    public static ResourceLocation dustUranium = new ResourceLocation(RotationCraft.MOD_ID,"uranium_dust");
    public static ResourceLocation dustEmerald = new ResourceLocation(RotationCraft.MOD_ID,"emerald_dust");
    public static ResourceLocation graphite = new ResourceLocation(RotationCraft.MOD_ID,"graphite");
    public ItemReactorMaterial(ResourceLocation name){
        setUnlocalizedName(RotationCraft.MOD_ID + "." + name.getResourcePath());
        setRegistryName(name);
    }

    private void setExtra(ResourceLocation name) {
        if (dustUranium.equals(name)) {
            OreDictionary.registerOre("dustUranium",this);
        }
        if (dustEmerald.equals(name)) {
            OreDictionary.registerOre("dustEmerald",this);
        }
        if (graphite.equals(name)) {
            OreDictionary.registerOre("gemGraphite",this);
        }
    }

    public static void init(){
        registerAndAdd(triso);
        registerAndAdd(triso_used);
        registerAndAdd(dustUranium);
        registerAndAdd(dustEmerald);
        registerAndAdd(graphite);
    }
    public static void registerAndAdd(ResourceLocation name){
        ItemReactorMaterial item = new ItemReactorMaterial(name);
        ForgeRegistries.ITEMS.register(item);
        item.setExtra(name);
        RotationCraft.items.put(name,item);
    }
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(RotationCraft.items.get(triso),0,new ModelResourceLocation("rc:reactormaterial","triso"));
        ModelLoader.setCustomModelResourceLocation(RotationCraft.items.get(triso_used),0,new ModelResourceLocation("rc:reactormaterial","triso_used"));
        ModelLoader.setCustomModelResourceLocation(RotationCraft.items.get(dustUranium),0,new ModelResourceLocation("rc:reactormaterial","dusturanium"));
        ModelLoader.setCustomModelResourceLocation(RotationCraft.items.get(dustEmerald),0,new ModelResourceLocation("rc:reactormaterial","dustemerald"));
        ModelLoader.setCustomModelResourceLocation(RotationCraft.items.get(graphite),0,new ModelResourceLocation("rc:reactormaterial","graphite"));
    }
}
