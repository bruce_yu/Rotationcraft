package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Enchantments;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.stats.StatList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

@GameInitializer
public class ItemArmorBedrock extends ItemArmor implements IModelRegister {
    public static final ArmorMaterial bedrock_armor = EnumHelper.addArmorMaterial("rc:bedrock_armor", "", 9999, new int[] { 8, 20, 16, 8 }, 1000, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 20.0F);
    public final EntityEquipmentSlot slot;
    public static ResourceLocation HELMET = new ResourceLocation("rc","bedrock_helmet");
    public static ResourceLocation CHESTPLATE = new ResourceLocation("rc","bedrock_chestplate");
    public static ResourceLocation CHESTPLATE_FLYABLE = new ResourceLocation("rc","bedrock_chestplate_flyable");
    public static ResourceLocation PANTS = new ResourceLocation("rc","bedrock_pants");
    public static ResourceLocation BOOTS = new ResourceLocation("rc","bedrock_boots");

    public static ItemArmorBedrock bedrock_helmet;
    public static ItemArmorBedrock bedrock_chestplate;
    public static ItemArmorBedrock bedrock_chestplate_flyable;
    public static ItemArmorBedrock bedrock_pants;
    public static ItemArmorBedrock bedrock_boots;

    public static void init(){
        bedrock_helmet = new ItemArmorBedrock(EntityEquipmentSlot.HEAD);
        bedrock_helmet.setUnlocalizedName("rc.bedrock_helmet");
        bedrock_helmet.setRegistryName(HELMET);

        bedrock_chestplate = new ItemArmorBedrock(EntityEquipmentSlot.CHEST);
        bedrock_chestplate.setUnlocalizedName("rc.bedrock_chestplate");
        bedrock_chestplate.setRegistryName(CHESTPLATE);

        bedrock_pants = new ItemArmorBedrock(EntityEquipmentSlot.LEGS);
        bedrock_pants.setUnlocalizedName("rc.bedrock_pants");
        bedrock_pants.setRegistryName(PANTS);

        bedrock_boots = new ItemArmorBedrock(EntityEquipmentSlot.FEET);
        bedrock_boots.setUnlocalizedName("rc.bedrock_boots");
        bedrock_boots.setRegistryName(BOOTS);

        bedrock_chestplate_flyable = new ItemArmorBedrock(EntityEquipmentSlot.CHEST);
        bedrock_chestplate_flyable.setUnlocalizedName("rc.bedrock_chestplate_flyable");
        bedrock_chestplate_flyable.setRegistryName(CHESTPLATE_FLYABLE);

        ForgeRegistries.ITEMS.register(bedrock_helmet);
        ForgeRegistries.ITEMS.register(bedrock_chestplate);
        ForgeRegistries.ITEMS.register(bedrock_chestplate_flyable);
        ForgeRegistries.ITEMS.register(bedrock_pants);
        ForgeRegistries.ITEMS.register(bedrock_boots);
        RotationCraft.INSTANCE.items.put(HELMET,bedrock_helmet);
        RotationCraft.INSTANCE.items.put(CHESTPLATE,bedrock_chestplate);
        RotationCraft.INSTANCE.items.put(CHESTPLATE_FLYABLE,bedrock_chestplate_flyable);
        RotationCraft.INSTANCE.items.put(PANTS,bedrock_pants);
        RotationCraft.INSTANCE.items.put(BOOTS,bedrock_boots);
    }

    @Override
    public void registerModel() {
        registerModel(bedrock_helmet, "helmet_bedrock");
        registerModel(bedrock_chestplate, "chestplate_bedrock");
        registerModel(bedrock_chestplate_flyable, "chestplate_bedrock_flyable");
        registerModel(bedrock_pants, "leggings_bedrock");
        registerModel(bedrock_boots, "boots_bedrock");
    }

    @SideOnly (Side.CLIENT)
    public void registerModel(Item item, String stackName) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(RotationCraft.MOD_ID + ":armor", "type=" + stackName));
    }

    public ItemArmorBedrock(EntityEquipmentSlot slot) {
        super(bedrock_armor, 0, slot);
        this.slot = slot;
    }


    private static final String[] TEXTURES = {"rc:textures/armor/bedrock_1.png","rc:textures/armor/bedrock_2.png","rc:textures/armor/bedrock_flyable.png"};

    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String type) {
        if(stack.getItem().equals(bedrock_chestplate_flyable)){
            return TEXTURES[2];
        }
        if (slot == EntityEquipmentSlot.LEGS) {
            return TEXTURES[1];
        }
        return TEXTURES[0];
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        super.setDamage(stack, 0);
    }


    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }


    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack) {

        if (slot == armorType) {
            Multimap<String, AttributeModifier> map = super.getAttributeModifiers(slot, stack);
            map.putAll(properties);
            return map;
        }
        return super.getAttributeModifiers(slot, stack);
    }
    protected Multimap<String, AttributeModifier> properties = HashMultimap.create();




    @SuppressWarnings ("rawtypes")
    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
        if (armorType == EntityEquipmentSlot.HEAD) {
            player.setAir(300);
            player.getFoodStats().addStats(20, 20F);
            addPotionEffect(player,MobEffects.NIGHT_VISION,3);
        } else if (armorType == EntityEquipmentSlot.CHEST) {
            if(itemStack.getItem().equals(bedrock_chestplate_flyable)){
                player.capabilities.allowFlying = true;
                if(world.isRemote){
                    player.capabilities.setFlySpeed(0.2F);
                }
            }
            List<PotionEffect> effects = Lists.newArrayList(player.getActivePotionEffects());
            for (PotionEffect potion : Collections2.filter(effects, potion -> potion.getPotion().isBadEffect())) {
                player.removePotionEffect(potion.getPotion());
            }
            player.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(40);
            addPotionEffect(player,MobEffects.REGENERATION,10);
        } else if (armorType == EntityEquipmentSlot.LEGS) {
            if (player.isBurning()) {
                player.extinguish();
                addPotionEffect(player,MobEffects.FIRE_RESISTANCE,1);
            }
            addPotionEffect(player,MobEffects.HASTE,1);
            addPotionEffect(player,MobEffects.SPEED,3);
        } else if (armorType == EntityEquipmentSlot.FEET){
            player.fallDistance = 0;
            player.takeStat(StatList.FALL_ONE_CM);
            addPotionEffect(player,MobEffects.JUMP_BOOST,2);
        }
    }


    private void addPotionEffect(EntityPlayer player, Potion potion,int amp){
        PotionEffect nv = player.getActivePotionEffect(potion);
        if(nv == null) {
            nv = new PotionEffect(potion, 300,  amp, false, false);
            player.addPotionEffect(nv);
        }
        nv.duration = 300;
    }
    public static ItemStack getEnchantStack(EntityEquipmentSlot slot){
        ItemStack stack = ItemStack.EMPTY;
        if (slot == EntityEquipmentSlot.HEAD){
            stack = new ItemStack(bedrock_helmet);
            stack.addEnchantment(Enchantments.AQUA_AFFINITY,5);
        }

        if (slot == EntityEquipmentSlot.CHEST){
            stack = new ItemStack(bedrock_chestplate);
            stack.addEnchantment(Enchantments.PROJECTILE_PROTECTION,5);

        }

        if (slot == EntityEquipmentSlot.LEGS){
            stack = new ItemStack(bedrock_pants);
            stack.addEnchantment(Enchantments.BLAST_PROTECTION,5);
        }

        if (slot == EntityEquipmentSlot.FEET){
            stack = new ItemStack(bedrock_boots);
            stack.addEnchantment(Enchantments.BLAST_PROTECTION,5);
        }
        stack.addEnchantment(Enchantments.PROTECTION,5);
        return stack;
    }










}