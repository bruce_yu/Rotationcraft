package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer
public class ItemPortalGem extends Item implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"portalgem");
    public ItemPortalGem(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
        setMaxStackSize(1);
        setNoRepair();
    }

    public static void init(){
        ItemPortalGem item = new ItemPortalGem();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation("minecraft:ender_pearl"));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if(player.isSneaking()){
            if(stack.hasTagCompound()){
                stack.setTagCompound(null);
            }else{
                NBTTagCompound tagCompound = new NBTTagCompound();
                tagCompound.setInteger("world",worldIn.provider.getDimension());
                tagCompound.setDouble("x",player.posX);
                tagCompound.setDouble("y",player.posY);
                tagCompound.setDouble("z",player.posZ);
                tagCompound.setFloat("pitch",player.rotationPitch);
                tagCompound.setFloat("yaw",player.rotationYaw);
                stack.setTagCompound(tagCompound);
                return new ActionResult<>(EnumActionResult.SUCCESS,stack);
            }
        }
        return new ActionResult<>(EnumActionResult.FAIL,stack);
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return stack.hasTagCompound();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        if(stack.hasTagCompound()){
            StringBuilder sb = new StringBuilder();
            sb.append("DIM"+stack.getTagCompound().getInteger("world"));
            sb.append(String.format(" [%d,%d,%d]",Math.round(stack.getTagCompound().getDouble("x")),
                    Math.round(stack.getTagCompound().getDouble("y")),
                            Math.round(stack.getTagCompound().getDouble("z"))));
            tooltip.add(sb.toString());
        }
    }

}
