package cn.tohsaka.factory.rotationcraft.enet;

import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ITickable;
import net.minecraftforge.fml.common.FMLCommonHandler;

import javax.annotation.Nullable;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class NetworkTicker implements ITickable {
    public static NetworkTicker instance;
    private ConcurrentHashMap<UUID,Network> elements = new ConcurrentHashMap<>();
    private ConcurrentHashMap<UUID,Long> timings = new ConcurrentHashMap<>();
    private int tc = 0;
    public Long getNetTickmillis(UUID uuid){
        if(timings.containsKey(uuid)){
            return timings.get(uuid);
        }
        return 0L;
    }
    @Override
    public void update() {
        if(GeneralConfig.ENET_TICKRATE>0){
            tc++;
            if(tc>GeneralConfig.ENET_TICKRATE){
                tc=0;
            }
        }
        if(tc==0){
            synchronized (elements){
                for (Network element : elements.values()) {
                    long time = System.currentTimeMillis();
                    element.update();
                    timings.put(element.getUuid(),System.currentTimeMillis()-time);
                }
            }
        }
    }

    public static void init(){
        instance = new NetworkTicker();
        MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
        server.tickables.add(instance);
    }

    public void addElement(Network network){
        elements.put(network.getUuid(),network);
    }
    @Nullable
    public void removeElement(Network network){
        elements.remove(network.getUuid());
    }
}
