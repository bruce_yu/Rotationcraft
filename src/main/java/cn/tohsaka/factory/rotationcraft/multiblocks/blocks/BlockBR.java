package cn.tohsaka.factory.rotationcraft.multiblocks.blocks;

import cn.tohsaka.factory.rotationcraft.multiblocks.TypeBR;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;

public class BlockBR extends Block {
        public static final IProperty<TypeBR> TYPE = PropertyEnum.create("type", TypeBR.class);
        public BlockBR() {
                super(Material.IRON);
                setHardness(6F);
        }
}
