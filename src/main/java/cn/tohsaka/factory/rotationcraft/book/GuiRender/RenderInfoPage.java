package cn.tohsaka.factory.rotationcraft.book.GuiRender;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.book.GuiGuide;
import cn.tohsaka.factory.rotationcraft.utils.ItemStackUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RenderInfoPage extends RenderGui{
    protected GuiGuide screen;
    public static ResourceLocation bookb = new ResourceLocation(RotationCraft.MOD_ID,"textures/gui/book/bookb.png");
    public RenderInfoPage(GuiGuide gui) {
        super(gui,gui.guiLeft+10,gui.guiTop,256,220);
        screen = gui;
    }


    public void mouseClicked(GuiGuide screen,String id,int mouseX, int mouseY, int mouseButton){

    }

    @Override
    public void drawForegroundLayer(int mouseX, int mouseY) {

    }

    @Override
    public void drawBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        screen.mc.renderEngine.bindTexture(new ResourceLocation(screen.currentPage.background));
        screen.drawModalRectWithCustomSizedTexture(screen.guiLeft+10,screen.guiTop,0,0,256,220,256,241);

        FontRenderer fr = screen.mc.fontRenderer;
        GlStateManager.pushMatrix();
        GlStateManager.translate(screen.guiLeft+17,screen.guiTop+7,0);
        fr.drawString(screen.currentPage.title,0,0,0);
        GlStateManager.translate(0,15,0);
        drawSubtitle(screen.currentPage.subtitle,fr);
        GlStateManager.popMatrix();
        for (int i=0;i<screen.currentPage.body.length;i++){
            String b = screen.currentPage.body[i];
            fr.drawString(b, screen.guiLeft+20, screen.guiTop+90+i*10, 0xFFFFFF);
        }
    }

    private void drawSubtitle(String[] subtitle,FontRenderer fr){
        //fr.drawString(screen.currentPage.subtitle,0,0,0);
        for (String row:subtitle){
            GlStateManager.pushMatrix();
            if(row.startsWith("item:")){//item:rc:blockmeter,0,0,8,1.0       item:modid:registryname,meta,x,y,scale;
                String[] r2 = row.replace("item:","").split(",");
                ItemStack stack = ItemStackUtils.simpleParse(String.join(",",r2[0],r2[1]));
                int x = Integer.parseInt(r2[2]);
                int y = Integer.parseInt(r2[3]);
                float scale = Float.parseFloat(r2[4]);
                GlStateManager.translate(x,y,0);
                GlStateManager.scale(scale,scale,scale);


                RenderHelper.enableGUIStandardItemLighting();
                RenderItem itemRenderer = Minecraft.getMinecraft().getRenderItem();
                itemRenderer.renderItemIntoGUI(stack,0,0);
                RenderHelper.disableStandardItemLighting();
            }
            if(row.startsWith("str:")){
                String[] r2 = row.replace("str:","").split(",");
                int color = Integer.parseInt(r2[1]);
                int x = Integer.parseInt(r2[2]);
                int y = Integer.parseInt(r2[3]);
                float scale = Float.parseFloat(r2[4]);
                GlStateManager.translate(x,y,0);
                GlStateManager.scale(scale,scale,scale);
                fr.drawString(r2[0], 0, 0, color);
            }
            GlStateManager.popMatrix();
        }
    }



}
