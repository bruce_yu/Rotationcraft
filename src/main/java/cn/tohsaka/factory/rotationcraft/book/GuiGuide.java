package cn.tohsaka.factory.rotationcraft.book;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.book.GuiRender.RenderGui;
import cn.tohsaka.factory.rotationcraft.book.GuiRender.RenderIcon;
import cn.tohsaka.factory.rotationcraft.book.GuiRender.RenderInfoPage;
import cn.tohsaka.factory.rotationcraft.book.GuiRender.RenderTabStack;
import cn.tohsaka.factory.rotationcraft.items.ItemGuide;
import cn.tohsaka.factory.rotationcraft.utils.ItemStackUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentString;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class GuiGuide extends GuiScreen {
    BookProvider.Startpage startpage;
    protected int xSize = 276;
    protected int ySize = 220;
    public int guiLeft;
    public int guiTop;
    public BookProvider.BookPage currentPage;
    public Map<String,BookProvider.BookCategory> categoryMap = new HashMap<>();
    public RenderIcon prev;
    public RenderIcon next;
    public Map<String,RenderGui> renders = new HashMap<>();
    public LinkedList<RenderGui> pages = new LinkedList<>();
    public RenderInfoPage page;
    public int pageOffset = 0;
    @Override
    public void initGui() {
        super.initGui();
        this.guiLeft = (this.width - this.xSize) / 2;
        this.guiTop = (this.height - this.ySize) / 2;
        page = new RenderInfoPage(this);
        startpage = BookProvider.readIndex();

        if(startpage==null || startpage.categories.size()==0){
            Minecraft.getMinecraft().player.closeScreen();
            Minecraft.getMinecraft().player.sendMessage(new TextComponentString("<Rotationcraft> no guide for this language"));
            return;
        }

        for(BookProvider.BookCategory c:startpage.categories){
            categoryMap.put(c.uuid,c);
        }

        currentPage = startpage.startpage;

        prev = new RenderIcon(this,guiLeft-7,guiTop + 2).setV(0);
        next = new RenderIcon(this,guiLeft-7, guiTop + 2 + 200).setV(1);

        renders.clear();
        pages.clear();

        addStacktoRender("main",new RenderTabStack(this,new ItemStack(RotationCraft.items.get(ItemGuide.NAME)),null,guiLeft-7,0,true,"main").setActive(true));
        for (BookProvider.BookCategory ca:startpage.categories){
            ItemStack stack = ItemStackUtils.simpleParse(ca.icon);
            addStacktoRender(ca.uuid,new RenderTabStack(this,stack,ca.title,guiLeft-7,0,true,ca.uuid));
        }

    }



    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {

        int i = this.guiLeft;
        int j = this.guiTop;
        this.drawBackgroundLayer(partialTicks, mouseX, mouseY);

        this.drawForegroundLayer(mouseX, mouseY);
    }

    private void drawBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        prev.drawBackgroundLayer(partialTicks,mouseX,mouseY);
        int top = guiTop +2 + 20;
        for(int i=0;i<9;i++){
            if(pages.size()>(i+pageOffset)){
                pages.get(i+pageOffset).y = top + 20*i;
                pages.get(i+pageOffset).drawBackgroundLayer(partialTicks,mouseX,mouseY);
            }
        }

        page.drawBackgroundLayer(partialTicks,mouseX,mouseY);

        next.drawBackgroundLayer(partialTicks,mouseX,mouseY);
    }

    private void drawForegroundLayer(int mouseX, int mouseY) {
        prev.drawForegroundLayer(mouseX,mouseY);
        int top = guiTop +2 + 20;
        for(int i=0;i<9;i++){
            if(pages.size()>(i+pageOffset)){
                pages.get(i+pageOffset).y = top + 20*i;
                pages.get(i+pageOffset).drawForegroundLayer(mouseX,mouseY);
            }
        }
        page.drawForegroundLayer(mouseX,mouseY);
        next.drawForegroundLayer(mouseX,mouseY);
    }

    public void addStacktoRender(String key,RenderGui render){
        renders.put(key,render);
        pages.add(render);
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        if(mouseY>guiTop && (mouseY-guiTop)<20){
            pageOffset = Math.max(0,pageOffset-1);
            return;
        }
        if(mouseY>(guiTop+200) && mouseY<(guiTop+220)){
            pageOffset = Math.min(pages.size()-9,pageOffset+1);
            return;
        }

        if(mouseX>=guiLeft-7 && mouseX<=guiLeft+13){
            int top = guiTop + 10 + 20;//fixed!! but,why????
            int index = Math.round((mouseY-top) / 20F)+pageOffset;
            pages.get(index).mouseClicked(this,pages.get(index).key,mouseX,mouseY,mouseButton);
        }

        /*for(RenderGui render:pages){
            if(mouseX>=render.x && mouseX<=render.x+render.w && mouseY>=render.y && mouseY<= render.y+render.h){
                render.mouseClicked(this,render.key,mouseX,mouseY,mouseButton);
                return;
            }
        }*/


        //super.mouseClicked(mouseX, mouseY, mouseButton);
    }


    public void mouseClicked(String id, int mouseX, int mouseY, int mouseButton){
        if(id.equalsIgnoreCase("main")){
            currentPage = startpage.startpage;
        }else if(categoryMap.containsKey(id)){
            BookProvider.BookCategory ca = categoryMap.get(id);
            currentPage = BookProvider.readPage(ca.file);
        }
    }

    public void onScroll(int i){
        if(i>0){
            pageOffset = Math.min(pages.size()-9,pageOffset+1);
        }else{
            pageOffset = Math.max(0,pageOffset-1);
        }
    }

    @Override
    public void handleMouseInput() throws IOException {
        int i = Mouse.getEventDWheel();
        int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
        if(x>(guiLeft-7) && x<(guiLeft+13)){
            if (i != 0)
            {
                if(i>=1){
                    i=-1;
                }else{
                    i=1;
                }

                onScroll(i);
            }
        }

        super.handleMouseInput();
    }



    public void drawSizedTexturedModalRect(int x, int y, int u, int v, int width, int height, float texW, float texH) {

        float texU = 1 / texW;
        float texV = 1 / texH;
        BufferBuilder buffer = Tessellator.getInstance().getBuffer();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        buffer.pos(x, y + height, this.zLevel).tex((u) * texU, (v + height) * texV).endVertex();
        buffer.pos(x + width, y + height, this.zLevel).tex((u + width) * texU, (v + height) * texV).endVertex();
        buffer.pos(x + width, y, this.zLevel).tex((u + width) * texU, (v) * texV).endVertex();
        buffer.pos(x, y, this.zLevel).tex((u) * texU, (v) * texV).endVertex();
        Tessellator.getInstance().draw();
    }

}
