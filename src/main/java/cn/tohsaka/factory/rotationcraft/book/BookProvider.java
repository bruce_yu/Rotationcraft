package cn.tohsaka.factory.rotationcraft.book;


import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.events.RotationcraftEvents;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import com.google.gson.Gson;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fml.common.event.FMLEvent;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class BookProvider {
    private static String getLangCode(){
        return Minecraft.getMinecraft().getLanguageManager().getCurrentLanguage().getLanguageCode().toLowerCase();
    }
    private static Map<Integer,String> cache = new HashMap<>();

    private static String getAssets(String path) throws Exception{
        if(GeneralConfig.book_nocache || !cache.containsKey(path.hashCode())){

            String p = "assets/rc/book/"+path;
            InputStream is = RotationCraft.class.getClassLoader().getResourceAsStream(p);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));

            String line;
            StringWriter sw = new StringWriter();
            line = reader.readLine();
            while (line != null) {
                sw.append(line);
                sw.append("\n");
                line = reader.readLine();
            }
            reader.close();
            is.close();
            cache.put(path.hashCode(),sw.toString());
            sw.close();
        }
        return cache.get(path.hashCode());
    }

    /*private static String getAssets(String path) throws Exception{
        if(GeneralConfig.book_nocache || !cache.containsKey(path.hashCode())){
            File jarpath = RotationCraft.coremodLocation;
            String p = "assets/rc/book/"+path;
            ZipFile zipFile = new ZipFile(jarpath);
            ZipEntry entry = zipFile.getEntry(p);
            BufferedReader reader = new BufferedReader(new InputStreamReader(zipFile.getInputStream(entry),"UTF-8"));

            String line;
            StringWriter sw = new StringWriter();
            line = reader.readLine();
            while (line != null) {
                sw.append(line);
                sw.append("\n");
                line = reader.readLine();
            }
            reader.close();
            zipFile.close();
            cache.put(path.hashCode(),sw.toString());
            sw.close();
        }
        return cache.get(path.hashCode());
    }*/
    public static Startpage readIndex(){
        try {
            String data = getAssets(getLangCode()+"/"+"index.json");
            Startpage startpage = new Gson().fromJson(data,Startpage.class);
            MinecraftForge.EVENT_BUS.post(new RotationcraftEvents.BookIndexed(startpage));
            return startpage;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    public static BookPage readPage(String file){
        try {
            String data = getAssets(getLangCode()+"/"+file);
            BookPage bookPage = new Gson().fromJson(data,BookPage.class);
            return bookPage;
        }catch (Exception ex){
            RotationcraftEvents.BookReadPage event = new RotationcraftEvents.BookReadPage(file);
            MinecraftForge.EVENT_BUS.post(event);
            if(event.page != null){
                return event.page;
            }
            ex.printStackTrace();
        }
        return new BookPage("rc:textures/gui/book/bookb.png",
                StringHelper.localize("info.rc.book.notfoundtitle")
                ,new String[]{},
                new String[]{StringHelper.localize("info.rc.book.notfoundsub0")+": "+file},"");
    }

    public class Startpage{
        BookPage startpage;
        public Set<BookCategory> categories;

        public BookPage getCategory(BookCategory ca) {
            BookPage page = readPage(ca.file);
            if(page!=null){
                page.icon = ca.icon;
            }
            return page;
        }
    }
    public static class BookCategory{
        public int index;
        public String uuid;
        public String icon;
        public String title;
        public String file;

        public BookCategory(int index, String uuid, String icon, String title, String file) {
            this.index = index;
            this.uuid = uuid;
            this.icon = icon;
            this.title = title;
            this.file = file;
        }
    }
    public static class BookPage{
        public String background;
        public String title;
        public String[] subtitle;
        public String[] body;
        public String icon;
        public BookPage(String b,String t,String[] s,String[] bd,String i){
            background = b;
            title = t;
            subtitle = s;
            body = bd;
            icon = i;
        }
    }
}
