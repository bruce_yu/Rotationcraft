package cn.tohsaka.factory.rotationcraft.network;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.api.IGuiPacketHandler;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.config.PacketConfigSync;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class NetworkDispatcher {

    public static final String NET_CHANNEL = RotationCraft.MOD_ID;
    public static void dispatchMachineUpdate(TileMachineBase machineTile) {
        PacketCustom packet = new PacketCustom(NET_CHANNEL, 1);
        packet.writePos(machineTile.getPos());
        machineTile.writeUpdateData(packet);
        packet.sendToChunk(machineTile);
    }

    public static void dispatchGuiChanges(EntityPlayer player, IGuiPacketHandler machineTile, boolean isFullSync) {
        PacketCustom packet = new PacketCustom(NET_CHANNEL, 2);
        packet.writePos(((TileEntity)machineTile).getPos());
        packet.writeBoolean(isFullSync);
        machineTile.writeGuiData(packet, isFullSync);
        packet.sendToPlayer(player);
    }
    public static void requireGuiUpdate(TileEntity machineTile) {
        PacketCustom packet = new PacketCustom(NET_CHANNEL, 99);
        packet.writePos(machineTile.getPos());
        packet.sendToServer();
    }

    public static void uploadingClientSetting(EntityPlayer player, TileMachineBase machineTile) {
        PacketCustom packet = new PacketCustom(NET_CHANNEL, 3);
        packet.writePos(machineTile.getPos());
        packet.writeNBTTagCompound(machineTile.getModePacket());
        packet.sendToServer();
    }
    public static void uploadingClientSetting(EntityPlayer player, TileMachineBase machineTile, NBTTagCompound data) {
        PacketCustom packet = new PacketCustom(NET_CHANNEL, 4);
        packet.writePos(machineTile.getPos());
        packet.writeNBTTagCompound(data);
        packet.sendToServer();
    }
    public static void sendConfig(EntityPlayer player) {
        PacketCustom packet = PacketConfigSync.getPacket();
        packet.sendToPlayer(player);
    }

}