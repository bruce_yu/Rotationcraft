package cn.tohsaka.factory.rotationcraft.network;

import cn.tohsaka.factory.librotary.packet.ICustomPacketHandler;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.api.IGuiPacketHandler;
import cn.tohsaka.factory.rotationcraft.config.PacketConfigSync;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.INetHandlerPlayClient;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;

/**
 * Created by covers1624 on 20/05/2017.
 */
public class ClientPacketHandler implements ICustomPacketHandler.IClientPacketHandler {

    @Override
    public void handlePacket(PacketCustom packet, Minecraft mc, INetHandlerPlayClient handler) {
        switch (packet.getType()) {
            case 1: {
                BlockPos pos = packet.readPos();
                TileEntity tile = mc.world.getTileEntity(pos);
                if (tile instanceof TileMachineBase) {
                    ((TileMachineBase) tile).readUpdatePacket(packet);
                } else {
                    System.out.println("Received Machine update packet for tile that is not a Machine... Pos: " + pos.toString());
                }
                break;
            }
            case 2: {
                BlockPos pos = packet.readPos();
                TileEntity tile = mc.world.getTileEntity(pos);
                if (tile instanceof IGuiPacketHandler) {
                    ((IGuiPacketHandler) tile).readGuiData(packet, packet.readBoolean());
                } else {
                    System.out.println("Received Gui update packet for tile that is not a Machine... Pos: " + pos.toString());
                }
                break;
            }
            case 127:{
                PacketConfigSync.readPacket(packet);
            }
        }
    }
}