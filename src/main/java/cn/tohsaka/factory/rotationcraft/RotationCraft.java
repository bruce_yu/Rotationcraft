package cn.tohsaka.factory.rotationcraft;

import cn.tohsaka.factory.rotationcraft.enet.NetworkTicker;
import cn.tohsaka.factory.rotationcraft.etc.Metrics;
import cn.tohsaka.factory.rotationcraft.init.Initializer;
import cn.tohsaka.factory.rotationcraft.props.Brandings;
import cn.tohsaka.factory.rotationcraft.proxy.CommonEventHandler;
import cn.tohsaka.factory.rotationcraft.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;

import javax.annotation.Nullable;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Mod(
        modid = RotationCraft.MOD_ID,
        name = RotationCraft.MOD_NAME,
        dependencies = "required-after:librotary"
)
public class RotationCraft{

    public static final String MOD_ID = "rc";
    public static final String MOD_NAME = "rotationcraft";
    public static final String VERSION = "1.0";
    public static String BUILDNUMBER = "0000";
    public static Logger logger = Logger.getLogger(RotationCraft.MOD_ID);

    @SidedProxy(clientSide="cn.tohsaka.factory.rotationcraft.proxy.ClientProxy", serverSide="cn.tohsaka.factory.rotationcraft.proxy.CommonProxy")
    public static CommonProxy proxy;
    public static Metrics metrics;

    @Mod.Instance(MOD_ID)
    public static RotationCraft INSTANCE;

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(CommonEventHandler.INSTANCE);
        Initializer.init();
        proxy.preInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.Init();
        Initializer.init2();
        metrics = new Metrics(13951);
    }

    @Mod.EventHandler
    public void postinit(FMLPostInitializationEvent event) {
        proxy.postInit();
        Initializer.posinit();
    }

    @Mod.EventHandler
    public void loadComplete(FMLLoadCompleteEvent event){
        proxy.loadComplete();
        Initializer.loadcomplete();
        Brandings.brandingTexts.add("Rotationcraft "+VERSION);
        Brandings.compile();
    }


    @Mod.EventHandler
    public void onServerStarted(FMLServerStartedEvent event){
        metrics.onServerStarted(event);
        NetworkTicker.init();
        ForgeChunkManager.setForcedChunkLoadingCallback(this, new ForgeChunkManager.LoadingCallback() {
            @Override
            public void ticketsLoaded(List<ForgeChunkManager.Ticket> list, World world) {

            }
        });
    }
    @Mod.EventHandler
    public void onServerStopped(FMLServerStoppedEvent event){
        metrics.onServerStopped(event);
    }

    public static HashMap<ResourceLocation,Block> blocks = new HashMap<>();
    public static HashMap<ResourceLocation,Item> items = new HashMap<>();
    public static HashMap<String, Fluid> fluids = new HashMap<String, Fluid>();

}
