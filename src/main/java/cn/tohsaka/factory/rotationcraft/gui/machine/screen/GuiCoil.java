package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerCoil;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementButtonManaged;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementTextField;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementTextFieldNumber;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.varable.GuiColor;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileCoil;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.List;

public class GuiCoil extends GuiMachinebase {
    TileCoil tile;
    public static String tex_path = "textures/gui/coil.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    public GuiCoil(TileCoil te, InventoryPlayer playerInv) {
        super(new ContainerCoil(te,playerInv), tex);
        tile = te;
    }

    private ElementTextField tf1;
    private ElementTextField tf2;
    private ElementButtonManaged save;
    private ElementDualScaled2 power;

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        power.setQuantity((int) Math.round(((double) tile.powerStorage.getPowerStored() / (double)tile.powerStorage.getPowerCapacity()) * 42F));
    }

    @Override
    public void initGui() {
        super.initGui();
        power = (ElementDualScaled2) addElement(new ElementDualScaled2(this,127,37,176,0).setMode(1).setTexture(btn_tex,256,256).setSize(42,42));
        tf1 = (ElementTextField) addElement(new ElementTextFieldNumber(this,68,21,30,12,5,String.valueOf(tile.powerStorage.getOutpuing().getTorque())).setValueRange(0,tile.isBedrockCoil()?8192:1024).setBackgroundColor(new GuiColor(0, 0, 0).getColor(),0,new GuiColor(115, 115, 115).getColor()));
        tf2 = (ElementTextField) addElement(new ElementTextFieldNumber(this,68,36,30,12,4,String.valueOf(tile.powerStorage.getOutpuing().getSpeed())).setValueRange(0,tile.isBedrockCoil()?8192:1024).setBackgroundColor(new GuiColor(0, 0, 0).getColor(),0,new GuiColor(115, 115, 115).getColor()));
        save = (ElementButtonManaged) addElement(new ElementButtonManaged(this, 67, 58, 32, 18, StringHelper.localize("info.cvt.gui.save")) {
            @Override
            public void onClick() {
                try {
                    int t = Integer.parseInt(tf1.getText());
                    int s = Integer.parseInt(tf2.getText());
                    tile.uploadOutput(new RotaryPower(t,s,0,0));
                    NetworkDispatcher.uploadingClientSetting(mc.player,tile);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });


    }

    @Override
    public void addTooltips(List<String> tooltip) {
        ElementBase element = getElementAtPosition(mouseX, mouseY);
        if(element!=null && element instanceof ElementDualScaled2){
            tooltip.add(StringHelper.localize("info.rc.coil.charge_torque")+": "+RotaryPower.formatNumber(tile.getChargingTorque())+"Nm");
            tooltip.add(StringHelper.localize("info.rc.coil.energy_stored")+": "+RotaryPower.formattedPower(tile.powerStorage.getPowerStored()));
            tooltip.add(StringHelper.localize("info.rc.coil.energy_maxium")+": "+RotaryPower.formattedPower(tile.powerStorage.getPowerCapacity()));
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        super.drawGuiContainerForegroundLayer(x, y);
        fontRenderer.drawString(StringHelper.localize("info.rc.waila.names.torque"), 38, 23, 0x404040);
        fontRenderer.drawString(StringHelper.localize("info.rc.waila.names.speed"), 38, 38, 0x404040);
    }
}
