package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileMiner;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerVoidMiner extends ContainerCore {

    public ContainerVoidMiner(TileMiner te, InventoryPlayer inv){
        super(te,inv);
        for(int i=0;i<3;i++){
            for(int j=0;j<9;j++){
                addSlotToContainer(new SlotRemoveOnly(te,i*9+j,8+j*18,17+i*18));
            }
        }
    }



}
