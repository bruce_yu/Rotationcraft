package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TilePulse;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerPulse extends ContainerCore {

    public ContainerPulse(TilePulse te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(0,item),te,0,125,16));
        addSlotToContainer(new SlotRemoveOnly(te,1,125,52));
    }



}