package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileComb;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerComb extends ContainerCore {

    public ContainerComb(TileComb te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(0,item),te,0,79,35));
    }



}
