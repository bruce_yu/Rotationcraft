package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerMicro;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileMicro;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class GuiMicro extends GuiMachinebase {
    public static String tex_path = "textures/gui/basic_gui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileMicro tile;
    private ElementFuelTank fuel;
    private FuelTank fuelTank;
    @Override
    public void initGui() {
        super.initGui();
        fuelTank = new FuelTank(tile.fuelTank.getCapacity(),"guitank");
        addElement(new ElementSlot(this,44,55));
        fuel = (ElementFuelTank) addElement(new ElementFuelTank(this,31,16,fuelTank).setType(1));
    }

    public GuiMicro(TileMicro te, InventoryPlayer playerInv) {
        super(new ContainerMicro(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockmicro.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        fuelTank.setFuel(tile.fuelTank.getFluidAmount());
        super.updateElementInformation();
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>30 && x<39 && y>15 && y<73){
            if(tile.fuelTank.getFluid() != null){
                List<String> tips = Arrays.asList(
                        StringHelper.localize("info.rc.fueltitle")+":"+tile.fuelTank.getFluid().getLocalizedName(),
                        StringHelper.localize("info.rc.fuelcap")+":"+String.valueOf(tile.fuelTank.getFluid().amount),
                        String.format(StringHelper.localize("info.rc.fuelcaptime"),Math.round(tile.fuelTank.getFluid().amount/8))
                );
                this.drawHoveringText(tips, x, y, fontRenderer);
            }
        }
    }


}
