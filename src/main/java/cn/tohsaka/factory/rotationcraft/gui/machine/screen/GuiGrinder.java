package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerGrinder;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileGrinder;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class GuiGrinder extends GuiMachinebase {
    public static String tex_path = "textures/gui/grinder.png";
    public static String gui_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    public TileGrinder tile;
    private ElementDualScaled2 lubricant;
    private ElementDualScaled2 progress;
    @Override
    public void initGui() {
        super.initGui();
        lubricant = (ElementDualScaled2) addElement(new ElementDualScaled2(this,24,21,176,71).setMode(2).setSize(8,55).setTexture(gui_tex,256,256));
        progress = (ElementDualScaled2) addElement(new ElementDualScaled2(this,99,35,176,14).setMode(1).setSize(24,16).setTexture(gui_tex,256,256));
    }

    public GuiGrinder(TileGrinder te, InventoryPlayer playerInv) {
        super(new ContainerGrinder(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockgrinder.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        lubricant.setQuantity(Math.round(((float)tile.fluidTank.getFluidAmount()) / ((float) tile.fluidTank.getCapacity())*55));
        progress.setQuantity(24-(int) Math.ceil(((float)tile.processRem/(float)tile.processMax) * 24F));
        super.updateElementInformation();
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>23 && x<32 && y>20 && y<76){
            if(tile.fluidTank.getFluid() != null){
                List<String> tips = Arrays.asList(
                        tile.fluidTank.getFluid().getLocalizedName(),
                        String.format("%dmB / %dmB",tile.fluidTank.getFluid().amount,tile.fluidTank.getCapacity())
                );
                this.drawHoveringText(tips, x, y, fontRenderer);
            }
        }
    }

}
