package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerFerm;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementTex;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFluidTank;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import cn.tohsaka.factory.rotationcraft.utils.TempetureUtils;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;

public class GuiFerm extends GuiMachinebase {
    public static String tex_path = "textures/gui/ferm.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileFerm tile;
    private ElementDualScaled temp;
    private ElementDualScaled progress;
    private ElementFluidTank tank1;
    @Override
    public void initGui() {
        super.initGui();
        temp = (ElementDualScaled) addElement(new ElementDualScaled(this,23,15).setMode(0).setBackground(false).setSize(11,56).setTexture(ElementTex.TEMP,11,56));
        temp.setQuantity(0);
        progress = (ElementDualScaled) addElement(new ElementDualScaled(this,80,35).setMode(1).setBackground(false).setSize(24,16).setTexture(ElementTex.ARROW,24,16));
        progress.setQuantity(0);


        tank1 = (ElementFluidTank) addElement(new ElementFluidTank(this,55,35,tile.fluidTank).setSize(16,16));
        tank1.setGauge(-1).setAlwaysShow(true).setVisible(true);
    }

    public GuiFerm(TileFerm te, InventoryPlayer playerInv) {
        super(new ContainerFerm(te,playerInv), tex);
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        super.updateElementInformation();
        temp.setQuantity(TempetureUtils.getMeterQuantity(tile.getTemp()));
        if(tile.isActive() && tile.processMax>0){
            progress.setQuantity(tile.getScaledProgress(24));
        }
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>22 && x<34 && y>14 && y<71){
            this.drawHoveringText(Arrays.asList(String.format("%.1f",tile.getTemp())+" "+ StringHelper.localize("info.rc.tempchar")), x, y, fontRenderer);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int x, int y) {
        super.drawGuiContainerBackgroundLayer(partialTicks, x, y);
        if(tile.getWorld().isBlockPowered(tile.getPos())){
            RenderHelper2.enableGUIStandardItemLighting();
            this.itemRender.renderItemAndEffectIntoGUI(new ItemStack(ItemMaterial.materialMap.get(50),1),guiLeft+154,guiTop+6);
        }
    }
}
