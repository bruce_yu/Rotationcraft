package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerVoidMiner;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileMiner;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiVoidMiner extends GuiMachinebase {
    public static String tex_path = "textures/gui/voidminer.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileMiner tile;
    private ElementDualScaled2 progress;
    @Override
    public void initGui() {
        super.initGui();
        progress = (ElementDualScaled2) addElement(new ElementDualScaled2(this,7,74,0,166).setMode(1).setBackground(false).setSize(161,5).setTexture(btn_tex,256,256));
        progress.setQuantity(0);
    }

    public GuiVoidMiner(TileMiner te, InventoryPlayer playerInv) {
        super(new ContainerVoidMiner(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockvoidminer.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        progress.setQuantity(Math.round(((Math.abs(tile.processRem - tile.processMax)) / ((float) tile.processMax)) * 161F));
    }
}
