package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerFract;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFuelTank;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFract;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class GuiFract extends GuiMachinebase {
    public static String tex_path = "textures/gui/fract.png";
    public static String gui_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    public TileFract tile;
    private ElementFuelTank fuel;
    private FuelTank fuelTank;
    private ElementDualScaled2 progress;
    @Override
    public void initGui() {
        super.initGui();
        fuelTank = new FuelTank(tile.fuelTank.getCapacity(),"guitank");
        fuel = (ElementFuelTank) addElement(new ElementFuelTank(this,138,13,fuelTank).setType(1));
        progress = (ElementDualScaled2) addElement(new ElementDualScaled2(this,63,24,176,0).setMode(1).setSize(32,40).setTexture(gui_tex,256,256));
    }

    public GuiFract(TileFract te, InventoryPlayer playerInv) {
        super(new ContainerFract(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockfract.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        progress.setQuantity(32-(int) Math.ceil(((float)tile.processRem/(float)tile.processMax) * 32F));
        fuelTank.setFuel(tile.fuelTank.getFluidAmount());
        super.updateElementInformation();
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>138 && x<144 && y>13 && y<68){
            if(tile.fuelTank.getFluid() != null){
                List<String> tips = Arrays.asList(
                        StringHelper.localize("info.rc.fueltitle")+":"+tile.fuelTank.getFluid().getLocalizedName(),
                        StringHelper.localize("info.rc.fuelcap")+":"+String.format("%dmB/%dmB",tile.fuelTank.getFluid().amount,tile.fuelTank.getCapacity())
                );
                this.drawHoveringText(tips, x, y, fontRenderer);
            }
        }
    }

}
