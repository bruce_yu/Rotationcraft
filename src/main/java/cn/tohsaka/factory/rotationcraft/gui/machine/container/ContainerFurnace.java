package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFurnace;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerFurnace extends ContainerCore {

    public ContainerFurnace(TileFurnace te, InventoryPlayer inv){
        super(te,inv);
        for(int i=0;i<3;i++){
            for(int j=0;j<4;j++){
                int slodid = i*4+j;
                addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(slodid,item),te,slodid,8+j*18,18+i*18));
            }
        }

        for(int i=0;i<3;i++){
            for(int j=0;j<4;j++){
                addSlotToContainer(new SlotRemoveOnly( te,i*4+j + 12,98+j*18,18+i*18));
            }
        }

    }



}
