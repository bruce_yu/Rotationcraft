package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerJet;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementButton;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileJet;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class GuiJet extends GuiMachinebase {
    public static String tex_path = "textures/gui/basic_gui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    public static String buttontex = RotationCraft.MOD_ID+":"+"textures/gui/rcelement/jetbutton.png";
    public TileJet tile;
    private ElementFuelTank fuel;
    private FuelTank fuelTank;
    private ElementButton button;
    @Override
    public void initGui() {
        super.initGui();
        fuelTank = new FuelTank(tile.fuelTank.getCapacity(),"guitank");
        addElement(new ElementSlot(this,44,55));
        button = (ElementButton) addElement(new ElementButton(this,70,28,36,18,36,0,36,0,buttontex).setid(0).setGuiManagedClicks(true));
        fuel = (ElementFuelTank) addElement(new ElementFuelTank(this,31,16,fuelTank).setType(1));
        if(tile.canAfterBurn()){
            button.setEnabled(true);
            button.setSheetX(tile.afterBurning?72:36);
            button.setHoverX(tile.afterBurning?72:36);
        }else{
            button.setSheetX(0);
            button.setDisabled();
        }
    }

    public GuiJet(TileJet te, InventoryPlayer playerInv) {
        super(new ContainerJet(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockjet.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        if(!tile.canAfterBurn()){
            button.setEnabled(false);
            button.setSheetX(0);
            button.setHoverX(36);
            button.setGuiManagedClicks(true);
        }else{
            button.setEnabled(true);
        }
        fuelTank.setFuel(tile.fuelTank.getFluidAmount());
        super.updateElementInformation();
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>30 && x<39 && y>15 && y<73){
            if(tile.fuelTank.getFluid() != null){
                List<String> tips = Arrays.asList(
                        StringHelper.localize("info.rc.fueltitle")+":"+tile.fuelTank.getFluid().getLocalizedName(),
                        StringHelper.localize("info.rc.fuelcap")+":"+String.valueOf(tile.fuelTank.getFluid().amount),
                        String.format(StringHelper.localize("info.rc.fuelcaptime"),Math.round(tile.fuelTank.getFluid().amount/(tile.afterBurning?160:80)))
                );
                this.drawHoveringText(tips, x, y, fontRenderer);
            }
        }
    }

    @Override
    public void handleIDButtonClick(int id, int mouseButton) {
        if(id == 0){
            tile.setAfterBurning(!tile.afterBurning);
            if(tile.afterBurning){
                button.setSheetX(72);
                button.setHoverX(72);
            }else{
                button.setSheetX(36);
                button.setHoverX(36);
            }
            NetworkDispatcher.uploadingClientSetting(this.mc.player,tile);
        }
    }
}
