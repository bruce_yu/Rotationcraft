package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBlast;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;

public class ContainerBlast extends ContainerCore {

    public ContainerBlast(TileBlast te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(0,item),te,0,26,16));
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(1,item),te,1,26,35));
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(2,item),te,2,26,54));

        for(int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                int slotid = i*3+j+3;
                addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(slotid,item),te,slotid,62+j*18,17+i*18));
            }
        }
        addSlotToContainer(new SlotRemoveOnly( te,12,148,17));
        addSlotToContainer(new SlotRemoveOnly( te,13,148,35));
        addSlotToContainer(new SlotRemoveOnly( te,14,148,53));

    }



}
