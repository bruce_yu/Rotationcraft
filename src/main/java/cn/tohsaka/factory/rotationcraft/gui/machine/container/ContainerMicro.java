package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileComb;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileMicro;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerMicro extends ContainerCore {

    public ContainerMicro(TileMicro te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(0,item),te,0,44,55));
    }



}
