package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileCVT;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;

public class ContainerCVT extends ContainerCore {
    public ContainerCVT(TileCVT te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(0,item),te,0,8,59));
    }
}
