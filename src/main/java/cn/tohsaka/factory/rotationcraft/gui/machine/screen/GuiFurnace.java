package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerFurnace;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFurnace;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiFurnace extends GuiMachinebase {
    public static String tex_path = "textures/gui/furnace.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileFurnace tile;
    private ElementDualScaled2 progress;
    @Override
    public void initGui() {
        super.initGui();
        progress = (ElementDualScaled2) addElement(new ElementDualScaled2(this,78,17,176,0).setMode(1).setBackground(false).setSize(20,54).setTexture(btn_tex,256,256));
        progress.setQuantity(0);
    }

    public GuiFurnace(TileFurnace te, InventoryPlayer playerInv) {
        super(new ContainerFurnace(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockfurnace.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        progress.setQuantity(Math.round(((Math.abs(tile.processRem - tile.processMax)) / ((float) tile.processMax)) * 20F));
    }
}
