package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerCorner;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementButton;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileCorner;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

import java.util.HashMap;
import java.util.Map;

public class GuiCorner extends GuiMachinebase {
    IInventory playerinventory;
    TileCorner tile;
    public static String tex_path = "textures/gui/corner.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    protected int mouseX = 0;
    protected int mouseY = 0;
    public GuiCorner(TileCorner te, InventoryPlayer playerInv) {
        super(new ContainerCorner(playerInv),tex);
        tile = te;
        NAME = "gui.corner.title";
        playerinventory = playerInv;
    }
    public Map<Integer,ElementButton> buttons = new HashMap<>();
    @Override
    public void initGui() {
        super.initGui();
        addElement(new ElementButton(this,25,20,17,17,177,37,177,55,177,55,btn_tex).setText("D").setid(0));
        addElement(new ElementButton(this,44,20,17,17,177,37,177,55,177,55,btn_tex).setText("U").setid(1));
        addElement(new ElementButton(this,25,39,17,17,177,37,177,55,177,55,btn_tex).setText("N").setid(2));
        addElement(new ElementButton(this,44,39,17,17,177,37,177,55,177,55,btn_tex).setText("S").setid(3));
        addElement(new ElementButton(this,25,58,17,17,177,37,177,55,177,55,btn_tex).setText("W").setid(4));
        addElement(new ElementButton(this,44,58,17,17,177,37,177,55,177,55,btn_tex).setText("E").setid(5));

        addElement(new ElementButton(this,115,20,17,17,177,37,177,55,177,55,btn_tex).setText("D").setid(6));
        addElement(new ElementButton(this,134,20,17,17,177,37,177,55,177,55,btn_tex).setText("U").setid(7));
        addElement(new ElementButton(this,115,39,17,17,177,37,177,55,177,55,btn_tex).setText("N").setid(8));
        addElement(new ElementButton(this,134,39,17,17,177,37,177,55,177,55,btn_tex).setText("S").setid(9));
        addElement(new ElementButton(this,115,58,17,17,177,37,177,55,177,55,btn_tex).setText("W").setid(10));
        addElement(new ElementButton(this,134,58,17,17,177,37,177,55,177,55,btn_tex).setText("E").setid(11));

        for(ElementBase element:elements){
            if(element instanceof ElementButton){
                ((ElementButton)element).setGuiManagedClicks(true);
                buttons.put(((ElementButton) element).getid(),(ElementButton) element);
            }
        }
        revalidate();
    }

    public void revalidate(){
        EnumFacing facing = tile.getFacing();
        EnumFacing facing1 = tile.getFacingIn();
        for(int i=0;i<12;i++){
            buttons.get(i).setEnabled(true);
        }
        buttons.get(facing.ordinal()).setDisabled();
        buttons.get(facing1.ordinal()+6).setDisabled();
    }

    @Override
    public void handleIDButtonClick(int id, int mouseButton) {
        if(id>=6){
            if(validateFacing(tile.getFacing(),EnumFacing.VALUES[id-6])){
                tile.setFacingIn(EnumFacing.VALUES[id-6]);
                tile.setFacing(tile.getFacing());
            }
        }else{
            if(Math.abs(tile.getFacingIn().ordinal()-id)<=1){
                EnumFacing facingin;
                if(EnumFacing.VALUES[id].ordinal()<=1){
                    facingin = EnumFacing.NORTH;
                }else{
                    facingin = EnumFacing.VALUES[id].rotateY();
                }
                tile.setFacingIn(facingin);
            }
            tile.setFacing(EnumFacing.VALUES[id]);
        }
        NetworkDispatcher.uploadingClientSetting(mc.player,tile);
        revalidate();
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        boolean u = fontRenderer.getUnicodeFlag();
        fontRenderer.setUnicodeFlag(false);
        fontRenderer.drawString(StringHelper.localize("gui.corner.output"),26,6,0,false);
        fontRenderer.drawString(StringHelper.localize("gui.corner.input"),116,6,0,false);
        fontRenderer.setUnicodeFlag(u);
    }

    private static boolean validateFacing(EnumFacing a,EnumFacing b){
        if(a.ordinal()<=1 && b.ordinal()<=1){// up - down
            return false;
        }
        if(a.ordinal() == b.ordinal()){
            return false;
        }
        if(a == EnumFacing.NORTH && b == EnumFacing.SOUTH){
            return false;
        }
        if(a == EnumFacing.SOUTH && b == EnumFacing.NORTH){
            return false;
        }
        if(a == EnumFacing.WEST && b == EnumFacing.EAST){
            return false;
        }
        if(a == EnumFacing.EAST && b == EnumFacing.WEST){
            return false;
        }
        return true;
    }
}
