package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerExtractor;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileExtractor;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.List;

public class GuiExtractor extends GuiMachinebase {
    public static String tex_path = "textures/gui/extractor.png";
    public static String gui_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    private ElementDualScaled2 step1;
    private ElementDualScaled2 step2;
    private ElementDualScaled2 step3;
    private ElementDualScaled2 step4;


    public TileExtractor tile;
    @Override
    public void initGui() {
        super.initGui();
        step1 = (ElementDualScaled2) addElement(new ElementDualScaled2(this,29,34,176,48).setMode(0).setSize(10,16).setTexture(gui_tex,256,256));
        step1.setQuantity(0);
        step2 = (ElementDualScaled2) addElement(new ElementDualScaled2(this,63,35,186,48).setMode(0).setSize(14,14).setTexture(gui_tex,256,256));
        step2.setQuantity(0);
        step3 = (ElementDualScaled2) addElement(new ElementDualScaled2(this,99,35,200,48).setMode(0).setSize(14,14).setTexture(gui_tex,256,256));
        step3.setQuantity(0);

        step4 = (ElementDualScaled2) addElement(new ElementDualScaled2(this,133,34,176,64).setMode(0).setSize(17,16).setTexture(gui_tex,256,256));
        step4.setQuantity(0);

    }

    public GuiExtractor(TileExtractor te, InventoryPlayer playerInv) {
        super(new ContainerExtractor(te,playerInv), tex);
        tile = te;
        setGuiSize(175,165);

        machine = tile;
        showtab = true;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        super.updateElementInformation();
        step1.setQuantity((int) Math.ceil(((float)tile.step1/(float)tile.tickmax) * 16F));
        step2.setQuantity((int) Math.ceil(((float)tile.step2/(float)tile.tickmax) * 14F));
        step3.setQuantity((int) Math.ceil(((float)tile.step3/(float)tile.tickmax) * 14F));
        step4.setQuantity((int) Math.ceil(((float)tile.step4/(float)tile.tickmax) * 16F));
    }


    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);

    }

    public void drawTabElements(){
        RotaryPower currentPower = machine.powerMachine.getPower();
        for(int i=0;i<4;i++){
            RotaryPower power = TileExtractor.steps[i];
            float scale1 = Math.min(1,(float) (currentPower.getWatt() / power.getWatt()));
            float scale2 = Math.min(1,currentPower.getTorque() / power.getTorque());
            float scale3 = Math.min(1,currentPower.getSpeed() / power.getSpeed());
            drawPowerBar(String.format("Step%d",i+1),5+35*i,scale1,scale2,scale3);
        }
        //fontRenderer.drawString(String.format("%d,%d",mouseX,mouseY),0,-20,0xffffff);
    }

    public void addTooltips(List<String> tooltip) {

        ElementBase element = getElementAtPosition(mouseX, mouseY);

        if (element != null && element.isVisible()) {
            element.addTooltip(tooltip);
        }else{
            if(machine!=null && mouseX>=-33 && mouseX <-6){
                RotaryPower currentPower = machine.powerMachine.getPower();
                if(mouseY>=15 && mouseY <46){
                    addTooltipsStep(0,currentPower,tooltip);
                }

                if(mouseY>=50 && mouseY <81){
                    addTooltipsStep(1,currentPower,tooltip);
                }

                if(mouseY>=85 && mouseY <116){
                    addTooltipsStep(2,currentPower,tooltip);
                }

                if(mouseY>=120 && mouseY <151){
                    addTooltipsStep(3,currentPower,tooltip);
                }
            }
        }


        if(mouseX > 9 && mouseX <17 && mouseY > 32 && mouseY < 51 && tile.drill>0){
            tooltip.add(String.format(StringHelper.localize("info.rc.bounds"),(tile.drill==1)?20:300)+"%");
        }
    }

    private static void addTooltipsStep(int step,RotaryPower currPower,List<String> tooltip){
        RotaryPower power = TileExtractor.steps[step];
        tooltip.add("Step"+(step+1)+" "+StringHelper.localize("info.rc.need")+" / "+StringHelper.localize("info.rc.curr"));
        tooltip.add(String.format("%s : %s / %s",StringHelper.localize("info.rc.waila.names.power"),power.getFormattedWatt(),currPower.getFormattedWatt()));
        tooltip.add(String.format("%s : %s / %s",StringHelper.localize("info.rc.waila.names.torque"),power.getFormattedNm(),currPower.getFormattedNm()));
        tooltip.add(String.format("%s : %s / %s",StringHelper.localize("info.rc.waila.names.speed"),power.getFormattedSpeed(),currPower.getFormattedSpeed()));
    }

    public void drawPowerBar(String text,int y,float scale1,float scale2,float scale3){
        fontRenderer.drawString(text,9,y,0);
        drawPowerbar4At(10,11+y,scale1);
        drawPowerbar4At(20,11+y,scale2);
        drawPowerbar4At(30,11+y,scale3);
    }

    public void drawPowerbar4At(int x,int y,float scale){
        GlStateManager.color(255, 255, 255, 1);
        mc.renderEngine.bindTexture(tabTex);
        drawTexturedModalRect(x, y, 82, 16, 6, 27);
        int yy = Math.round(scale * 20F);
        drawTexturedModalRect(x, y+(21-yy), 89, 16+(21-yy),6, yy);
    }

    public static String appendStr(String str,int length){
        while (str.length()<length){
            str = " "+str;
        }
        return str;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int x, int y) {
        super.drawGuiContainerBackgroundLayer(partialTicks, x, y);
        bindTexture(tex);
        if(tile.drill>0){
            drawSizedTexturedModalRect(guiLeft+10,guiTop+33,tile.drill==1?182:190,142,6,18,256,256);
        }
    }
}
