package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerPfe;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import cn.tohsaka.factory.rotationcraft.tiles.power.TilePfe;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class GuiPfe extends GuiMachinebase {
    public static String tex_path = "textures/gui/basic_gui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TilePfe tile;
    private ElementFuelTank fuel;
    private ElementFuelTank fuelrs;
    private ElementFuelTank fuelcata;
    @Override
    public void initGui() {
        super.initGui();
        addElement(new ElementSlot(this,49,55));
        addElement(new ElementSlot(this,69,55));
        addElement(new ElementSlot(this,89,55));
        fuel = (ElementFuelTank) addElement(new ElementFuelTank(this,16,16,tile.fuelTank));
        fuelrs = (ElementFuelTank) addElement(new ElementFuelTank(this,25,16,tile.fuelTankrs).setType(1));
        fuelcata = (ElementFuelTank) addElement(new ElementFuelTank(this,34,16,tile.fuelTankcata).setType(4));
    }

    public GuiPfe(TilePfe te, InventoryPlayer playerInv) {
        super(new ContainerPfe(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.pfe.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        super.updateElementInformation();
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>15 && x<42 && y>15 && y<71){
            List<String> tips = Arrays.asList(
                    StringHelper.localize("info.rc.fuelcap")+":"+String.valueOf(tile.fuelTank.getFuel()),
                    StringHelper.localize("tile.redstoneDust.name")+":"+String.valueOf(tile.fuelTankrs.getFuel()),
                    StringHelper.localize("info.rc.fuelcata")+":"+String.valueOf(tile.fuelTankcata.getFuel()),
                    String.format(StringHelper.localize("info.rc.fuelcaptime"),
                            Math.min(Math.min(tile.fuelTank.getFuel(),tile.fuelTankrs.getFuel()),tile.fuelTankcata.getFuel())
                            /20)
            );
            this.drawHoveringText(tips, x, y, fontRenderer);
        }
    }
}
