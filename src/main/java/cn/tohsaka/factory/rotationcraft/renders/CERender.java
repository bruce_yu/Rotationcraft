package cn.tohsaka.factory.rotationcraft.renders;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelPerformance;
import cn.tohsaka.factory.rotationcraft.tiles.power.TilePfe;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;

public class CERender extends TileEntitySpecialRenderer<TilePfe> {
    private ModelPerformance model= new ModelPerformance();
    public CERender() {
        super();
    }

    @Override
    public void render(TilePfe te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        {
            this.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/perftex.png"));


            GlStateManager.pushMatrix();
            GlStateManager.enableRescaleNormal();
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.translate(x, y, z);
            GlStateManager.scale(1.0F, -1.0F, -1.0F);
            GlStateManager.translate(0.5F, 0.5F, 0.5F);
            GlStateManager.translate(0F,-2F,-1F);
            RenderUtils.rotate(te.getFacing(), 270, 90, 180, 0);
        }
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        model.renderAll(te,null,te.angle,0F);

        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }
}
