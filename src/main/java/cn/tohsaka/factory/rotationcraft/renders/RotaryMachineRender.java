package cn.tohsaka.factory.rotationcraft.renders;

import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBEMiner;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;

public class RotaryMachineRender extends TileEntitySpecialRenderer<TileModelMachine> {
    @Override
    public boolean isGlobalRenderer(TileModelMachine te) {
        if(te instanceof TileBEMiner){
            return true;
        }
        return super.isGlobalRenderer(te);
    }

    @Override
    public void render(TileModelMachine te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        te.renderTileEntityAt(x,y,z);
    }
}
