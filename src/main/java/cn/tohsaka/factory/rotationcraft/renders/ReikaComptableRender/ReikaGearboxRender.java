package cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelWorm;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.gearbox.*;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.helper.ReikaModHelper;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileWorm;
import cn.tohsaka.factory.rotationcraft.utils.FluidUtils;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;
import org.lwjgl.opengl.GL11;

import static net.minecraft.client.renderer.texture.TextureMap.LOCATION_BLOCKS_TEXTURE;

public class ReikaGearboxRender extends TileEntitySpecialRenderer<TileGearbox> {

    private ModelGearbox GearboxModel = new ModelGearbox();
    private ModelGearbox4 GearboxModel4 = new ModelGearbox4();
    private ModelGearbox8 GearboxModel8 = new ModelGearbox8();
    private ModelGearbox16 GearboxModel16 = new ModelGearbox16();
    private ModelHighGear GearboxModel256 = new ModelHighGear();
    private ModelWorm WormGear = new ModelWorm();
    @Override
    public void render(TileGearbox te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        ReikaModHelper.setupGL(te,x,y,z);

        if(te instanceof TileWorm){
            ReikaModHelper.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,String.format("textures/render/Transmission/shaft/shafttex.png",te.getMetadata())));
            WormGear.renderAll(te,te.angleInput,te.angleOutput);
        }else {
            ReikaModHelper.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,String.format("textures/render/Transmission/Gear/geartex%d.png",te.getMetadata())));
            if(!te.isDownshift()){
                GlStateManager.rotate(180,0,1,0);
            }
            float angle = (!te.isDownshift())?-te.angleInput:te.angleOutput;
            float angle2 = (!te.isDownshift())?-te.angleOutput:te.angleInput;
            switch(te.gearratio) {
                case 2:
                    GearboxModel.renderAll(te, angle,angle2);
                    break;
                case 4:
                    GearboxModel4.renderAll(te, angle,angle2);
                    break;
                case 8:
                    GearboxModel8.renderAll(te, angle,angle2);
                    break;
                case 16:
                    GearboxModel16.renderAll(te, angle,angle2);
                    break;
                case 256:
                    ReikaModHelper.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,String.format("textures/render/Transmission/Gear/highgeartex.png",te.getMetadata())));

                    GearboxModel256.renderAll(te,null,angle);
            }
        }



        ReikaModHelper.closeGL(te,x,y,z);


        GlStateManager.pushMatrix();


        GL11.glEnable(GL11.GL_BLEND);

        GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GlStateManager.translate(x, y, z);
        bindTexture(LOCATION_BLOCKS_TEXTURE);
        renderFluid(te);

        GL11.glDisable(GL11.GL_BLEND);
        GlStateManager.popMatrix();

    }

    private void renderFluid(TileGearbox te)
    {
        float maxscale = 0.0625F * 4.5F;
        FluidStack fluid = te.lubricantTank.getFluid();
        if(fluid==null || fluid.getFluid()==null || fluid.amount==0){
            return;
        }

        float scale = maxscale/1000*fluid.amount;

        if(scale > 0.0f)
        {
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder renderer = tessellator.getBuffer();
            TextureAtlasSprite sprite = RenderHelper2.getTexture(fluid.getFluid().getStill(fluid));
            if(sprite == null) {
                return;
            }
            RenderHelper2.disableStandardItemLighting();


            renderer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);

            float u1 = sprite.getMinU();
            float v1 = sprite.getMinV();
            float u2 = sprite.getMaxU();
            float v2 = sprite.getMaxV();

            float margin = 1f;
            float offset = 0.0625f;

            int color = FluidUtils.getLiquidColorWithBiome(fluid);

            float r = FluidUtils.getRed(color);
            float g = FluidUtils.getGreen(color);
            float b = FluidUtils.getBlue(color);
            float a = 0.9F;
            int TANK_THICKNESS = 0;
            // Top
            renderer.pos(offset, scale, offset).tex(u1, v1).color(r, g, b, a).endVertex();
            renderer.pos(offset, scale, 0.9375).tex(u1, v2).color(r, g, b, a).endVertex();
            renderer.pos(0.9375, scale, 0.9375).tex(u2, v2).color(r, g, b, a).endVertex();
            renderer.pos(0.9375, scale, offset).tex(u2, v1).color(r, g, b, a).endVertex();
            // Bottom
/*            renderer.pos(margin - TANK_THICKNESS, TANK_THICKNESS, TANK_THICKNESS + offset).tex(u2, v1).color(r, g, b, a).endVertex();
            renderer.pos(margin - TANK_THICKNESS, TANK_THICKNESS, margin - TANK_THICKNESS).tex(u2, v2).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, TANK_THICKNESS, margin - TANK_THICKNESS).tex(u1, v2).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, TANK_THICKNESS, TANK_THICKNESS + offset).tex(u1, v1).color(r, g, b, a).endVertex();

            // Sides
            //NORTH
            renderer.pos(TANK_THICKNESS + offset, scale + TANK_THICKNESS, margin - TANK_THICKNESS).tex(u1, v1).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, TANK_THICKNESS, margin - TANK_THICKNESS).tex(u1, v2).color(r, g, b, a).endVertex();
            renderer.pos(margin - TANK_THICKNESS, TANK_THICKNESS, margin - TANK_THICKNESS).tex(u2, v2).color(r, g, b, a).endVertex();
            renderer.pos(margin - TANK_THICKNESS, scale + TANK_THICKNESS, margin - TANK_THICKNESS).tex(u2, v1).color(r, g, b, a).endVertex();

            //SOUTH
            renderer.pos(margin - TANK_THICKNESS, scale + TANK_THICKNESS, TANK_THICKNESS + offset).tex(u2, v1).color(r, g, b, a).endVertex();
            renderer.pos(margin - TANK_THICKNESS, TANK_THICKNESS, TANK_THICKNESS + offset).tex(u2, v2).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, TANK_THICKNESS, TANK_THICKNESS + offset).tex(u1, v2).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, scale + TANK_THICKNESS, TANK_THICKNESS + offset).tex(u1, v1).color(r, g, b, a).endVertex();

            //WEAST
            renderer.pos(margin - TANK_THICKNESS, scale + TANK_THICKNESS, margin - TANK_THICKNESS).tex(u2, v1).color(r, g, b, a).endVertex();
            renderer.pos(margin - TANK_THICKNESS, TANK_THICKNESS, margin - TANK_THICKNESS).tex(u2, v2).color(r, g, b, a).endVertex();
            renderer.pos(margin - TANK_THICKNESS, TANK_THICKNESS, TANK_THICKNESS + offset).tex(u1, v2).color(r, g, b, a).endVertex();
            renderer.pos(margin - TANK_THICKNESS, scale + TANK_THICKNESS, TANK_THICKNESS + offset).tex(u1, v1).color(r, g, b, a).endVertex();

            //EAST
            renderer.pos(TANK_THICKNESS + offset, scale + TANK_THICKNESS, TANK_THICKNESS + offset).tex(u1, v1).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, TANK_THICKNESS, TANK_THICKNESS + offset).tex(u1, v2).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, TANK_THICKNESS, margin - TANK_THICKNESS).tex(u2, v2).color(r, g, b, a).endVertex();
            renderer.pos(TANK_THICKNESS + offset, scale + TANK_THICKNESS, margin - TANK_THICKNESS).tex(u2, v1).color(r, g, b, a).endVertex();
*/
            tessellator.draw();

            RenderHelper2.enableStandardItemLighting();
        }
    }
}
