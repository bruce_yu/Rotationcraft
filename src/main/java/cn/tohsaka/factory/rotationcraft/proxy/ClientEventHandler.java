package cn.tohsaka.factory.rotationcraft.proxy;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.utils.TextureManager;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
public class ClientEventHandler {
    public static ClientEventHandler INSTANCE = new ClientEventHandler();
    @SubscribeEvent
    public void handleTextureStitchPreEvent(TextureStitchEvent.Pre event)
    {
        RotationCraft.logger.info("preparing textures");
        TextureManager.registerTextures(event.getMap());
    }
}
