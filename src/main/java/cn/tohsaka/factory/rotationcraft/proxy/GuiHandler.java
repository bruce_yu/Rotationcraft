package cn.tohsaka.factory.rotationcraft.proxy;

import cn.tohsaka.factory.rotationcraft.api.ICustomGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.book.GuiGuide;
import cn.tohsaka.factory.rotationcraft.items.ItemFilter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
    public static final int BLOCK_GUI = 954;
    public static final int FILTER_GUI = 350;
    public static final int GUIDE_BOOK = 802;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID){
            case BLOCK_GUI:
                {
                    TileEntity tile = world.getTileEntity(new BlockPos(x, y, z));
                    if (tile instanceof IGuiProvider){
                        return ((IGuiProvider) tile).getGuiServer(player);
                    }
                }
                return null;
            case FILTER_GUI:
                if(player.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemFilter){
                    return ((ItemFilter)player.getHeldItem(EnumHand.MAIN_HAND).getItem()).getGuiServer(player);
                }
                return null;
            default:
                {
                    TileEntity tile = world.getTileEntity(new BlockPos(x, y, z));
                    if (tile instanceof ICustomGuiProvider){
                        return ((ICustomGuiProvider) tile).getGuiServer(player,ID);
                    }
                }
                return null;
        }
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID){
            case BLOCK_GUI:
                {
                    TileEntity tile = world.getTileEntity(new BlockPos(x, y, z));
                    if (tile instanceof IGuiProvider)
                        return ((IGuiProvider) tile).getGuiClient(player);
                }
                return null;
            case GUIDE_BOOK:
                return new GuiGuide();

            case FILTER_GUI:
                if(player.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemFilter){
                    return ((ItemFilter)player.getHeldItem(EnumHand.MAIN_HAND).getItem()).getGuiClient(player);
                }
                return null;
            default:
                {
                    TileEntity tile = world.getTileEntity(new BlockPos(x, y, z));
                    if (tile instanceof ICustomGuiProvider){
                        return ((ICustomGuiProvider) tile).getGuiClient(player,ID);
                    }
                }
                return null;
        }
    }

}
