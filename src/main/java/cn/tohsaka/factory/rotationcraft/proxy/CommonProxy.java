package cn.tohsaka.factory.rotationcraft.proxy;


import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.network.ServerPacketHandler;
import cn.tohsaka.factory.rotationcraft.worldgen.RcWorldGen;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy {
    static {
        FluidRegistry.enableUniversalBucket();
    }
    public void preInit(){
        PacketCustom.assignHandler(NetworkDispatcher.NET_CHANNEL, new ServerPacketHandler());
        NetworkRegistry.INSTANCE.registerGuiHandler(RotationCraft.INSTANCE, new GuiHandler());
    }
    public void Init(){
        Recipes.init();
    }
    public void postInit(){
        GameRegistry.registerWorldGenerator(new RcWorldGen(),1);
    }
    public void loadComplete(){

    }
}
