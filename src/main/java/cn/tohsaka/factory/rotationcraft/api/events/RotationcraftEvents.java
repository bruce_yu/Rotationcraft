package cn.tohsaka.factory.rotationcraft.api.events;

import cn.tohsaka.factory.rotationcraft.book.BookProvider;
import net.minecraftforge.fml.common.eventhandler.Event;

public class RotationcraftEvents {
    public static class BookIndexed extends Event {
        public BookProvider.Startpage startpage;
        public BookIndexed(BookProvider.Startpage page){
            this.startpage = page;
        }
    }
    public static class BookReadPage extends Event {
        public String file;
        public BookProvider.BookPage page;
        public BookReadPage(String f){
            this.file = f;
        }


        public void setPage(BookProvider.BookPage p) {
            page = p;
        }
    }
}
