package cn.tohsaka.factory.rotationcraft.api;

import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import net.minecraft.client.model.ModelBase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IDynamicTextureTile {
    @SideOnly(Side.CLIENT)
    public abstract ModelBase getModel();

    @SideOnly(Side.CLIENT)
    public abstract String getTexName();
}
