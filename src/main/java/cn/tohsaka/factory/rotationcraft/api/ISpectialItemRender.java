package cn.tohsaka.factory.rotationcraft.api;

public interface ISpectialItemRender {
    abstract void renderItemSpectial();
}
