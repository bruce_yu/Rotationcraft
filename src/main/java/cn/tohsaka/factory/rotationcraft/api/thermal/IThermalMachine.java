package cn.tohsaka.factory.rotationcraft.api.thermal;

public interface IThermalMachine {
    abstract boolean canDamage();
    abstract float getDamagedTemp();
    abstract void onDamage();
    abstract float getTemp();
    abstract void setTemp(Float thermal);
    abstract void addTemp(Float thermal);
    abstract boolean isTempSource();
}
