package cn.tohsaka.factory.rotationcraft.api;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockJet;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface IUpgradable {
    abstract boolean canUpgrade(ItemStack stack);
    abstract boolean installUpgrade(ItemStack stack, EntityPlayer playerMP);

    default void readUpgradeFromNBT(NBTTagCompound tag){
        if(this instanceof TileMachineBase){
            if(tag.hasKey("upgrades")){
                NBTTagList list = tag.getTagList("upgrades",Constants.NBT.TAG_COMPOUND);
                ((TileMachineBase)this).upgrades.clear();
                for(NBTBase tt:list){
                    ((TileMachineBase)this).upgrades.add(new ItemStack((NBTTagCompound) tt));
                }
            }
        }
    }

    default NBTTagCompound writeUpgradeToNBT(NBTTagCompound tag){
        if(this instanceof TileMachineBase){
            NBTTagList list = new NBTTagList();
            for(ItemStack stack:((TileMachineBase)this).upgrades){
                list.appendTag(stack.writeToNBT(new NBTTagCompound()));
            }
            tag.setTag("upgrades",list);
        }
        return tag;
    }
    default boolean hasUpgrade(ItemStack stack){
        if(this instanceof TileMachineBase) {
            return ((TileMachineBase) this).upgrades.stream().anyMatch(ss -> stack.isItemEqual(ss));
        }
        return false;
    }

    default void dropUpgrades(World world, BlockPos pos, List<ItemStack> upgrades){
        for(ItemStack stack:upgrades){
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(), stack.copy()));
        }
        upgrades.clear();
    }
    default void dropUpgrades(World world, BlockPos pos, ItemStack upgrade){
        world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(), upgrade.copy()));
        List<ItemStack> u = ((TileMachineBase) this).upgrades.stream().filter( stack -> !upgrade.isItemEqual(stack)).collect(Collectors.toList());
        ((TileMachineBase) this).upgrades.clear();
        ((TileMachineBase) this).upgrades.addAll(u);
    }
}
