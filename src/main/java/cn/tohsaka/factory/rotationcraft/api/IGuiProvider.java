package cn.tohsaka.factory.rotationcraft.api;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;

public interface IGuiProvider {
    abstract Object getGuiClient(EntityPlayer player);

    abstract Object getGuiServer(EntityPlayer player);
}
