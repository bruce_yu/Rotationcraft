package cn.tohsaka.factory.rotationcraft.api;

import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IModelProvider {
    @SideOnly(Side.CLIENT)
    public RotaryModelBase getModel();
    @SideOnly(Side.CLIENT)
    public String getTexName();

    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity,float partialTicks);

    @SideOnly(Side.CLIENT)
    public void beginItemRender();

}
