package cn.tohsaka.factory.rotationcraft.blocks.machine;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.power.IBuildOptimizer;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemBlockModelBased;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockMachineBase;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileMagnetizer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;

@GameInitializer(after = BlockFerm.class)
public class BlockMagnetizer extends BlockMachineBase implements IModelRegister, ITileEntityProvider, IBuildOptimizer {

    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockmagnetizer");
    public ItemBlock itemBlock;

    public BlockMagnetizer() {
        this.setLightOpacity(0);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        //setCreativeTab(RotationCraft.TAB_BLOCK);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlockModelBased(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        setDefaultState();
    }

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockMagnetizer());
        //RecipeHelper.addShapedRecipe(new ItemStack(RotationCraft.INSTANCE.blocks.get(NAME)),"A A","ABA","ACA",'A', ItemMaterial.getStackById(37),'B', ItemMaterial.getStackById(13),'C', ItemMaterial.getStackById(2));
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileMagnetizer();
    }


    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }


    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntity tileEntity = worldIn.getTileEntity(pos);
        if(tileEntity instanceof TileMagnetizer){
            return ((TileMagnetizer)tileEntity).playerInteract(playerIn,hand,facing);
        }
        return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
    }
}
