package cn.tohsaka.factory.rotationcraft.blocks.transmits;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.power.IBuildOptimizer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemBlockAxls;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileAxls;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

@GameInitializer(after = BlockBase.class)
public class BlockAxls extends BlockBase implements IModelRegister, ITileEntityProvider, IBuildOptimizer {

    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockaxls");
    public ItemBlockAxls itemBlock;
    public Map<Integer,BlockAxls> blocks = new HashMap<>();

    public BlockAxls(Material material) {
        super(material);
    }
    public BlockAxls() {
        super(Material.ROCK);
        this.setLightOpacity(0);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        //setCreativeTab(RotationCraft.TAB_BLOCK);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlockAxls(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        setDefaultState();
    }

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockAxls());
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "hsla"));
        ModelLoader.setCustomModelResourceLocation(itemBlock, 1, new ModelResourceLocation(getRegistryName(), "diamond"));
        ModelLoader.setCustomModelResourceLocation(itemBlock, 2, new ModelResourceLocation(getRegistryName(), "bedrock"));
    }

    @Override
    public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items) {
        for(int i: VARIANT.getAllowedValues()){
            items.add(itemBlock.setDefaultTag(new ItemStack(this, 1, i)));
        }
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileAxls();
    }

    @Override
    public int getMaxmeta() {
        return 2;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

}
