package cn.tohsaka.factory.rotationcraft.blocks.transfer.storage;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

import static net.minecraft.client.renderer.texture.TextureMap.LOCATION_BLOCKS_TEXTURE;

public class RenderTank extends TileEntitySpecialRenderer<TileTank> {
    public ModelTank model = new ModelTank();
    private static RenderTank renderTank;
    public static RenderTank INSTANCE(){
        if(renderTank==null){
            renderTank = new RenderTank();
        }
        return renderTank;
    }
    @Override
    public void render(TileTank te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        super.render(te, x, y, z, partialTicks, destroyStage, alpha);
        renderTileEntityAt(te,x,y,z);
    }

    @SideOnly(Side.CLIENT)
    public void renderTileEntityAt(TileTank tile,double x, double y, double z){
        {
            RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/combtex.png"));
            GlStateManager.pushMatrix();
            GlStateManager.enableRescaleNormal();
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.translate(x, y, z);
            GlStateManager.scale(1.0F, -1.0F, -1.0F);
            GlStateManager.translate(0.5F, 0.5F, 0.5F);
            GlStateManager.translate(0F,-2F,-1F);
        }

        model.renderAll(tile,null,0F,0F);

        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);


        GlStateManager.pushMatrix();
        GL11.glEnable(GL11.GL_BLEND);
        GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GlStateManager.translate(x, y, z);
        RenderHelper2.bindTexture(LOCATION_BLOCKS_TEXTURE);
        //GlStateManager.translate(0.5, -1, 0.5);
        model.renderFluid(tile);
        GL11.glDisable(GL11.GL_BLEND);
        GlStateManager.popMatrix();
    }
}
