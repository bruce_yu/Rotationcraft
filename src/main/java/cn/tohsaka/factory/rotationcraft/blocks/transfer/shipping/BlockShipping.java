package cn.tohsaka.factory.rotationcraft.blocks.transfer.shipping;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.storage.TileTank;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;

@GameInitializer(after = BlockBase.class)
public class BlockShipping extends BlockBase implements ITileEntityProvider, IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID, "blockshipping");

    public BlockShipping() {
        super(Material.IRON);
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlock(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
    }

    public static ItemBlock itemBlock;

    public static void init() {
        RotationCraft.INSTANCE.blocks.put(NAME, new BlockShipping());
        RecipeHelper.addShapedRecipe(new ItemStack(itemBlock),"ABA","ACA","AAA",'A', ItemMaterial.getStackById(0),'B', ItemMaterial.getStackById(16),'C',new ItemStack(Blocks.CHEST));
    }


    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityShipping();
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        TileEntityShipping tile = (TileEntityShipping)world.getTileEntity(pos);
        ItemStack stack = new ItemStack(this,1);
        stack.setTagCompound(tile.writeToNBTSelf(new NBTTagCompound()));
        return stack;
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {

    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase player, ItemStack stack) {
        super.onBlockPlacedBy(world, pos, state, player, stack);
        TileEntityShipping te = (TileEntityShipping) world.getTileEntity(pos);
        if(te!=null && stack.getTagCompound()!=null){
            te.readFromNBTSelf(stack.getTagCompound());
        }
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(NAME, "inventory"));
    }
}
