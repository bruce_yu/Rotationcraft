package cn.tohsaka.factory.rotationcraft.blocks.base;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockMachineBase;
import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBlast;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;

@GameInitializer(after = BlockBase.class)
public class BlockBlast extends BlockMachineBase implements IModelRegister, ITileEntityProvider {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockblast");
    public ItemBlock itemBlock;
    public BlockBlast(){
        this.setLightOpacity(255);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlock(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        setDefaultState();
    }

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockBlast());
    }
    @Override
    public int getMaxmeta() {
        return 0;
    }
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileBlast();
    }
}
