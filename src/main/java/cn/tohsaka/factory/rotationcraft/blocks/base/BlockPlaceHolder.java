package cn.tohsaka.factory.rotationcraft.blocks.base;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.BlockBarrier;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import java.util.Random;

@GameInitializer
public class BlockPlaceHolder extends BlockBarrier implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"placeholder");
    public static BlockPlaceHolder INSTANCE;
    public BlockPlaceHolder(){
        setRegistryName(NAME);
        this.setHardness(0.0F);
        this.setResistance(6000001.0F);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.BLOCKS.register(this);
        INSTANCE = this;
    }
    public static void init(){
        new BlockPlaceHolder();
    }
    @Override
    public void registerModel() {

    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return new ItemBlock(Blocks.AIR);
    }

}
