package cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.enet.*;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMeter;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.*;

import static cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe.ConnectorType.LARGE;
import static cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe.ConnectorType.NONE;

@GameInitializer(after = BlockPipe.class)
public class TilePipeCore extends TileEntity implements INetTickable, ITickable {
    public Node node = new Node(this,UUID.randomUUID());


    public TilePipeCore(){

    }
    public static void init(){
        GameRegistry.registerTileEntity(TilePipeCore.class,new ResourceLocation(RotationCraft.MOD_ID,"tilepipe"));
    }

    @Override
    public void update() {
        if(!world.isRemote && world.getTotalWorldTime() % 40 == 0){
            updateNetwork();
        }
    }

    public void updateNetwork(){
        chekingSides();
        for(int i=0;i<6;i++){
            if (BlockPipe.isConnectedToPipe(world, pos, EnumFacing.VALUES[i])) {
                TileEntity tile = world.getTileEntity(pos.offset(EnumFacing.VALUES[i]));
                if(tile!=null && !tile.isInvalid() && tile instanceof TilePipeCore && ((TilePipeCore)tile).node.getNetwork().isPresent()){
                    if(node.getNetwork().isPresent()){
                        Network.mergeNetwork(node.getNetwork().get(),((TilePipeCore)tile).node.getNetwork().get());
                    }else{
                        ((TilePipeCore)tile).node.getNetwork().get().addNode(node);
                    }
                }
            }
        }
        if(!this.node.getNetwork().isPresent()){
            Network.createByNode(node);
        }
    }

    boolean[] connectedSides = new boolean[]{false,false,false,false,false,false};
    public void chekingSides(){
        Arrays.fill(connectedSides,false);
        Arrays.fill(types,NONE);
        IBlockState state = world.getBlockState(pos).getActualState(world,pos);
        if(state==null || state.getBlock() != this.getBlockType()){
            return;
        }
        //DUNSWE
        connectedSides[0] = state.getValue(BlockPipe.DOWN);
        connectedSides[1] = state.getValue(BlockPipe.UP);
        connectedSides[2] = state.getValue(BlockPipe.NORTH);
        connectedSides[3] = state.getValue(BlockPipe.SOUTH);
        connectedSides[4] = state.getValue(BlockPipe.WEST);
        connectedSides[5] = state.getValue(BlockPipe.EAST);
        for(int i=0;i<6;i++){
            if(connectedSides[i]){
                if(BlockPipe.isConnectedToPipe(world,pos,EnumFacing.VALUES[i])){
                    types[i]=ConnectorType.NORMAL;
                }else{
                    types[i]= LARGE;
                }
            }
        }

    }

    public EnumFacing[] getConnected(){
        List<EnumFacing> faces = new ArrayList<>();
        for(int i=0;i<6;i++){
            if(connectedSides[i]){
                faces.add(EnumFacing.VALUES[i]);
            }
        }
        return faces.toArray(new EnumFacing[]{});
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY;
    }

    FluidTank tank = new FluidTank(16000);

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability==CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(tank);
        }
        return super.getCapability(capability, facing);
    }
    public FluidStack getFluidProperty(){
        return tank.getFluid();
    }



    public List<AxisAlignedBB> getCollisionBoxes(AxisAlignedBB entityBox) {
        List<AxisAlignedBB> list = new ArrayList<>();

        for (EnumFacing side : getConnected()) {
            AxisAlignedBB box = getTransmitterType(side) == LARGE ? BlockPipe.largeSides[side.ordinal()] : BlockPipe.smallSides[side.ordinal()];
            if (box.intersects(entityBox)) {
                list.add(box);
            }
        }

        /*AxisAlignedBB box = getTransmitterType(side) == LARGE ? BlockPipe.largeSides[side.ordinal()] : BlockPipe.smallSides[side.ordinal()];
        if (box.intersects(entityBox)) {
            list.add(box);
        }*/
        return list;
    }

    public List<AxisAlignedBB> getCollisionBoxes() {
        List<AxisAlignedBB> list = new ArrayList<>();
        for (EnumFacing side : getConnected()) {
            boolean ctp = BlockPipe.isConnectedToPipe(world,pos,side);
            if(getTransmitterType(side) != NONE) {
                if (ctp) {
                    list.add(BlockPipe.smallSides[side.ordinal()]);
                } else {
                    list.add(BlockPipe.largeSides[side.ordinal()]);
                }
            }
        }

        list.add(BlockPipe.smallSides[6]);
        return list;
    }

    public ConnectorType types[] = new ConnectorType[]{NONE,NONE,NONE,NONE,NONE,NONE};
    public ConnectorType getTransmitterType(EnumFacing side){
        if(connectedSides[side.ordinal()]){
            return types[side.ordinal()];
        }
        return NONE;
    }




    public boolean onBlockActivated(IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, AxisAlignedBB defaultBox) {
        if(!(playerIn.getHeldItem(hand).getItem() instanceof ItemMeter)){
            return false;
        }
        if(!world.isRemote){
            /*boolean isLarge = Arrays.asList(BlockPipe.largeSides).contains(defaultBox);
            int index = Arrays.asList(isLarge?BlockPipe.largeSides:BlockPipe.smallSides).indexOf(defaultBox);
            EnumFacing side = null;
            if(index<6){
                side = EnumFacing.VALUES[index];
                playerIn.sendMessage(new TextComponentString(side.getName()));
            }*/

            Collection<NetworkConnection> connections = Network.getConnections(node);
            playerIn.sendMessage(new TextComponentString(String.format("pipe network established with %d port",connections.size())));
            playerIn.sendMessage(new TextComponentString(String.format("latest tick using %s millis", NetworkTicker.instance.getNetTickmillis(Network.getNetwork(node).getUuid()).toString())));
        }

        return true;
    }

    @Override
    public void tickNetwork() {

    }

    @Override
    public boolean isNodeInvalid() {
        return this.isInvalid();
    }

    @Override
    public Collection<NetworkConnection> getConnections() {
        List<NetworkConnection> connections = new ArrayList<>();
        for(int i=0;i<6;i++){
            if(connectedSides[i] && !BlockPipe.isConnectedToPipe(world,pos,EnumFacing.VALUES[i])){
                connections.add(new NetworkConnection(node,world,pos,EnumFacing.VALUES[i],node,world.getBlockState(pos.offset(EnumFacing.VALUES[i])).getBlock().getLocalizedName()));
            }
        }
        return connections;
    }

    @Override
    public BlockPos getBlockPos() {
        return getPos();
    }


    @Override
    public void invalidate() {
        super.invalidate();
        node.getNetwork().ifPresent(network -> network.invalidate());
    }

}
