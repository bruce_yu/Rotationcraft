package cn.tohsaka.factory.rotationcraft.blocks.transmits;

import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockGearBoxBase extends BlockBase {
    public BlockGearBoxBase(Material materialIn) {
        super(materialIn);
    }


    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {

    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase player, ItemStack stack) {
        super.onBlockPlacedBy(world, pos, state, player, stack);
        TileGearbox te = (TileGearbox) world.getTileEntity(pos);
        if(te!=null && stack.getTagCompound()!=null){
            te.readFromNBTSelf(stack.getTagCompound());
        }
    }
    public int[] diagear = new int[]{63,64,65,66};
    public int[] hslagear = new int[]{79,80,81,82};

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if(playerIn.getHeldItem(hand).getItem() instanceof ItemMaterial && !worldIn.isRemote){
            TileEntity te = worldIn.getTileEntity(pos);
            if(te!=null && te instanceof TileGearbox){
                int id = ItemMaterial.getStackId(playerIn.getHeldItem(hand));
                int ratioid = ((TileGearbox) te).gearratio==2?0:(((TileGearbox) te).gearratio==4?1:(((TileGearbox) te).gearratio==8?2:3));
                if(((TileGearbox) te).getMetadata()==0){
                    //HSLA
                    if(id==hslagear[ratioid]){
                        playerIn.getHeldItem(hand).grow(-1);
                        ((TileGearbox) te).fix();
                        return true;
                    }
                }else{
                    if(id==diagear[ratioid]){
                        playerIn.getHeldItem(hand).grow(-1);
                        ((TileGearbox) te).fix();
                        return true;
                    }
                }
            }
            return false;
        }
        return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        if(VARIANT!=null){
            TileEntity tile = world.getTileEntity(pos);
            ItemStack stack = new ItemStack(this,1,((TileMachineBase)tile).getMetadata());
            if(tile!=null && tile instanceof TileGearbox){
                stack.setTagCompound(tile.writeToNBT(new NBTTagCompound()));
            }
            return stack;
        }
        return new ItemStack(this,1,0);
    }
}
