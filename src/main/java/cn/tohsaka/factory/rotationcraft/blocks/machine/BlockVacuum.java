package cn.tohsaka.factory.rotationcraft.blocks.machine;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.power.IBuildOptimizer;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockGearbox16x;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockMachineBase;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileVacuum;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

@GameInitializer(after = BlockGearbox16x.class)
public class BlockVacuum extends BlockMachineBase implements IModelRegister, ITileEntityProvider, IBuildOptimizer {

    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockvacuum");
    public ItemBlock itemBlock;

    public BlockVacuum() {
        this.setLightOpacity(255);
        this.setLightLevel(1);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        //setCreativeTab(RotationCraft.TAB_BLOCK);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlock(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        setDefaultState();
    }

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockVacuum());
        /*RecipeHelper.addShapedRecipe(new ItemStack(RotationCraft.INSTANCE.blocks.get(NAME)),"ABA","CDC","ABA",
                'A', ItemMaterial.getStackById(0),
                'B', new ItemStack(Items.ENDER_PEARL),
                'C', ItemMaterial.getStackById(77),
                'D', new ItemStack(Blocks.CHEST)
        );*/
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileVacuum();
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }


}