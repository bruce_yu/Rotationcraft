package cn.tohsaka.factory.rotationcraft.blocks.transmits;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.power.IBuildOptimizer;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemBlockGearbox;
import cn.tohsaka.factory.rotationcraft.items.ItemBlockModelBased;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileCVT;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;

@GameInitializer(after = BlockGearbox16x.class)
public class BlockCVT extends BlockBase implements IModelRegister, ITileEntityProvider, IBuildOptimizer {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"cvt");
    public ItemBlock itemBlock;
    public BlockCVT(){
        super(Material.ROCK);
        this.setLightOpacity(0);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        //setCreativeTab(RotationCraft.TAB_BLOCK);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlockModelBased(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        setDefaultState();
    }
    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockCVT());
    }
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileCVT();
    }


    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {

    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase player, ItemStack stack) {
        super.onBlockPlacedBy(world, pos, state, player, stack);
        TileCVT te = (TileCVT) world.getTileEntity(pos);
        if(te!=null && stack.getTagCompound()!=null){
            te.readFromNBTSelf(stack.getTagCompound());
        }
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        if(VARIANT!=null){
            TileEntity tile = world.getTileEntity(pos);
            ItemStack stack = new ItemStack(this,1);
            if(tile!=null && tile instanceof TileGearbox){
                stack.setTagCompound(tile.writeToNBT(new NBTTagCompound()));
            }
            return stack;
        }
        return new ItemStack(this,1);
    }



    @Override
    public boolean shouldCheckWeakPower(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side) {
        return true;
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
        TileEntity tile = worldIn.getTileEntity(pos);
        if(tile instanceof TileCVT){
            ((TileCVT) tile).onWorldChanged(worldIn.isBlockPowered(pos));
        }
    }
}
