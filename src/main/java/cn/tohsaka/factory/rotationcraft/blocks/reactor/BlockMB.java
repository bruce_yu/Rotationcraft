package cn.tohsaka.factory.rotationcraft.blocks.reactor;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockFerm;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemScrewer;
import cn.tohsaka.factory.rotationcraft.proxy.GuiHandler;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileBedCore;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileBedFrame;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileBedIO;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;

@GameInitializer(after = BlockFerm.class)
public class BlockMB extends Block implements ITileEntityProvider, IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockmb");
    public static ItemBlock itemBlock;
    public static PropertyInteger TYPE = PropertyInteger.create("type",0,2);
    private static final int MB_WALL = 0;
    private static final int MB_STEAMIO = 1;
    private static final int MB_CORE = 2;
    public BlockMB() {
        super(Material.IRON);
        setUnlocalizedName(NAME.getResourceDomain()+"."+NAME.getResourcePath());
        setRegistryName(NAME);
        ForgeRegistries.BLOCKS.register(this);

        itemBlock = new ItemBlock(this){
            /**
             * Returns the unlocalized name of this item. This version accepts an ItemStack so different stacks can have
             * different names based on their damage or NBT.
             *
             * @param stack
             */
            @Override
            public String getUnlocalizedName(ItemStack stack) {
                return super.getUnlocalizedName(stack)+"."+stack.getMetadata();
            }
        };
        itemBlock.setRegistryName(NAME);
        itemBlock.setHasSubtypes(true);
        ForgeRegistries.ITEMS.register(itemBlock);

        setDefaultState();
    }

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockMB());
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[] { TYPE });
    }

    public void setDefaultState(){
        IBlockState ib = getBlockState().getBaseState().withProperty(TYPE, 0);
        setDefaultState(ib);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(TYPE);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState().withProperty(TYPE,meta);
    }


    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand) {
        int damage = placer.getHeldItem(hand).getMetadata();
        return getDefaultState().withProperty(TYPE,damage);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(state.getBlock(),1,state.getValue(TYPE));
    }

    @Override
    public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items) {
        for(int id:TYPE.getAllowedValues()){
            items.add(id,new ItemStack(itemBlock,1,id));
        }
    }



    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        if(meta==MB_WALL){
            return new TileBedFrame();
        }
        if(meta==MB_STEAMIO){
            return new TileBedIO();
        }
        if(meta==MB_CORE){
            return new TileBedCore();
        }
        return null;
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack held = playerIn.getHeldItem(hand);
        if(held.getItem() instanceof ItemScrewer){
            if(worldIn.isRemote){
                return true;
            }
            TileEntity tile = worldIn.getTileEntity(pos);
            if (tile!=null && tile instanceof TileBedFrame){
                ((TileBedFrame) tile).checkingStructure(playerIn);
            }
        }else if(held.getItem() instanceof ItemBlock && (((ItemBlock) held.getItem()).getBlock() instanceof BlockMB || ((ItemBlock) held.getItem()).getBlock() instanceof BlockConcrete)){
            return false;
        }else {
            TileEntity tile = worldIn.getTileEntity(pos);
            if (tile!=null && tile instanceof TileBedFrame && ((TileBedFrame) tile).getCore()!=null && ((TileBedFrame) tile).getCore().isFramed()){
                playerIn.openGui(RotationCraft.MOD_ID, GuiHandler.BLOCK_GUI, worldIn, ((TileBedFrame) tile).corepos.getX(),((TileBedFrame) tile).corepos.getY(),((TileBedFrame) tile).corepos.getZ());
                return true;
            }else {
                return false;
            }
        }
        return false;
    }

    @Override
    public void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer player) {
        if(!world.isRemote){
            TileEntity te = world.getTileEntity(pos);
            if(te!=null && te instanceof TileBedFrame){
                ((TileBedFrame)te).onBlockHarvested(player);
            }
        }
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(BlockMB.itemBlock,0,new ModelResourceLocation(BlockMB.NAME, "type=0"));
        ModelLoader.setCustomModelResourceLocation(BlockMB.itemBlock,1,new ModelResourceLocation(BlockMB.NAME, "type=1"));
        ModelLoader.setCustomModelResourceLocation(BlockMB.itemBlock,2,new ModelResourceLocation(BlockMB.NAME, "type=2"));
    }

}
