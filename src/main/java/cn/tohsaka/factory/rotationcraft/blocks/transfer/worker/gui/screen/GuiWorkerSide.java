package cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.container.ContainerWorkerSide;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.tiles.TileWorker;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementButtonManaged;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class GuiWorkerSide extends GuiMachinebase {
    public static ResourceLocation tex_path = new ResourceLocation(RotationCraft.MOD_ID,"textures/gui/basic_gui.png");
    private TileWorker tile;
    private InventoryPlayer player;
    private int sideindex;
    public GuiWorkerSide(TileWorker te, InventoryPlayer playerInv,int side) {
        super(new ContainerWorkerSide(te,playerInv,side), tex_path);
        NAME = te.getBlockType().getLocalizedName() + "(" + EnumFacing.VALUES[side].getName() + ")";
        tile = te;
        showtab = false;
        player = playerInv;
        sideindex = side;
    }
    private ElementButtonManaged elementButton;

    @Override
    public void initGui() {
        super.initGui();
        addElement(new ElementSlot(this,71,24,new ResourceLocation(RotationCraft.MOD_ID,"textures/gui/worker/slotfilter.png")));
        addElement(new ElementSlot(this,89,24,new ResourceLocation(RotationCraft.MOD_ID,"textures/gui/worker/slotfilter.png")));

        elementButton = (ElementButtonManaged)addElement(new ElementButtonManaged(this,70,45,36,18,tile.rsmode[sideindex]==0?"RS NONE":(tile.rsmode[sideindex]==1?"RS ON":"RS OFF")) {
            @Override
            public void onClick() {
                if(this.getText().contains("RS OFF")){
                    this.setText("RS NONE");
                    tile.setSideRSMode(sideindex,0);
                    return;
                }

                if(this.getText().contains("RS NONE")){
                    this.setText("RS ON");
                    tile.setSideRSMode(sideindex,1);
                    return;
                }

                if(this.getText().contains("RS ON")){
                    this.setText("RS OFF");
                    tile.setSideRSMode(sideindex,2);
                    return;
                }
            }
        });

    }
}
