package cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.container;

import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.tiles.TileWorker;
import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerWorker extends ContainerCore {
    public ContainerWorker(TileWorker te, InventoryPlayer inv){
        super(te,inv);
        for (int i=0;i<4;i++){
            int slotid = i;
            addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(slotid,item),te,slotid,152,9+i*18));
        }

        for (int i=0;i<4;i++){
            int slotid = i+4;
            addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(slotid,item),te,slotid,134,9+i*18));
        }

    }
}
