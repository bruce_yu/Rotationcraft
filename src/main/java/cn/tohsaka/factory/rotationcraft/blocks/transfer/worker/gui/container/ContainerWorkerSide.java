package cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileInvMachine;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerWorkerSide extends ContainerCore {
    public ContainerWorkerSide(TileInvMachine te, InventoryPlayer playerInventory,int side) {
        super(te, playerInventory);
        int startslot = 8 + side * 2;
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(startslot+0,item),te,startslot+0,71,24));
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(startslot+1,item),te,startslot+1,89,24));
    }
}
