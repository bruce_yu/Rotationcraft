package cn.tohsaka.factory.rotationcraft.blocks.reactor;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

@GameInitializer
public class BlockConcrete extends Block implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockconcrete");
    public static ItemBlock itemBlock;
    public BlockConcrete() {
        super(Material.ROCK);
        setUnlocalizedName(NAME.getResourceDomain()+"."+NAME.getResourcePath());
        setRegistryName(NAME);
        ForgeRegistries.BLOCKS.register(this);

        itemBlock = new ItemBlock(this);
        itemBlock.setRegistryName(NAME);
        itemBlock.setHasSubtypes(true);
        ForgeRegistries.ITEMS.register(itemBlock);

    }

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockConcrete());
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock,0,new ModelResourceLocation(NAME, "normal"));
    }
}
