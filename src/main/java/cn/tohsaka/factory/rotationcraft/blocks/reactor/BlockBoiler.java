package cn.tohsaka.factory.rotationcraft.blocks.reactor;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileBoiler;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;

@GameInitializer
public class BlockBoiler extends Block implements IModelRegister, ITileEntityProvider {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockboiler");
    public static ItemBlock itemBlock;
    public BlockBoiler() {
        super(Material.ROCK);
        setUnlocalizedName(NAME.getResourceDomain()+"."+NAME.getResourcePath());
        setRegistryName(NAME);
        ForgeRegistries.BLOCKS.register(this);

        itemBlock = new ItemBlock(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);

    }

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockBoiler());
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock,0,new ModelResourceLocation(NAME, "normal"));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World world, int i) {
        return new TileBoiler();
    }
}
