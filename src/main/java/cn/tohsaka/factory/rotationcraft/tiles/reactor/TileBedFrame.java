package cn.tohsaka.factory.rotationcraft.tiles.reactor;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.Nullable;

@GameInitializer
public class TileBedFrame extends TileEntity {
    public TileBedCore core;
    public BlockPos corepos;
    public static void init(){
        GameRegistry.registerTileEntity(TileBedFrame.class,new ResourceLocation(RotationCraft.MOD_ID,"tilebedframe"));
    }
    public void checkingStructure(EntityPlayer playerIn){
        IteratorAABB iter = new IteratorAABB(this.pos.add(-1,-1,-1),3,3,3);
        while (iter.hasNext()){
            if(setCore(iter.next())){
                break;
            }
        }
        if(core!=null){
            core.checkingStructure(playerIn);
        }
    }
    public boolean setCore(BlockPos blockPos){
        if(blockPos==null){
            core = null;
            corepos = null;
        }else {
            TileEntity tile = world.getTileEntity(blockPos);
            if(tile!=null && tile instanceof TileBedCore){
                core = (TileBedCore) tile;
                corepos = blockPos;
                markDirty();
                return true;
            }
        }
        return false;
    }

    public void onBlockHarvested(EntityPlayer player) {
        if(core!=null){
            core.destoryFrame();
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        if(world!=null) {
            super.readFromNBT(compound);
            if(!(this instanceof TileBedCore)){
                if(compound.hasKey("corepos")){
                    int[] ints = compound.getIntArray("corepos");
                    corepos = new BlockPos(ints[0],ints[1],ints[2]);
                }
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tag = super.writeToNBT(compound);
        if(!(this instanceof TileBedCore)) {
            if (corepos != null) {
                tag.setIntArray("corepos", new int[]{corepos.getX(), corepos.getY(), corepos.getZ()});
            }
        }
        return tag;
    }

    @Override
    protected void setWorldCreate(World worldIn) {
        this.world = worldIn;
    }
    public TileBedCore getCore(){
        if(core==null && corepos==null){
            return null;
        }
        if(core==null && corepos!=null){
            setCore(corepos);
        }
        return core;
    }

    protected boolean isFramed(){
        return getCore()!=null && getCore().isFramed();
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return isFramed() && capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY && facing.ordinal() < 2;
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(isFramed() && capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY && facing.ordinal() < 2){
            return getCore().getCapability(capability,facing);
        }
        return super.getCapability(capability, facing);
    }
}
