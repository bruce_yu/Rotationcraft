package cn.tohsaka.factory.rotationcraft.tiles.etc;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IControlableEngine;
import cn.tohsaka.factory.rotationcraft.api.IScrewable;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

@GameInitializer
public class TileEngineController extends TileEntity implements IScrewable {
    public static void init(){
        GameRegistry.registerTileEntity(TileEngineController.class,new ResourceLocation(RotationCraft.MOD_ID,"tileenginecontroller"));
    }
    public float effective = 1f;
    @Override
    protected void setWorldCreate(World worldIn) {
        this.world = worldIn;
    }

    public void toggleEngine(EntityPlayer player){
        TileEntity te = world.getTileEntity(this.pos.add(0,1,0));
        if(te==null || !(te instanceof IControlableEngine)){
            return;
        }
        toggle();
        ((IControlableEngine) te).setEffective(effective);
        if (player!=null){
            player.sendMessage(new TextComponentString(String.format("Engine effective set to %.1f",effective*100)+"%"));
        }
    }

    public float toggle(){
        effective +=0.125;
        if(effective>1){
            effective = 0;
        }
        markDirty();
        return effective;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("effective")){
            effective = compound.getFloat("effective");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setFloat("effective",effective);
        return super.writeToNBT(compound);
    }

    @Override
    public boolean screw(EnumFacing facing, EntityLivingBase player) {
        if(!world.isRemote && player instanceof EntityPlayer){
            toggleEngine((EntityPlayer) player);
        }
        return true;
    }
}
