package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockComb;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelCombustion;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerComb;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiComb;
import cn.tohsaka.factory.rotationcraft.init.Sounds;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

@GameInitializer(after = BlockComb.class)
public class TileComb extends TileEngine implements IGuiProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileComb.class,new ResourceLocation(RotationCraft.MOD_ID,"tilecomb"));
    }
    public FuelTank fuelTank = new FuelTank(16*1000,"fuel");
    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelCombustion();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "combtex";
    }

    public TileComb(){
        super(new RotaryPower(128,512,0,0));
        createSlots(1);
        setSlotMode(SlotMode.INPUT,new int[]{0});
    }

    @SideOnly(Side.CLIENT)
    public void playsound(){
        SoundHandler.startTileSound(Sounds.GASENGINE.getEvent(),0.5F,pos);
    }

    @SideOnly(Side.CLIENT)
    public void stopsound(){
        SoundHandler.stopTileSound(pos);
    }

    @Override
    public void update() {
        super.update();
        if(world.isRemote){
            if(isActive){
                playsound();
            }else{
                stopsound();
            }
        }
    }
    @Override
    public void invalidate() {
        if(world.isRemote){
            stopsound();
        }
        super.invalidate();
    }

    @Override
    public void tick() {
        if(fuelTank.getFreeSpace()>1000 && !getStackInSlot(0).isEmpty()){
            fuelTank.inc(1000);
            decrStackSize(0,1);
        }
        fuelTank.dec(1);
        if(fuelTank.isEmpty()){
            isActive = false;
            powerSource.setPower(RotaryPower.ZERO);
        }else if(!fuelTank.isEmpty()){
            isActive = true;
            powerSource.setPower(new RotaryPower(128,512,0,0));
        }

        if(world.getTotalWorldTime() % 100 == 0){
            markDirty();
        }
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return stack.isItemEqual(new ItemStack(ItemMaterial.materialMap.get(60)));
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        fuelTank.readFromNBT(compound);
        isActive = compound.getBoolean("isActive");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tag = super.writeToNBT(compound);
        tag = fuelTank.writeToNBT(compound);
        compound.setBoolean("isActive",isActive);
        return tag;
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiComb(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerComb(this,player.inventory);
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add(StringHelper.localize("info.rc.fuelcap")+":"+String.valueOf(fuelTank.getFuel()));
        if(fuelTank.getFuel()<=0){
            return;
        }
        tooltip.add(StringHelper.localize("info.rc.waila.names.rtime")+":"+Math.round(fuelTank.getFuel()/20));
    }
}
