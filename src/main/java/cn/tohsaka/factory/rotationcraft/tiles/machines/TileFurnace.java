package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockFurnace;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.ExtractorRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerFurnace;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiFurnace;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemCrushedOre;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.StackUtils;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@GameInitializer(after = BlockFurnace.class)
public class TileFurnace extends TileRotation implements IGuiProvider{
    public TileFurnace(){
        super(FacingTool.Position.DOWN);
        createSlots(24);
        setSlotMode(SlotMode.INPUT,new int[]{0,1,2,3,4,5,6,7,8,9,10,11});
        setSlotMode(SlotMode.OUTPUT,new int[]{12,13,14,15,16,17,18,19,20,21,22,23});
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileFurnace.class,new ResourceLocation(RotationCraft.MOD_ID,"tilefurnace"));
    }


    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(stack.getItem() instanceof ItemCrushedOre){
            return true;
        }

        List<CraftingPattern> list = Recipes.INSTANCE.getList(TileExtractor.class);
        for(CraftingPattern pattern:list){
            if(((ExtractorRecipe)pattern).test(stack)){
                return true;
            }
        }
        /*ItemStack result = FurnaceRecipes.instance().getSmeltingResult(stack);
        if(result!=null && !result.isEmpty()){
            return true;
        }*/
        return false;
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiFurnace(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerFurnace(this,player.inventory);
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(32,2048,0,0);
    }

    @Override
    public boolean canStart() {
        return !Arrays.stream(inventory).allMatch(ItemStack::isEmpty);
    }
    public static final int procTime = 240;

    @Override
    public boolean processStart() {
        sortInventory();
        processMax = processRem = calcProcessTime(procTime,getMinimumPower());
        return true;
    }

    @Override
    public void processTick() {
        processRem--;

    }
    boolean sorting = false;
    public void sortInventory() {
        if (world.isBlockPowered(pos)){
            boolean didOp = false;

            int[] inputSlots = new int[]{0,1,2,3,4,5,6,7,8,9,10,11};

            List<InvID> invStacks = new ArrayList<>();



            for (int id : inputSlots) {
                invStacks.add(InvID.get(id, this));
            }

            for (InvID invID1 : invStacks) {
                for (InvID invID2 : invStacks) {
                    if (invID1.ID == invID2.ID || StackUtils.diffIgnoreNull(invID1.stack, invID2.stack)
                            || Math.abs(invID1.size() - invID2.size()) < 2) {
                        continue;
                    }

                    List<ItemStack> evened = StackUtils.even(getStackInSlot(invID1.ID), getStackInSlot(invID2.ID));
                    setInventorySlotContents(invID1.ID, evened.get(0));
                    setInventorySlotContents(invID2.ID, evened.get(1));

                    didOp = true;
                    break;
                }

                if (didOp) {
                    markDirty();
                    break;
                }
            }
        }
    }

    public static class InvID {

        public ItemStack stack;
        public int ID;

        public InvID(ItemStack s, int i) {
            stack = s;
            ID = i;
        }

        public static InvID get(int id, IInventory inv) {
            return new InvID(inv.getStackInSlot(id), id);
        }

        public int size() {
            return !stack.isEmpty() ? stack.getCount() : 0;
        }

        public Item item() {
            return !stack.isEmpty() ? stack.getItem() : null;
        }
    }


    @Override
    public void processEnd() {
        for(int i=0;i<12;i++){
            ItemStack stack = getSmeltingResult(getStackInSlot(i),world.rand);
            if(stack!=null && Utils.checkInvCanInsertItem(this,stack,12,24)){
                Utils.distributeOutput(this,stack,12,24,true);
                decrStackSize(i,1);
            }
        }
    }

    @Override
    public void processStop() {
        super.processStop();
    }

    public static ItemStack getSmeltingResult(ItemStack stack,Random random){
        ItemStack result = ItemStack.EMPTY;
        /*if(stack.getItem() instanceof ItemCrushedOre && stack.hasTagCompound() && stack.getTagCompound().hasKey("ore")){
            ItemStack ore = new ItemStack(stack.getTagCompound().getCompoundTag("ore"));
            result = FurnaceRecipes.instance().getSmeltingResult(ore);
            result.setCount(getOreBounds(ore,1,random));
        }*/

        if(stack.getItem() instanceof ItemCrushedOre && stack.hasTagCompound() && stack.getTagCompound().hasKey("ore")){
            ItemStack ore = new ItemStack(stack.getTagCompound().getCompoundTag("ore"));
            List<CraftingPattern> list = Recipes.INSTANCE.getList(TileExtractor.class);
            for(CraftingPattern pattern:list){
                if(((ExtractorRecipe)pattern).test(ore)){
                    result = ((ExtractorRecipe)pattern).output.copy();
                    result.setCount(getOreBounds(ore,1,random));
                    return result;
                }
            }
        }else{
            result = FurnaceRecipes.instance().getSmeltingResult(stack);
            result.setCount(getOreBounds(stack,0,random));
        }
        if(result!=null && !result.isEmpty()){
            return result;
        }
        return ItemStack.EMPTY;
    }
    public static int getOreBounds(ItemStack stack,int quantity,Random random){
        if(!(stack.getItem() instanceof ItemBlock)){
            return 1;
        }
        Block block = ((ItemBlock)stack.getItem()).getBlock();
        return block.quantityDroppedWithBonus(quantity,random);
    }

    @Override
    public NBTTagCompound writeGuiTagCompound(NBTTagCompound tag) {
        return super.writeGuiTagCompound(tag);
    }

    @Override
    public void readGuiTagCompound(NBTTagCompound tag) {
        super.readGuiTagCompound(tag);
    }
}
