package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IScrewable;
import cn.tohsaka.factory.rotationcraft.api.IUpgradable;
import cn.tohsaka.factory.rotationcraft.api.IUpgrade;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockDc;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelMagnetic;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemUpgrade;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockDc.class)
public class TileMagnetic extends TileEngine implements IUpgradable, IScrewable {
    public EnergyStorage energyStorage;


    public static void init(){
        GameRegistry.registerTileEntity(TileMagnetic.class,new ResourceLocation(RotationCraft.MOD_ID,"tilemagnetic"));
    }
    public TileMagnetic(){
        super(new RotaryPower(1024,65536));
        energyStorage = new EnergyStorage(240000);
    }
    boolean isPowered = false;
    @Override
    public void tick() {
        if(powerSource.getPower().getWatt() > 0 || world.getTotalWorldTime() % 100 == 0){
            if(energyStorage.getEnergyStored()>0){
                RotaryPower power = new RotaryPower(1 << (upgrades.size() * 2),65535);
                int fe = getFEUsage();
                if(energyStorage.getEnergyStored()>=fe){
                    powerSource.setPower(power);
                }else {
                    powerSource.setPower(RotaryPower.ZERO);
                }
                energyStorage.extractEnergy(fe,false);
            }else {
                powerSource.setPower(RotaryPower.ZERO);
            }
        }
    }
    public int getFEUsage(){
        return (int) Math.round((powerSource.getPower().getWatt() / GeneralConfig.FE_converter_ratio) * 1.2f);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelMagnetic();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "magnetictex";
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        super.onBlockHarvested(player);
        dropUpgrades(world,pos,upgrades);
    }

    @Override
    public boolean canUpgrade(ItemStack stack) {
        if(stack.getItem() instanceof IUpgrade && ((IUpgrade) stack.getItem()).readyForUse(stack) && stack.getItem() instanceof ItemUpgrade && stack.getMetadata()>0 && stack.getMetadata()<6){
            return stack.getMetadata() - upgrades.size() == 1;
        }
        return false;
    }

    @Override
    public boolean installUpgrade(ItemStack stack,EntityPlayer player) {
        this.upgrades.add(stack.copy());
        if(!world.isRemote){
            NetworkDispatcher.dispatchMachineUpdate(this);
            player.sendMessage(new TextComponentString(StringHelper.localize("info.rc.etc.upgrade_installed")));
        }else {
            SoundHandler.playSound(SoundEvents.BLOCK_ANVIL_USE);
        }
        return true;
    }


    @Override
    public boolean screw(EnumFacing facing, EntityLivingBase player) {
        if(upgrades.size()>0){
            if(world.isRemote){
                return true;
            }
            dropUpgrades(world,pos,upgrades.get(upgrades.size()-1).copy());
            NetworkDispatcher.dispatchMachineUpdate(this);
            return true;
        }
        return false;
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add("Current Level:"+upgrades.size());
        tooltip.add("FE Usage:"+ getFEUsage());
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return (capability == CapabilityEnergy.ENERGY) || super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityEnergy.ENERGY && (side == FacingTool.getRelativeSide(FacingTool.Position.FRONT,facing) || side == null)){
            return CapabilityEnergy.ENERGY.cast(energyStorage);
        }
        return super.getCapability(capability, side);
    }

}
