package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.thermal.IThermalMachine;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockBlast;
import cn.tohsaka.factory.rotationcraft.crafting.BlastRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerBlast;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiBlast;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.Arrays;

@GameInitializer(after = BlockBlast.class)
public class TileBlast  extends TileRotation implements IGuiProvider, IThermalMachine {
    public TileBlast(){
        createSlots(15);
        setSlotMode(SlotMode.INPUT,new int[]{0,1,2,3,4,5,6,7,8,9,10,11});
        setSlotMode(SlotMode.OUTPUT,new int[]{12,13,14});
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileBlast.class,new ResourceLocation(RotationCraft.MOD_ID,"tileblast"));
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiBlast(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerBlast(this,player.inventory);
    }



    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        /*if(index<3){
            ItemStack stackInSlot = getStackInSlot(index);
            if(stack!=null && !stackInSlot.isEmpty() && stackInSlot.isItemEqual(stack)){
                return true;
            }
            for(CraftingPattern recipe:Recipes.INSTANCE.getList(TileBlast.class)){
                if(recipe instanceof BlastRecipe){
                    if(index>=((BlastRecipe) recipe).cata.size()){
                        continue;
                    }
                    if(((BlastRecipe) recipe).cata.get(index).test(stack)){
                        return true;
                    }
                }
            }
        }
        if(index>2 && index<12){
            if(Arrays.stream(inventory).skip(3).limit(9).allMatch(ItemStack::isEmpty)){
                for(CraftingPattern recipe:Recipes.INSTANCE.getList(TileBlast.class)){
                    if(recipe instanceof BlastRecipe){
                        if(((BlastRecipe)recipe).inputList.test(stack)){
                            return true;
                        }
                    }
                }
            }else{
                for(int i=3;i<12;i++){
                    ItemStack stack1 = getStackInSlot(i);
                    if(stack1==null || stack1.isEmpty()){
                        continue;
                    }
                    return stack1.isItemEqual(stack);
                }
            }
        }*/
        return true;
    }


    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(0,0,0,0);
    }


    BlastRecipe curRecipe;



    @Override
    public boolean canStart() {
        if(Arrays.stream(inventory).skip(3).limit(9).allMatch(ItemStack::isEmpty) || Arrays.stream(inventory).limit(3).allMatch(ItemStack::isEmpty)){
            return false;
        }
        curRecipe = null;
        for(CraftingPattern c:Recipes.INSTANCE.getList(TileBlast.class)){
            if(((BlastRecipe)c).test(Arrays.stream(inventory).skip(3).limit(9).toArray(ItemStack[]::new),Arrays.stream(inventory).limit(3).toArray(ItemStack[]::new))){
                curRecipe = (BlastRecipe) c;
                return true;
            }
        }
        return false;
    }

    public static int worktime = 360;

    @Override
    public boolean processStart() {
        if(curRecipe.getTemp()>temp){
            return false;
        }
        float time = Math.max(worktime * (1-((temp - curRecipe.getTemp()) / 1350)),1);
        //float time = Math.max(worktime - (((temp - curRecipe.getTemp()) / 25F) * 8F),1);
        if(time>0){
            processMax = processRem = Math.round(time);
        }else{
            processMax = processRem = worktime;
        }
        return true;
    }
    int sleep=0;
    @Override
    public void processTick(){
        if(curRecipe.getTemp()>getTemp()){
            processEnd();
            sleep+=100;
            return;
        }
        if(curRecipe==null){
            processEnd();
            return;
        }
        processRem--;
    }

    @Override
    public void processEnd() {
        if(curRecipe==null){
            return;
        }

        if(curRecipe.isShapeless()){
            long count = 9 - Arrays.stream(inventory).skip(3).limit(9).filter(ItemStack::isEmpty).count();
            ItemStack stack = curRecipe.getOutput().copy();
            stack.setCount((int) count);
            if(curRecipe.getBound()>0){
                for(int i=0;i<count;i++){
                    if(world.rand.nextFloat()>=curRecipe.getBound()){
                        stack.grow(1);
                    }
                }
            }
            if(Utils.checkInvCanInsertItem(this,stack,12,15)){
                for(int i=0;i<curRecipe.cataItem.size();i++){
                    if(curRecipe.cata.get(i).test(getStackInSlot(i))){
                        decrStackSize(i,curRecipe.cataItem.get(i)[0].getCount());
                    }
                }
                for(int i=3;i<12;i++){
                    decrStackSize(i,curRecipe.inputItem.get(0)[0].getCount());
                }
                Utils.distributeOutput(this,stack.copy(),12,15,false);
            }
        }else{
            for(int i=0;i<curRecipe.cataItem.size();i++){
                if(curRecipe.cata.get(i).test(getStackInSlot(i))){
                    decrStackSize(i,curRecipe.cataItem.get(i)[0].getCount());
                }
            }
            for(int i=0;i<9;i++){
                if(curRecipe.inputItem.size()>i){
                    decrStackSize(i+3,curRecipe.inputItem.get(i)[0].getCount());
                }
            }


            ItemStack stack = curRecipe.getOutput().copy();
            if(curRecipe.getBound()>0){
                for(int i=0;i<curRecipe.getBound();i++){
                    if(world.rand.nextFloat()>=curRecipe.getBound()){
                        stack.grow(1);
                    }
                }
            }
            Utils.distributeOutput(this,stack.copy(),12,15,false);
        }

    }


    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {

    }
}
