package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockPlaceHolder;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelPump;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import com.google.common.base.Predicate;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDynamicLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.SPacketChunkData;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.*;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@GameInitializer(after = BlockExtractor.class)
public class TilePump extends TileModelMachine implements IModelProvider {
    public TilePump(){

    }

    public FluidTank fluidTank = new FluidTank(32000);

    public static void init(){
        GameRegistry.registerTileEntity(TilePump.class,new ResourceLocation(RotationCraft.MOD_ID,"tilepump"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelPump();
    }

    @Override
    public String getTexName() {
        return "pumptex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GL11.glRotatef(90,0,1,0);
    }



    @Override
    public boolean isActive() {
        return powerMachine.getPower().isPowered() &&
                powerMachine.getPower().getWatt()>getMinimumPower().getWatt() &&
                powerMachine.getPower().getTorque()>=getMinimumPower().getTorque();
    }

    public IteratorAABB workIterator;
    int range = 2;
    boolean somethingchanged = false;
    Set<Chunk> chunks = new HashSet<Chunk>();
    @Override
    public void update() {
        if(world.isRemote){
            if(isActive()) {
                angle = AxlsUtils.calcAngle(angle, powerMachine.getPower().getSpeed());
            }
            /*if(isActive() && !RenderUtils.containsRender(this)){
                addIO();
            }*/
        }else{
            updatePowerInput();

            sendChanges();

            if(!isActive()){
                return;
            }
            transferOutputFluid();
            if((fluidTank.getCapacity() - fluidTank.getFluidAmount())<1000){
                return;
            }
            Fluid fluid = getFluidOnBottom();
            if(world.getTotalWorldTime() % 20 == 0){
                if(fluid == FluidRegistry.WATER){
                    int count = (int) Math.max(Math.max(1,powerMachine.getPower().getSpeed() / 2048),16);
                    fluidTank.fill(new FluidStack(FluidRegistry.WATER,1000 * count),true);
                }else{
                    if(workIterator == null){
                        range = 8;
                        workIterator = new IteratorAABB(pos.add(-range, -pos.getY(), -range),(range*2)+1,pos.getY(),(range*2)+1);
                    }
                    for(int i=0;i<Math.max(Math.max(1,powerMachine.getPower().getSpeed() / 2048),8);i++){
                        doPump();
                    }
                }
            }
        }
    }

    public void sendChanges(){
        if(world.getTotalWorldTime() % 40 != 0){
            return;
        }
        if(somethingchanged){
            if(chunks.isEmpty()){
                somethingchanged = false;
                return;
            }

            chunks.forEach(chunk -> {
                SPacketChunkData data = new SPacketChunkData(world.getChunkFromBlockCoords(pos),65535);
                FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendPacketToAllPlayersInDimension(data, world.provider.getDimension());
            });

            chunks.clear();
            somethingchanged = false;
        }
    }

    BlockPos workpos;
    IBlockState placeholder = BlockPlaceHolder.INSTANCE.getDefaultState();
    public void doPump(){
        if(workIterator!=null && workIterator.hasNext()){
            workpos = workIterator.next();
            IBlockState old = world.getBlockState(workpos);
            Fluid fluid1 = getFluidOnPos(workpos);
            if(fluid1!=null){
                fluidTank.fill(new FluidStack(fluid1,1000),true);
                world.setBlockState(workpos,placeholder,2);

            }
        }else{
            workIterator = null;
        }

    }

    public void transferOutputFluid()
    {
        if (fluidTank.getFluidAmount() <= 0)
            return;

        int side;
        FluidStack output = new FluidStack(fluidTank.getFluid(), Math.min(fluidTank.getFluidAmount(), 16000));

        for(int i=2;i<6;i++){
            int toDrain = FluidHelper.insertFluidIntoAdjacentFluidHandler(this, EnumFacing.VALUES[i], output, true);
            if (toDrain > 0)
            {
                fluidTank.drain(toDrain, true);
                break;
            }
        }
        return;
    }

    private Fluid getFluidOnBottom(){
        IBlockState state = world.getBlockState(pos.add(0,-1,0));
        Block block = state.getBlock();
        Fluid fluid = lookupFluidForBlock(block);
        return fluid;
    }
    private Fluid getFluidOnPos(BlockPos target){
        IBlockState state = world.getBlockState(target);
        Block block = state.getBlock();
        if(block instanceof BlockDynamicLiquid){
            return null;
        }
        Fluid fluid = lookupFluidForBlock(block);
        return fluid;
    }
    final RotaryPower minimumpower = new RotaryPower(16,256,0,0);
    @Override
    public RotaryPower getMinimumPower() {
        return minimumpower;
    }

    public static Fluid lookupFluidForBlock(Block b) {
        if (b == Blocks.LAVA){
            return FluidRegistry.LAVA;
        }
        if(b == Blocks.FLOWING_LAVA){
            return null;
        }
        if (b == Blocks.WATER || b == Blocks.FLOWING_WATER) {
            return FluidRegistry.WATER;
        }
        Fluid f = FluidRegistry.lookupFluidForBlock(b);
        if (f == null && b instanceof IFluidBlock) {
            f = ((IFluidBlock) b).getFluid();
        }
        return f;
    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        if(workpos!=null){
            //drawables.add(new RenderUtils.RenderItem(0xFFFF00, workpos ,4F));
        }else{
            super.getOutBoxList(drawables);
        }
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    FluidTankInfo info = fluidTank.getInfo();
                    return new IFluidTankProperties[] { new FluidTankProperties(info.fluid, info.capacity, false, true) };
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    return 0;
                }


                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return fluidTank.drain(resource,doDrain);
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return fluidTank.drain(maxDrain,doDrain);
                }
            });
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public NBTTagCompound writeGuiTagCompound(NBTTagCompound tag) {
        if(workpos !=null){
            tag.setIntArray("workpos",new int[]{workpos.getX(),workpos.getY(),workpos.getZ()});
        }
        return super.writeGuiTagCompound(tag);
    }

    @Override
    public void readGuiTagCompound(NBTTagCompound tag) {
        super.readGuiTagCompound(tag);
        if(tag.hasKey("workpos")){
            int[] pos = tag.getIntArray("workpos");
            workpos = new BlockPos(pos[0],pos[1],pos[2]);
        }
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        if(workpos!=null){
            tooltip.add("work on: "+String.format("%d,%d,%d",workpos.getX(),workpos.getY(),workpos.getZ()));
        }
    }
}
