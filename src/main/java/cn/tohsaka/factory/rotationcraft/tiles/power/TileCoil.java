package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerStorage;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockCoil;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelCoil;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerCoil;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiCoil;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.ReikaMathLibrary;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockCoil.class)
public class TileCoil  extends TileEngine implements ITickable, IModeProvider, IModelProvider,IGuiProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileCoil.class,new ResourceLocation(RotationCraft.MOD_ID,"tilecoil"));
    }
    public RotaryPowerStorage powerStorage;
    public TileCoil(){
        super(RotaryPower.ZERO);
        powerStorage = new RotaryPowerStorage(getCapacity(),RotaryPower.ZERO,this) {
            @Nullable
            @Override
            public World getWorld() {
                return TileCoil.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                if (powerStorage.isOutputing()) {
                    return null;
                }
                return TileCoil.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                if (!powerStorage.isOutputing()) {
                    return null;

                }
                return FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK, 1);
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                if (powerStorage.isOutputing()) {
                    return null;
                }
                return CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),FacingTool.revert(facing));
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),facing);
            }
        };
    }
    public TileCoil(int meta){
        super(RotaryPower.ZERO);
        setMetadata(meta);
        RotaryPower maximum = getMetadata()==2?new RotaryPower(32768,32768):(getBlockMetadata()==1?new RotaryPower(4096,4096):new RotaryPower(1024,1024));
        powerStorage = new RotaryPowerStorage(getCapacity(),RotaryPower.ZERO,this) {
            @Nullable
            @Override
            public World getWorld() {
                return TileCoil.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                if (powerStorage.isOutputing()) {
                    return null;
                }
                return TileCoil.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                if (!powerStorage.isOutputing()) {
                    return null;

                }
                return FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK, 1);
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                if (powerStorage.isOutputing()) {
                    return null;
                }
                return CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),FacingTool.revert(facing));
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),facing);
            }
        };
        if(getMetadata()==2 && powerStorage.getPowerStored() != powerStorage.getPowerCapacity()){
            powerStorage.setPowerStored(powerStorage.getPowerCapacity()-1);
        }
    }

    public BlockPos getSrc() {
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1);
    }



    public RotaryPower getMaximum(){
        return isCreative()?new RotaryPower(32768,32768):(isBedrockCoil()?new RotaryPower(4096,4096):new RotaryPower(1024,1024));
    }
    public long getCapacity(){
        return  (getMetadata()>0) ? 240L*ReikaMathLibrary.longpow(10, 12) : 720*ReikaMathLibrary.intpow2(10, 6);
    }

    @Override
    public void update() {
        if (world.isRemote) {
            angle = AxlsUtils.calcAngle(angle, powerStorage.getPower().getSpeed());
        } else {
            tick();
        }
    }

    public void tick() {
        if(powerStorage.isOutputing() != world.isBlockPowered(pos)){
            onWorldChanged(!powerStorage.isOutputing());
        }
        store();
    }

    private void store() {
        long energy = powerStorage.getPowerStored();
        RotaryPower powerOut = powerStorage.getOutpuing().copy();
        if (!isCreative() && !world.isRemote && energy >= powerStorage.getPowerCapacity()) {
            //this.doExplode();
        }
        if (!powerStorage.isOutputing()) {
            if(!isCreative()){
                RotaryPower powerIn = getPowerInput();
                if(powerIn.isPowered()){
                    double pwr = powerIn.getWatt();
                    if (powerIn.getTorque() >= this.getChargingTorque() && pwr >= this.getChargingPower()) {
                        powerStorage.fill(powerIn);
                    }
                }
            }
        }else if (energy > 0 && powerOut.getTorque() > 0 && powerOut.getSpeed() > 0) {
            powerStorage.setPower(new RotaryPower(Math.min(powerOut.getTorque(), this.getTorqueCap()),powerOut.getSpeed()));
            if (!isCreative()) {
                powerStorage.drain(powerOut,true);
            }
        }
        else {
            powerStorage.setPower(RotaryPower.ZERO.copy());
        }
    }

    protected RotaryPower getPowerInput(){
        IPowerMachine cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),FacingTool.revert(facing));
        if(cap!=null){
            return cap.getPower();
        }else{
            return RotaryPower.ZERO;
        }
    }



    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        drawables.add(new RenderUtils.RenderItem(0xFF0000, FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),1F));
        drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),1F));
        super.getOutBoxList(drawables);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        powerStorage.readFromNBT(compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        powerStorage.writeToNBT(compound);
        compound.setInteger("meta",getMetadata());
        return compound;
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {

    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {

    }

    @SideOnly(Side.CLIENT)
    public void uploadOutput(RotaryPower power){
        powerStorage.setPower(power);
        NetworkDispatcher.uploadingClientSetting(null,this);
    }




    public int getTorqueCap() {
        return ReikaMathLibrary.ceilPseudo2Exp((int)Math.ceil(Math.sqrt(powerStorage.getPowerStored()/20)/4));
    }

    public long getChargingPower() {
        long energy = powerStorage.getPowerStored();
        return energy >= 20 ? ReikaMathLibrary.ceil2exp(ReikaMathLibrary.intpow2(ReikaMathLibrary.logbase2(energy/20), 4)) : 1;
    }

    public int getChargingTorque() {
        long energy = powerStorage.getPowerStored();
        int base = powerStorage.getPowerStored() >= 20 ? ReikaMathLibrary.ceil2exp(ReikaMathLibrary.intpow2(ReikaMathLibrary.logbase2(energy/20), 3))/2 : 1;
        if (isBedrockCoil()) {
            base = Math.max(base, powerStorage.getPowerStored() >= 20 ? 16*ReikaMathLibrary.ceil2exp(ReikaMathLibrary.intpow2(ReikaMathLibrary.logbase2(energy/80), 3))/2 : 16);
        }
        if (base <= (this.isBedrockCoil() ? 16 : 1))
            base = this.isBedrockCoil() ? 16 : 1;
        return base;
    }


    @Override
    public NBTTagCompound getModePacket() {
        NBTTagCompound tag = super.getModePacket();
        tag.setTag("powerOut", powerStorage.getOutpuing().writeToNBT(new NBTTagCompound()));
        return tag;
    }

    @Override
    public void handleModePacket(NBTTagCompound compound) {
        super.handleModePacket(compound);
        RotaryPower power = RotaryPower.readFromNBT(compound.getCompoundTag("powerOut"));
        powerStorage.setPower(power);
    }

    public boolean isBedrockCoil(){
        return getMetadata()>0;
    }
    public boolean isCreative(){
        return (getMetadata()==2);
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelCoil();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return getMetadata()>0?"coiltex_bed":"coiltex";
    }


    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiCoil(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerCoil(this,player.inventory);
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {
        if(tileEntity instanceof TileCoil){
            ((TileCoil) tileEntity).setMetadata(stack.getMetadata());
        }
    }

    @Override
    public void beginItemRender() {

    }

    public void onWorldChanged(boolean blockPowered){
        powerStorage.setOutputActive(blockPowered);
        markDirty();
    }



    @Override
    public void setFacing(EnumFacing facing) {
        super.setFacing(facing);
    }


    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if(!player.isCreative()){
            ItemStack stack = new ItemStack(RotationCraft.blocks.get(BlockCoil.NAME),1,getMetadata());
            NBTTagCompound tagCompound = new NBTTagCompound();
            tagCompound.setTag("storage",powerStorage.writeToNBT(new NBTTagCompound()));
            stack.setTagCompound(tagCompound);
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(),stack));
        }
    }

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        return false;
    }




    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null || FacingTool.revert(side) == facing){
                return true;
            }
            return false;
        }
        return super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null || FacingTool.revert(side) == facing){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerStorage);
            }
            return null;
        }
        return super.getCapability(capability, side);
    }

    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append(powerStorage.getOutpuing()+"\n");
        sb.append(String.format("PowerStorage{ %s / %s }\n",RotaryPower.formattedPower(powerStorage.getPowerStored()),RotaryPower.formattedPower(powerStorage.getPowerCapacity())));
        sb.append("Outputing: "+powerStorage.isOutputing()+"\n");
        return sb.toString();
    }
}
