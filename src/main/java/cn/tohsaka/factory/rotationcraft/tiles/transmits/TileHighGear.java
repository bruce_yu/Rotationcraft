package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockHighGear;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.gearbox.ModelHighGear;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

@GameInitializer(after = BlockHighGear.class)
public class TileHighGear extends TileGearbox {
    public static void init(){
        GameRegistry.registerTileEntity(TileHighGear.class,new ResourceLocation(RotationCraft.MOD_ID,"tilehighgear"));
    }
    public TileHighGear(){
        gearratio = 256;
    }


    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if(!player.isCreative()){
            ItemStack itemStack = new ItemStack(RotationCraft.blocks.get(BlockHighGear.NAME),1,0);
            if(!itemStack.isEmpty()){
                world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(),itemStack));
            }
        }
    }



    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelHighGear();
    }

    @Override
    public String getTexName() {
        return "transmission/gear/highgeartex";
    }


    @Override
    public void update() {
        if(world.isRemote){
            if(world.isRemote && powerTransformer.getPower().isPowered()){
                angleInput = AxlsUtils.calcAngle(angleInput,powerTransformer.getPowerIn().getSpeed());
                angleOutput = AxlsUtils.calcAngle(angleOutput,powerTransformer.getPower().getSpeed());
            }
        }else{
            updatePowerInput();
            if(lubricantTank.getFluidAmount()>0){
                if(isDownshift()){
                    RotaryPower power = new RotaryPower(powerTransformer.getPowerIn().getTorque()*(long)gearratio,powerTransformer.getPowerIn().getSpeed()/(long)gearratio,angleOutput,powerTransformer.getPowerIn().getBlend());
                    powerTransformer.setPower(power);
                }else{
                    RotaryPower power = new RotaryPower(powerTransformer.getPowerIn().getTorque()/(long)gearratio,powerTransformer.getPowerIn().getSpeed()*(long)gearratio,angleOutput,powerTransformer.getPowerIn().getBlend());
                    powerTransformer.setPower(power);
                }
                if(powerTransformer.getPower().getWatt()>0) {
                    lubricantTank.drain(1, true);
                    somethingchanged = true;
                }
            }else {
                powerTransformer.setPower(RotaryPower.ZERO);
            }

            if(somethingchanged){
                NetworkDispatcher.dispatchMachineUpdate(this);
                markDirty();
                somethingchanged = false;
            }
        }
    }
}
