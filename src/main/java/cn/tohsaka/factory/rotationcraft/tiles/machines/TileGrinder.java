package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.GrinderRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelGrinder;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerGrinder;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiGrinder;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockExtractor.class)
public class TileGrinder extends TileModelMachine implements IGuiProvider, IModelProvider {
    public TileGrinder(){
        createSlots(3);
        setSlotMode(SlotMode.INPUT,new int[]{0});
        setSlotMode(SlotMode.OUTPUT,new int[]{1});
        setSlotMode(SlotMode.OMNI,new int[]{2});
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileGrinder.class,new ResourceLocation(RotationCraft.MOD_ID,"tilegrinder"));
    }
    CraftingPattern curRecipe = null;
    @Override
    public boolean canStart() {
        if((fluidTank.getCapacity() - fluidTank.getFluidAmount())<=0){
            return false;
        }
        if(getStackInSlot(0).isEmpty()){
            return false;
        }
        if(getStackInSlot(1).getCount()>=getStackInSlot(1).getMaxStackSize()){
            return false;
        }
        List<CraftingPattern> recipes = Recipes.INSTANCE.getList(TileGrinder.class);
        if(recipes == null){
            return false;
        }
        for(CraftingPattern pattern:recipes){
            if(((GrinderRecipe)pattern).igd.test(getStackInSlot(0))){
                if(pattern.getData()!=null && pattern.getData().hasKey("fluid")){
                    FluidStack fluid = FluidStack.loadFluidStackFromNBT(pattern.getData().getCompoundTag("fluid"));
                    if(fluidTank.getCapacity() - fluidTank.getFluidAmount()<fluid.amount){
                        curRecipe = null;
                        return false;
                    }
                }
                curRecipe = pattern;
                return true;
            }
        }
        curRecipe = null;
        return false;
    }

    private static final int SLOT_FLUID = 2;
    @Override
    public void update() {
        if(!getStackInSlot(SLOT_FLUID).isEmpty() && fluidTank.getFluidAmount()>=1000 && FluidHelper.isFluidHandler(getStackInSlot(SLOT_FLUID))){
            if(getStackInSlot(SLOT_FLUID).getItem().equals(Items.BUCKET)){
                setInventorySlotContents(SLOT_FLUID,FluidUtil.getFilledBucket(new FluidStack(fluidTank.getFluid(),1000)));
                fluidTank.drain(1000,true);
            }else{
                IFluidHandler handler = getStackInSlot(SLOT_FLUID).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY,null);
                if (handler==null){
                    handler = getStackInSlot(SLOT_FLUID).getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY,null);
                }
                if (handler!=null){
                    int filled = handler.fill(fluidTank.getFluid(),true);
                    fluidTank.drain(filled,true);
                }
            }
        }
        if(world.getTotalWorldTime() % 20 == 0){
            recalcProcessTime();
        }
        super.update();
    }

    @Override
    public boolean processStart() {
        if(curRecipe == null){
            return false;
        }
        if(!getStackInSlot(1).isEmpty() && (!getStackInSlot(1).isItemEqual(curRecipe.getOutput()) || !ItemStack.areItemStackTagsEqual(getStackInSlot(1),curRecipe.getOutput()))){
            return false;
        }
        processMax = processRem = procTime;
        return true;
    }

    @Override
    public void processTick() {
        processRem--;
    }

    @Override
    public void processEnd() {
        int count = procTime==1?getNumberConsecutiveOperations():1;
        for(int i=0;i<count;i++){
            if(canStart()){
                make();
            }
        }
    }


    private void make() {
        if(curRecipe == null){
            return;
        }
        if(!curRecipe.getOutput().isEmpty()){
            Utils.distributeOutput(this,curRecipe.getOutput().copy(),1,2,true);
        }
        if(curRecipe.getData()!=null && CraftingPattern.hasFluids(curRecipe.getData())>0){
            fluidTank.fill(CraftingPattern.getFluid(curRecipe.getData())[0],true);
        }

        decrStackSize(0,((GrinderRecipe)curRecipe).getData().getInteger("input_count"));
    }



    private static int procTime = 256;
    private int getNumberConsecutiveOperations(){
        float ratio = (float) powerMachine.getPower().getSpeed() / (float)getMinimumPower().getSpeed();
        return Math.max(1,Math.min(Math.round(ratio / 256),4));
    }

    private void recalcProcessTime(){
        float ratio = (float) powerMachine.getPower().getSpeed() / (float)getMinimumPower().getSpeed();
        procTime = Math.max(1,Math.round(256-ratio));
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(128,32,0,0);
    }
    public FluidTank fluidTank = new FluidTank(Fluid.BUCKET_VOLUME*32);

    @Override
    public NBTTagCompound writeGuiTagCompound(NBTTagCompound tag) {
        tag.setTag("fluid",fluidTank.writeToNBT(new NBTTagCompound()));
        tag.setInteger("proctime",procTime);
        return super.writeGuiTagCompound(tag);
    }

    @Override
    public void readGuiTagCompound(NBTTagCompound tag) {
        if(tag.hasKey("fluid")){
            fluidTank.readFromNBT(tag.getCompoundTag("fluid"));
        }
        tag.getInteger("proctime");
        super.readGuiTagCompound(tag);
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        compound.setTag("fluid",fluidTank.writeToNBT(new NBTTagCompound()));
        super.readFromNBT(compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        if(compound.hasKey("fluid")){
            fluidTank.readFromNBT(compound.getCompoundTag("fluid"));
        }
        return super.writeToNBT(compound);
    }


    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(index==0){
            return true;
        }
        if(index==1){
            return false;
        }
        if(index==2){
            return FluidHelper.isFluidHandler(stack);
        }
        return false;
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelGrinder();
    }

    @Override
    public String getTexName() {
        return "grindertex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiGrinder(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerGrinder(this,player.inventory);
    }





    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return fluidTank.getTankProperties();
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    return 0;
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return fluidTank.drain(resource,doDrain);
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return fluidTank.drain(maxDrain,doDrain);
                }
            });
        }
        return super.getCapability(capability, side);
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) || super.hasCapability(capability, side);
    }
}
