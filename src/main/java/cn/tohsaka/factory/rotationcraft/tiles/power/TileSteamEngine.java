package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.thermal.IThermalMachine;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockSteamEngine;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelSteam;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerSteam;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiSteam;
import cn.tohsaka.factory.rotationcraft.init.Sounds;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.TempetureUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

@GameInitializer(after = BlockSteamEngine.class)
public class TileSteamEngine extends TileEngine implements IThermalMachine, IGuiProvider {
    public float temp;
    public FluidTank tank = new FluidTank(16000);
    public static void init(){
        GameRegistry.registerTileEntity(TileSteamEngine.class,new ResourceLocation(RotationCraft.MOD_ID,"tilesteamengine"));
    }
    public TileSteamEngine(){
        super(new RotaryPower(32,512));
    }
    boolean isPowered = false;
    boolean somthingchanged = false;

    @Override
    public void tick() {
        isActive = powerSource.getPower().isPowered();
        if(world.getTotalWorldTime() % 40 == 0) {
            addTemp(TempetureUtils.diffBlockTemp(temp, world, pos));
            if (somthingchanged) {
                NetworkDispatcher.dispatchMachineUpdate(this);
                markDirty();
            }

            if(temp>getDamagedTemp()){
                onDamage();
            }

            if(temp>=100 && tank.getFluidAmount()>=10){

                isPowered = true;
                powerSource.setPower(new RotaryPower(32,512,0,0));

                tank.drain(10,true);
                temp -=10;
                somthingchanged = true;
            }else{
                powerSource.setPower(RotaryPower.ZERO.copy());
                isPowered = false;
                somthingchanged = true;
            }
        }

    }

    @Override
    public void update() {
        super.update();
        if(world.isRemote){
            if(isActive){
                playsound();
            }else{
                stopsound();
            }
        }
    }

    @Override
    public void invalidate() {
        if(world.isRemote){
            stopsound();
        }
        super.invalidate();
    }

    @SideOnly(Side.CLIENT)
    public void playsound(){
        SoundHandler.startTileSound(Sounds.STEAMENGINE.getEvent(),0.5F,pos);
    }

    @SideOnly(Side.CLIENT)
    public void stopsound(){
        SoundHandler.stopTileSound(pos);
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiSteam(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerSteam(this,player.inventory);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setBoolean("isActive",isActive);
        tag.setFloat("temp",temp);
        tank.writeToNBT(tag);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        tank.readFromNBT(tag);
        temp = tag.getFloat("temp");
        isActive = tag.getBoolean("isActive");
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        tank.readFromNBT(compound);
        temp = compound.getFloat("temp");
        isActive = compound.getBoolean("isActive");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setBoolean("isActive",isActive);
        compound.setFloat("temp",temp);
        tank.writeToNBT(compound);
        return compound;
    }



    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler(){

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return tank.getTankProperties();
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    if(!resource.getFluid().equals(FluidRegistry.WATER)){
                        return 0;
                    }
                    return tank.fill(resource,doFill);
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }


    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelSteam();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "steamtex";
    }

    @Override
    public boolean canDamage() {
        return true;
    }

    @Override
    public float getDamagedTemp() {
        return 1000;
    }

    @Override
    public void onDamage() {
        world.setBlockToAir(pos);
        //world.playSound(pos.getX(),pos.getY(),pos.getZ(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.AMBIENT,1F,0F,true);
    }

    @Override
    public float getTemp() {
        return temp;
    }

    @Override
    public void setTemp(Float thermal) {
        temp = thermal;
    }

    @Override
    public void addTemp(Float thermal) {
        temp += thermal;
    }

    @Override
    public boolean isTempSource() {
        return false;
    }
}
