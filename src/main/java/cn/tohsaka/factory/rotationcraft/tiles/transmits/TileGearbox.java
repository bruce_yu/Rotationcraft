package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerTransformer;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerTransformer;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IGearboxRenderProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockGearbox16x;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockGearbox2x;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockGearbox4x;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockGearbox8x;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.gearbox.ModelGearbox;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.gearbox.ModelGearbox16;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.gearbox.ModelGearbox4;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.gearbox.ModelGearbox8;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.props.DamageSources;
import cn.tohsaka.factory.rotationcraft.utils.*;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@GameInitializer(after = BlockGearbox2x.class)
public class TileGearbox extends TileMachineBase implements ITickable, IGearboxRenderProvider, IModeProvider, IModelProvider, IMeterable {
    IPowerTransformer powerTransformer;
    public static void init(){
        GameRegistry.registerTileEntity(TileGearbox.class,new ResourceLocation(RotationCraft.MOD_ID,"tilegearbox"));
    }
    public FluidTank lubricantTank = new FluidTank(1000);
    public int gearratio = 2;
    public TileGearbox(){
        powerTransformer = new RotaryPowerTransformer(this) {

            @Nullable
            @Override
            public World getWorld() {
                return TileGearbox.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return TileGearbox.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return TileGearbox.this.getDest();
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing);
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing.getOpposite());
            }
        };
        powerTransformer.setPower(RotaryPower.ZERO);
    }
    public TileGearbox(int ratio){
        this();
        gearratio = ratio;
    }

    public float angleInput = 0;
    public float angleOutput = 0;
    public boolean somethingchanged = false;
    public float damage;
    @Override
    public void update() {
        if(world.isRemote){
            if(world.isRemote && powerTransformer.getPower().isPowered()){
                angleInput = AxlsUtils.calcAngle(angleInput,powerTransformer.getPowerIn().getSpeed());
                angleOutput = AxlsUtils.calcAngle(angleOutput,powerTransformer.getPower().getSpeed());
            }
        }else{
            updatePowerInput();
            if (isActive() && getMetadata()<2 && lubricantTank.getFluidAmount()==0){
                damage += getMetadata()==0?0.000125:0.00003125;
                if(damage >1){
                    damage = 1;
                }
                somethingchanged = true;
            }

            if(damage<1){
                if(isDownshift()){
                    RotaryPower power = new RotaryPower(powerTransformer.getPowerIn().getTorque()*(long)gearratio,powerTransformer.getPowerIn().getSpeed()/(long)gearratio,angleOutput,powerTransformer.getPowerIn().getBlend());
                    powerTransformer.setPower(power);
                }else{
                    powerTransformer.setPower(new RotaryPower(powerTransformer.getPowerIn().getTorque()/(long)gearratio,powerTransformer.getPowerIn().getSpeed()*(long)gearratio,angleOutput,powerTransformer.getPowerIn().getBlend()));
                }

                if(getMetadata()==0 && powerTransformer.getPower().getWatt()>0 && world.getTotalWorldTime() % 20 == 0){
                    lubricantTank.drain(1,true);
                }

                somethingchanged = true;
            }else {
                powerTransformer.setPower(RotaryPower.ZERO);
            }

            if(somethingchanged){
                NetworkDispatcher.dispatchMachineUpdate(this);
                markDirty();
                somethingchanged = false;
            }




            if(isActive() && getMetadata()<2){
                RotaryPower maxium = RotaryPower.MAXPOWER_BEDROCK;
                switch (getMetadata()){
                    case 0:
                        maxium = RotaryPower.MAXPOWER_HSLA;
                        break;
                    case 1:
                        maxium = RotaryPower.MAXPOWER_DIAMOND;
                        break;
                }

                if(powerTransformer.getPower().getWatt() > maxium.getWatt() &&
                        (powerTransformer.getPower().getSpeed() > maxium.getSpeed() || powerTransformer.getPower().getTorque() > maxium.getTorque())){
                    this.invalidatebyerror();
                }

            }

        }
    }

    public void invalidatebyerror(){
        WorldUtils.makeExplode(pos,16,world,(int) Math.round(powerTransformer.getPowerIn().getWatt()/1000L), getDamagedDrops(), DamageSources.GEAR_EXPLODE);
        world.removeTileEntity(pos);
        this.world.setBlockToAir(pos);
        super.invalidate();
        world.setBlockToAir(pos);
    }

    public List<ItemStack> getDamagedDrops(){
        List<ItemStack> list = new ArrayList<>();
        switch (getMetadata()){
            case 0:
                list.add(new ItemStack(Items.IRON_INGOT,gearratio));
                break;
            case 1:
                list.add(new ItemStack(Items.DIAMOND,gearratio));
                break;
        }

        return list;
    }


    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {

    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {

    }


    @Override
    public float getInputAngle() {
        return (float) angleInput;
    }

    @Override
    public float getOutputAngle() {
        return (float) angleOutput;
    }

    boolean mode = false;

    @Override
    public boolean isDownshift() {
        return mode;
    }

    public void setMode(boolean m) {
        mode = m;
        markDirty();
    }

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        setMode(!mode);
        return true;
    }


    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        drawables.add(new RenderUtils.RenderItem(0x0000FF, getSrc() ,1F));
        drawables.add(new RenderUtils.RenderItem(0xFF0000, getDest(),1F));

        super.getOutBoxList(drawables);
    }

    public BlockPos getSrc() {
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1);
    }

    public BlockPos getDest() {
        return FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1);
    }



    public void fix(){
        damage = 0;
        markDirty();
    }



    protected void updatePowerInput(){

        IPowerMachine cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),FacingTool.revert(facing));
        if(cap!=null){
            powerTransformer.setPowerIn(cap.getPower());
        }else{
            powerTransformer.setPowerIn(RotaryPower.ZERO);
        }
    }




    @Override
    public boolean isActive() {
        return powerTransformer.getPowerIn().isPowered();
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setInteger("ratio",gearratio);
        tag.setTag("lubricant",lubricantTank.writeToNBT(new NBTTagCompound()));
        tag.setFloat("damage",damage);
        tag.setBoolean("mode",mode);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        gearratio = tag.getInteger("ratio");
        lubricantTank.readFromNBT(tag.getCompoundTag("lubricant"));
        damage = tag.getFloat("damage");
        mode = tag.getBoolean("mode");
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
       readFromNBTSelf(compound);
    }

    public void readFromNBTSelf(NBTTagCompound compound) {
        gearratio = compound.getInteger("ratio");
        mode = compound.getBoolean("mode");
        damage = compound.getFloat("damage");
        lubricantTank.readFromNBT(compound.getCompoundTag("lubricant"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tag = super.writeToNBT(compound);
        tag.setInteger("ratio",gearratio);
        tag.setBoolean("mode",isDownshift());
        tag.setTag("lubricant",lubricantTank.writeToNBT(new NBTTagCompound()));
        tag.setFloat("damage",damage);
        return tag;
    }


    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        tooltip.add(StringHelper.localize("info.rc.waila.names.mode")+":"+StringHelper.localize("info.rc.waila.names.gearbox.mode"+(isDownshift()?"A":"B")));
        if(damage>0){
            tooltip.add(StringHelper.localize("info.rc.machinedamaged")+":"+String.format("%.3f",damage));
        }
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if(isInvalid()){
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(), ItemMaterial.getStackById(2)));
            return;
        }
        if(!player.isCreative()){
            ItemStack itemStack = ItemStack.EMPTY;
            switch (gearratio){
                case 2:
                    itemStack = new ItemStack(RotationCraft.blocks.get(BlockGearbox2x.NAME),1,getMetadata());
                    break;
                case 4:
                    itemStack = new ItemStack(RotationCraft.blocks.get(BlockGearbox4x.NAME),1,getMetadata());
                    break;
                case 8:
                    itemStack = new ItemStack(RotationCraft.blocks.get(BlockGearbox8x.NAME),1,getMetadata());
                    break;
                case 16:
                    itemStack = new ItemStack(RotationCraft.blocks.get(BlockGearbox16x.NAME),1,getMetadata());
                    break;
            }
            if(!itemStack.isEmpty()){
                itemStack.setTagCompound(writeToNBT(new NBTTagCompound()));
                world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(),itemStack));
            }
        }
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null){
                return true;
            }
            if(FacingTool.revert(side)==this.facing){
                return true;
            }
            return false;
        }
        if(getMetadata()==2){
            return super.hasCapability(capability, side);
        }
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY || super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return lubricantTank.getTankProperties();
                }


                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    if (resource == null || !FluidHelper.isFluidEqual(RotationCraft.fluids.get("lubricant"),resource.getFluid()) || lubricantTank.getFluidAmount()>0) {
                        return 0;
                    }
                    int amount = lubricantTank.fill(resource, doFill);
                    markDirty();
                    return amount;
                }



                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }


        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerTransformer);
            }
            if(FacingTool.revert(side)==this.facing){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerTransformer);
            }
        }
        return super.getCapability(capability, side);
    }

    @Override
    public RotaryModelBase getModel() {
        switch (gearratio){
            case 2:
                return new ModelGearbox();
            case 4:
                return new ModelGearbox4();
            case 8:
                return new ModelGearbox8();
            case 16:
                return new ModelGearbox16();
        }
        return null;
    }

    @Override
    public String getTexName() {
        return String.format("transmission/gear/geartex%d",getMetadata());
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {
        if(stack.getUnlocalizedName().equals("gearbox2x")){
            this.gearratio = 2;
        }
        if(stack.getUnlocalizedName().equals("gearbox4x")){
            this.gearratio = 4;
        }
        if(stack.getUnlocalizedName().equals("gearbox8x")){
            this.gearratio = 8;
        }
        if(stack.getUnlocalizedName().equals("gearbox16x")){
            this.gearratio = 16;
        }
        if(stack.hasTagCompound()){
            lubricantTank.readFromNBT(stack.getTagCompound().getCompoundTag("lubricant"));
        }
        setMetadata(stack.getMetadata());
    }

    @Override
    public void beginItemRender() {

    }


    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append("PowerIn: " + powerTransformer.getPowerIn()+"\n");
        sb.append(String.format("PowerOut[%s]: ",isDownshift()?(gearratio+":1"):"1:"+gearratio) + powerTransformer.getPower()+"\n");
        return sb.toString();
    }

    @Override
    public void onMeter() {
        addIO();
    }
}
