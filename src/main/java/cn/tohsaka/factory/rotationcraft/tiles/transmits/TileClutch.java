package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerForwarder;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerSource;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerForwarder;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerTranmitsAccelerator;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockMeter;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelClutch;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.props.DamageSources;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.WorldUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
@GameInitializer
public class TileClutch extends TileMachineBase implements RenderUtils.IOutBoxDrawable, ITickable, IModelProvider, IModeProvider, IPowerTranmitsAccelerator, IMeterable {
    public ClutchPowerForwarder powerForwarder;
    public RotaryPowerSource zeroPower;
    public TileClutch(){
        powerForwarder = new ClutchPowerForwarder(this);
        zeroPower = new RotaryPowerSource(RotaryPower.ZERO,this) {
            @Nullable
            @Override
            public World getWorld() {
                return TileClutch.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return TileClutch.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return TileClutch.this.getSrc();
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return null;
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing.getOpposite());
            }
        };
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileClutch.class,new ResourceLocation(RotationCraft.MOD_ID,"tileclutch"));
    }
    @Override
    public void update() {
        if(world.isRemote){
            if(powerForwarder.getPower().isPowered()) {
                angle = AxlsUtils.calcAngle(angle, powerForwarder.getPower().getSpeed());
            }
        }else{
            updatePowerInput();
        }
    }


    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        if(facing.ordinal()>1){
            drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1),1F));
            drawables.add(new RenderUtils.RenderItem(0xFF0000,FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),1F));

        }else{
            if(facing == EnumFacing.UP){
                drawables.add(new RenderUtils.RenderItem(0x0000FF, pos.add(0,1,0),1F));
                drawables.add(new RenderUtils.RenderItem(0xFF0000,pos.add(0,-1,0),1F));
            }else{
                drawables.add(new RenderUtils.RenderItem(0x0000FF, pos.add(0,-1,0),1F));
                drawables.add(new RenderUtils.RenderItem(0xFF0000,pos.add(0,1,0),1F));
            }
        }
        super.getOutBoxList(drawables);
    }


    public BlockPos getSrc() {
        if(facing.ordinal()<2){
            if(facing==EnumFacing.UP){
                return pos.add(0,-1,0);
            }else{
                return pos.add(0,1,0);
            }
        }
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1);
    }

    @Override
    public BlockPos getDst() {
        if(facing.ordinal()<2){
            if(facing==EnumFacing.UP){
                return pos.add(0,1,0);
            }else{
                return pos.add(0,-1,0);
            }
        }
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK,1);
    }

    public boolean nomode = false;
    public boolean isNO(){
        return nomode;
    }

    @Override
    public boolean isActive() {
        if(!powerForwarder.getPower().isPowered()){
            return false;
        }

        if(nomode){
            return !world.isBlockPowered(pos);
        }

        return world.isBlockPowered(pos);
    }


    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        if(!world.isRemote){
            nomode = !nomode;
            NetworkDispatcher.dispatchMachineUpdate(this);
            this.markDirty();
        }
        return true;
    }



    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        tooltip.add("Mode:"+(nomode?"NO":"NC"));
    }






    protected void updatePowerInput(){
        if(facing.ordinal()<2){
            if(facing==EnumFacing.UP){
                updatePowerInput(FacingTool.Position.UP);
            }else{
                updatePowerInput(FacingTool.Position.DOWN);
            }
        }else{
            updatePowerInput(FacingTool.Position.FRONT);
        }
    }
    protected void updatePowerInput(FacingTool.Position position){
        EnumFacing side = FacingTool.revert(facing);
        IPowerMachine cap = null;
        if(facing.ordinal()>1){
            cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, position,1),side);
        }else{
            cap = CapabilityRotaryPower.getCapability(world, pos.add(0,(position== FacingTool.Position.UP)?1:-1,0),side);
        }
        if(cap!=null){
            RotaryPower powerIn = cap.getPower();
            powerForwarder.setPower(powerIn);
        }else{
            powerForwarder.setPower(RotaryPower.ZERO);
        }
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("nomode")){
            nomode = compound.getBoolean("nomode");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setBoolean("nomode",nomode);
        return super.writeToNBT(compound);
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(facing == null){
                return true;
            }
            if(facing == FacingTool.revert(this.facing)){
                return true;
            }
            return false;
        }
        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(facing == null){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerForwarder);
            }
            if(facing == FacingTool.revert(this.facing)){
                if(nomode){
                    if(isActive()){
                        return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerForwarder);
                    }
                    return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(zeroPower);
                }else{
                    if(!isActive()){
                        return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(zeroPower);
                    }
                    return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerForwarder);
                }
            }
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public void onPowerChange() {
        updatePowerInput();
    }

    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append(powerForwarder.getPower()+"\n");
        return sb.toString();
    }

    @Override
    public void onMeter() {
        addIO();
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelClutch();
    }

    @Override
    public String getTexName() {
        return "transmission/shaft/shafttex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }


    static class ClutchPowerForwarder extends RotaryPowerForwarder{

        public ClutchPowerForwarder(TileEntity tileEntity) {
            super(tileEntity);
        }

        @Nullable
        @Override
        public World getWorld() {
            return ((TileClutch)this.tileEntity).getWorld();
        }

        @Nullable
        @Override
        public BlockPos getSrc() {
            return ((TileClutch)this.tileEntity).getSrc();
        }

        @Nullable
        @Override
        public BlockPos getDest() {
            return ((TileClutch)this.tileEntity).getDst();
        }

        @Nullable
        @Override
        public IPowerMachine getParent() {
            TileClutch te = ((TileClutch)this.tileEntity);
            return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,te.pos,te.world,te.facing);
        }

        @Nullable
        @Override
        public IPowerMachine getChild() {
            TileClutch te = ((TileClutch)this.tileEntity);
            return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,te.pos,te.world,te.facing.getOpposite());
        }

    }
}
