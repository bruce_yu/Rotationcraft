package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockAc;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelAC;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerAc;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiAc;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;

@GameInitializer(after = BlockAc.class)
public class TileAc extends TileEngine implements IGuiProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileAc.class,new ResourceLocation(RotationCraft.MOD_ID,"tileac"));
    }
    public TileAc(){
        super(new RotaryPower(256,512));
        createSlots(1);
        setSlotMode(SlotMode.INPUT,new int[]{0});
    }
    ArrayBlockingQueue<Boolean> status = new ArrayBlockingQueue<>(16);

    @Override
    public void update() {
        if(world.isRemote){
            angle = AxlsUtils.calcAngle(angle,powerSource.getPower().getSpeed());
        }else{
            if (getStackInSlot(0).isEmpty() ||
                    !getStackInSlot(0).hasTagCompound() ||
                    !getStackInSlot(0).getTagCompound().hasKey("magnet") ||
                    getStackInSlot(0).getTagCompound().getDouble("magnet")<=0
            ){
                if(powerSource.getPower().getTorque()>0){
                    powerSource.setPower(RotaryPower.ZERO);
                }
                return;
            }
            if(status.size()>=16){
                status.poll();
            }
            status.add(world.isBlockPowered(pos));
            if(world.getTotalWorldTime() % 20 == 0){
                tick();
            }
        }
    }

    @Override
    public void tick() {
        Iterator<Boolean> iter = status.iterator();
        if((status.stream().allMatch( e ->  e==true) || status.stream().allMatch( e ->  e==false))){
            powerSource.setPower(RotaryPower.ZERO);
            return;
        }
        long i=0;
        while (iter.hasNext()){
            if(iter.next()){
                i++;
            }
        }
        if(i>0){
            RotaryPower m = powerSource.getMaximumOutput();
            powerSource.setPower(new RotaryPower(512 / 16L * Math.min(i,16-i),512));
            NBTTagCompound tagCompound = getStackInSlot(0).getTagCompound();
            tagCompound.setDouble("magnet",Math.max(tagCompound.getDouble("magnet")-0.0001,0));
            getStackInSlot(0).setTagCompound(tagCompound);
        }else {
            powerSource.setPower(RotaryPower.ZERO);
        }

    }
    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelAC();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "actex";
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return (stack.getItem() instanceof ItemMaterial && stack.hasTagCompound() && stack.getTagCompound().hasKey("magnet"));
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiAc(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerAc(this,player.inventory);
    }
}
