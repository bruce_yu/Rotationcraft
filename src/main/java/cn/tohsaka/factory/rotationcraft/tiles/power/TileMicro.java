package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IControlableEngine;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockDc;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelMicroTurbine;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerMicro;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiMicro;
import cn.tohsaka.factory.rotationcraft.init.Sounds;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.UniversalBucket;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockDc.class)
public class TileMicro extends TileEngine implements IGuiProvider, IControlableEngine {
    public static void init(){
        GameRegistry.registerTileEntity(TileMicro.class,new ResourceLocation(RotationCraft.MOD_ID,"tilemicro"));
    }
    public FluidTank fuelTank;
    public TileMicro(){
        super(new RotaryPower(16,131072));
        fuelTank= new FluidTank(1000 * 16);
        createSlots(1);
        setSlotMode(SlotMode.OMNI,new int[]{0});
    }
    float startTime = 0;

    @SideOnly(Side.CLIENT)
    public void playsound(){
        SoundHandler.startTileSound(Sounds.MICROENGINE.getEvent(),0.5F,pos);
    }

    @SideOnly(Side.CLIENT)
    public void stopsound(){
        SoundHandler.stopTileSound(pos);
    }

    @Override
    public void update() {
        if(!getStackInSlot(0).isEmpty() && (fuelTank.getCapacity()-fuelTank.getFluidAmount())>0){
            if(getStackInSlot(0).getItem() instanceof UniversalBucket){
                if((fuelTank.getCapacity()-fuelTank.getFluidAmount())>=1000){
                    FluidStack fluid = FluidHelper.getFluidForFilledItem(getStackInSlot(0));
                    if(fluid.getFluid().equals(RotationCraft.fluids.get("jetfuel"))){
                        fuelTank.fill(fluid,true);
                        setInventorySlotContents(0,new ItemStack(Items.BUCKET));
                    }
                }
            }else{
                IFluidHandler handler = getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY,null);
                if (handler==null){
                    handler = getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY,null);
                }
                if (handler!=null){
                    FluidStack stack = handler.drain(new FluidStack(RotationCraft.fluids.get("jetfuel"),1000),true);
                    fuelTank.fill(stack,true);
                }
            }
        }
        super.update();
        if(world.isRemote){
            if(isActive){
                playsound();
            }else{
                stopsound();
            }
        }
    }
    @Override
    public void invalidate() {
        if(world.isRemote){
            stopsound();
        }
        super.invalidate();
    }

    @Override
    public void tick() {
        if(fuelTank.getFluidAmount() > 0){
            if(isActive){
                if(startTime<=1900){
                    startTime++;
                    if(GeneralConfig.turbin_nodelay || startTime%38 == 0){
                        float per = startTime / 1900F;
                        powerSource.setPower(new RotaryPower(16,Math.round(131072F*per),0,0));
                        // update output
                    }else{
                        float per = startTime / 1900F;
                        powerSource.setPower(new RotaryPower(16,Math.round(131072F*per),0,0));
                    }
                    if((world.getTotalWorldTime() % 20) == 0){
                        fuelTank.drain(8,true);
                    }
                }else{
                    if(startTime>1900){
                        powerSource.setPower(new RotaryPower(16,Math.round(131072 * effective),0,0));
                    }
                    if((world.getTotalWorldTime() % (Math.round(20f / effective))) == 0){
                        fuelTank.drain(8,true);
                    }
                }


            }else{
                startTime = 0;
                isActive = true;
                fuelTank.drain(40,true);
            }
        }else{
            isActive = false;
            startTime = 0;
            powerSource.setPower(RotaryPower.ZERO);
        }
        if(world.getTotalWorldTime() % 100 == 0){
            markDirty();
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        fuelTank.readFromNBT(compound);
        startTime = compound.getFloat("startTime");
        isActive = compound.getBoolean("isActive");
        if(compound.hasKey("effective")){
            effective = compound.getFloat("effective");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setFloat("startTime",startTime);
        compound.setBoolean("isActive",isActive);
        compound.setFloat("effective",effective);
        fuelTank.writeToNBT(compound);
        return compound;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler(){

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return fuelTank.getTankProperties();
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    if(!resource.getFluid().equals(RotationCraft.fluids.get("jetfuel"))){
                        return 0;
                    }
                    return fuelTank.fill(resource,doFill);
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiMicro(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerMicro(this,player.inventory);
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add(StringHelper.localize("info.rc.waila.names.fuel")+":"+String.valueOf(fuelTank.getFluidAmount()));
        if(fuelTank.getFluidAmount()<=0){
            return;
        }
        tooltip.add(StringHelper.localize("info.rc.waila.names.rtime")+":"+Math.round(fuelTank.getFluidAmount()/2));
    }
    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelMicroTurbine();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "microtex";
    }

    private float effective = 1f;

    @Override
    public float getEffective() {
        return effective;
    }

    @Override
    public void setEffective(float e) {
        effective = e;
        if(isActive && startTime>1900){
            powerSource.setPower(new RotaryPower(16,Math.round(131072 * effective),0,0));
        }
        markDirty();
    }
}
