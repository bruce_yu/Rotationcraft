package cn.tohsaka.factory.rotationcraft.etc.tree;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockLog;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class FallingTreeUtils{
	public static Set<Item> getAsItems(String[] names){
		return Arrays.stream(names).map(FallingTreeUtils::getItem).filter(Objects::nonNull).collect(Collectors.toSet());
	}
	
	@Nullable
	public static Item getItem(String name){
		try{
			return ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
		}
		catch(Exception e){
			return null;
		}
	}
	
	public static Set<Block> getAsBlocks(String[] names){
		return Arrays.stream(names).map(FallingTreeUtils::getBlock).filter(Objects::nonNull).collect(Collectors.toSet());
	}
	
	@Nullable
	public static Block getBlock(String name){
		try{
			return ForgeRegistries.BLOCKS.getValue(new ResourceLocation(name));
		}
		catch(Exception e){
			return null;
		}
	}
	
	public static boolean isTreeBlock(@Nonnull Block block){
		return block instanceof BlockLog;
	}
	
	public static boolean isLeafBlock(@Nonnull Block block){
		return block instanceof BlockLeaves;
	}
	
	public static boolean canPlayerBreakTree(@Nonnull EntityPlayer player){
		return true;
	}
}