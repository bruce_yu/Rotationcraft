package cn.tohsaka.factory.rotationcraft.etc.reikamodel.helper;

import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class ReikaModHelper {
    public static void bindTexture(ResourceLocation resourceLocation){
        Minecraft.getMinecraft().renderEngine.bindTexture(resourceLocation);
    }

    public static void setupGL(TileMachineBase tile, double x, double y, double z) {
        GlStateManager.pushMatrix();
        GlStateManager.enableRescaleNormal();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.translate(x, y, z);
        GlStateManager.scale(1.0F, -1.0F, -1.0F);
        GlStateManager.translate(0.5F, 0.5F, 0.5F);
        GlStateManager.translate(0F,-2F,-1F);
        RenderUtils.rotate(tile.getFacing(), 270, 90, 180, 0);

        //OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);

    }
    public static void closeGL(TileMachineBase tile,double x,double y,double z){
        GlStateManager.popMatrix();
    }
}
