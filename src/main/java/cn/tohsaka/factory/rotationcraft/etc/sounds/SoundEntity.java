package cn.tohsaka.factory.rotationcraft.etc.sounds;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.Sound;
import net.minecraft.client.audio.SoundEventAccessor;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.client.audio.SoundHandler;
import javax.annotation.Nullable;

public class SoundEntity implements ISound {

	private ResourceLocation location;
	private SoundEventAccessor soundEvent;
	private Sound sound;
	private boolean repeat;

	public SoundEntity(SoundEvent se,boolean repeat) {
		location = se.getSoundName();
		this.repeat = repeat;
		try {
			createAccessor(Minecraft.getMinecraft().getSoundHandler());
		} catch (Throwable e) {}
	}

	@Override
	public ResourceLocation getSoundLocation() {
		return location;
	}

	@Override
	public SoundEventAccessor createAccessor(SoundHandler handler) {
		soundEvent = handler.getAccessor(location);

		if (soundEvent == null)
			sound = SoundHandler.MISSING_SOUND;
		else
			sound = soundEvent.cloneEntry();

		return soundEvent;
	}

	@Override
	public Sound getSound() {
		return sound;
	}

	@Override
	public SoundCategory getCategory() {
		return SoundCategory.PLAYERS;
	}

	@Override
	public boolean canRepeat() {
		return repeat;
	}

	@Override
	public int getRepeatDelay() {
		return 10;
	}

	@Override
	public float getVolume() {
		return 100;
	}

	@Override
	public float getPitch() {
		return 0;
	}

	@Override
	public float getXPosF() {
		return 0;
	}

	@Override
	public float getYPosF() {
		return 0;
	}

	@Override
	public float getZPosF() {
		return 0;
	}

	@Override
	public AttenuationType getAttenuationType() {
		return AttenuationType.LINEAR;
	}

}