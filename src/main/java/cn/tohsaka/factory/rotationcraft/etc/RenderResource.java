package cn.tohsaka.factory.rotationcraft.etc;

import cn.tohsaka.factory.rotationcraft.items.ItemResource;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

public class RenderResource implements IItemColor {
    @Override
    public int colorMultiplier(@Nonnull ItemStack stack, int tintIndex) {
        if (stack.isEmpty()) {
            return 0;
        }
        if(stack.getItem() instanceof ItemResource){
            return ((ItemResource)stack.getItem()).color;
        }
        return 0xFFFFFF;
    }


}