package cn.tohsaka.factory.rotationcraft.etc;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.ISpectialItemRender;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import java.util.HashMap;
import java.util.Map;

public class ItemRender extends TileEntityItemStackRenderer {
    private static ItemRender itemRender;
    public static ItemRender INSTANCE(){
        if(itemRender==null){
            itemRender = new ItemRender();
        }
        return itemRender;
    }
    private Map<String,TileEntity> entityMap = new HashMap<>();
    private Map<String,RotaryModelBase> modelMap = new HashMap<>();
    @Override
    public void renderByItem(ItemStack stack, float partialTicks) {
        if(stack.getItem() instanceof ItemBlock && ((ItemBlock) stack.getItem()).getBlock() instanceof BlockBase){
            BlockBase block = (BlockBase) ((ItemBlock) stack.getItem()).getBlock();
            String hashcode = block.getUnlocalizedName()+":"+stack.getMetadata();
            if(stack.hasTagCompound()){
                hashcode+=","+stack.getTagCompound().toString();
            }
            TileEntity te;
            if(entityMap.containsKey(hashcode)){
                te = entityMap.get(hashcode);
            }else{
                te = ((ITileEntityProvider)block).createNewTileEntity(null,stack.getMetadata());
                entityMap.put(hashcode,te);
            }

            if(te!=null && te instanceof IModelProvider){
                if(te instanceof ISpectialItemRender){
                    ((IModelProvider) te).updateInformationForTEISR(stack,te,partialTicks);
                    RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/"+((IModelProvider) te).getTexName()+".png"));
                    ((ISpectialItemRender) te).renderItemSpectial();
                    return;
                }
                ((IModelProvider) te).updateInformationForTEISR(stack,te,partialTicks);

                RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/"+((IModelProvider) te).getTexName()+".png"));
                GlStateManager.pushMatrix();

                GlStateManager.translate(0.5,1.5,0.5);
                GlStateManager.rotate(180,1,0,0);
                GlStateManager.rotate(180,0,1,0);
                RotaryModelBase model;
                if(modelMap.containsKey(hashcode)){
                    model = modelMap.get(hashcode);
                }else{
                    model = ((IModelProvider) te).getModel();
                    modelMap.put(hashcode,model);
                }
                ((IModelProvider) te).beginItemRender();
                model.renderAll(te,null,0);
                model.renderExtra(te);
                GlStateManager.popMatrix();
            }
        }
    }
}
