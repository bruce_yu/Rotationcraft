package cn.tohsaka.factory.rotationcraft.etc;

import cn.tohsaka.factory.rotationcraft.items.ItemCrushedOre;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.Sys;

import javax.annotation.Nonnull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RenderOrePiece implements IItemColor {
    private Map<String,Integer> colorMap= new HashMap();
    @Override
    public int colorMultiplier(@Nonnull ItemStack stack, int tintIndex) {
        if (stack.isEmpty())
            return 0;



        if (stack.getItem() instanceof ItemCrushedOre) {
            try {
                if(stack.hasTagCompound() && stack.getTagCompound().hasKey("ore")){
                    ItemStack st2 = new ItemStack(stack.getTagCompound().getCompoundTag("ore"));
                    String id = st2.getUnlocalizedName()+":"+st2.getMetadata();
                    if(colorMap.containsKey(id)){
                        return colorMap.get(id);
                    }

                    if(st2.isEmpty()){
                        return Color.WHITE.getRGB();
                    }
                    IBlockState state = ((ItemBlock)st2.getItem()).getBlock().getStateFromMeta(st2.getMetadata());
                    ResourceLocation reg = st2.getItem().getRegistryName();
                    ResourceLocation location = new ResourceLocation(reg.toString());
                    Minecraft minecraft = Minecraft.getMinecraft();
                    BlockRendererDispatcher ren = minecraft.getBlockRendererDispatcher();
                    TextureAtlasSprite texture = ren.getModelForState(state).getQuads(state, EnumFacing.NORTH, 0).get(0).getSprite();
                    System.out.println(texture);
                    //return getColorFromItemStack(st2,tintIndex);
                    Color c = new Color(texture.getFrameTextureData(0)[0][102]);
                    colorMap.put(id,c.getRGB());
                    return c.getRGB();
                }
            }catch (Exception e){
                e.printStackTrace();
                return Color.WHITE.getRGB();
            }

        }
        return Color.WHITE.getRGB();
    }


    /*public int[] getColors(ItemStack stack){
        IBakedModel model = Minecraft.getMinecraft().getRenderItem().getItemModelWithOverrides(stack,null,null);
        List<BakedQuad> quads = model.getQuads(((ItemBlock)stack.getItem()).getBlock().getStateFromMeta(stack.getMetadata()), EnumFacing.NORTH,0);
        quads.get(0).getSprite().getDependencies()[0]
        return res;
    }*/
    @SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack stack, int renderPass) {
        if (stack.getItem() instanceof ItemBlock) {
                return Minecraft.getMinecraft().getBlockColors().colorMultiplier(((ItemBlock) stack.getItem()).getBlock().getStateFromMeta(stack.getMetadata()), null, null, renderPass);
            }
        return Minecraft.getMinecraft().getItemColors().colorMultiplier(stack, renderPass);
    }

}