/*******************************************************************************
 * @author Reika Kalseki
 * 
 * Copyright 2017
 * 
 * All rights reserved.
 * Distribution of the software in any form is only allowed with
 * explicit, prior permission from the owner.
 ******************************************************************************/
package cn.tohsaka.factory.rotationcraft.etc;

import java.util.ArrayList;

import net.minecraft.tileentity.TileEntity;

public interface TileModel {

	public void renderAll(TileEntity tile, ArrayList li);

}