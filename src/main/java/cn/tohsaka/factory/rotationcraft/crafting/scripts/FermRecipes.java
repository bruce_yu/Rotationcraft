package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.Loader;

import java.util.ArrayList;
import java.util.List;


@GameInitializer
public class FermRecipes implements INeedPostInit {
    public static void postinit(){
        List<CraftingPattern> patternList = new ArrayList<>();
        NBTTagCompound water = CraftingPattern.setFluid(new NBTTagCompound(),"fluid1",new FluidStack(FluidRegistry.WATER,250));
        NBTTagCompound water2 = CraftingPattern.setFluid(new NBTTagCompound(),"fluid1",new FluidStack(FluidRegistry.WATER,4000));
        patternList.add(new CraftingPattern(new String[]{"ore:treeLeaves","Minecraft:dirt"},new ItemStack(ItemMaterial.materialMap.get(52),2),water.copy()));
        patternList.add(new CraftingPattern(new String[]{"rc:material52,1","Minecraft:dye,1,15"},new ItemStack(ItemMaterial.materialMap.get(50),8),CraftingPattern.setRedstone(water.copy())));
        patternList.add(new CraftingPattern(new String[]{"rc:material52,1", "Minecraft:sugar"},new ItemStack(ItemMaterial.materialMap.get(60),8),CraftingPattern.setRedstone(water.copy())));

        patternList.add(new CraftingPattern(new String[]{"minecraft:cake", "rc:canola,4"},new ItemStack(Blocks.SLIME_BLOCK,4),CraftingPattern.setRedstone(water2.copy())));


        if(Loader.isModLoaded("ic2")){
            patternList.add(new CraftingPattern(new String[]{"ic2:leaves","Minecraft:dirt"},new ItemStack(ItemMaterial.materialMap.get(52),16),water.copy()));
        }

        Recipes.INSTANCE.addPattern(TileFerm.class,patternList);
    }
}
