package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.WeightedRandom;
import net.minecraftforge.oredict.OreDictionary;

import java.util.*;

@GameInitializer
public class GlobalOre {
    public static boolean initialized = false;
    public static void init2(){
        oreWeights.put(Ingredient.fromStacks(ItemStack.EMPTY),20000);
        addOreWeight("oreAluminum", 3940); // Tinkers' Construct
        addOreWeight("oreAmber", 2075); // Thaumcraft
        addOreWeight("oreApatite", 1595); // Forestry
        addOreWeight("oreBlueTopaz", 3195); // Ars Magica
        addOreWeight("oreCertusQuartz", 4275); // Applied Energistics
        addOreWeight("oreChimerite", 3970); // Ars Magica
        addOreWeight("oreCinnabar",  2585); // Thaumcraft
        addOreWeight("oreCoal", 32525); // Vanilla
        addOreWeight("oreCopper", 8325); // IC2, Thermal Expansion, Tinkers' Construct, etc.
        addOreWeight("oreDark", 1350); // EvilCraft
        addOreWeight("oreDarkIron", 1700); // Factorization (older versions)
        addOreWeight("oreFzDarkIron", 1700); // Factorization (newer versions)
        addOreWeight("oreDiamond", 1265); // Vanilla
        addOreWeight("oreEmerald", 780); // Vanilla
        addOreWeight("oreGalena", 1000); // Factorization
        addOreWeight("oreGold", 2970); // Vanilla
        addOreWeight("oreInfusedAir", 1225); // Thaumcraft
        addOreWeight("oreInfusedEarth", 1225); // Thaumcraft
        addOreWeight("oreInfusedEntropy", 1225); // Thaumcraft
        addOreWeight("oreInfusedFire", 1225); // Thaumcraft
        addOreWeight("oreInfusedOrder", 1225); // Thaumcraft
        addOreWeight("oreInfusedWater", 1225); // Thaumcraft
        addOreWeight("oreIron", 18665); // Vanilla
        addOreWeight("oreLapis", 1485); // Vanilla
        addOreWeight("oreLead", 7985); // IC2, Thermal Expansion, Factorization, etc.
        addOreWeight("oreMCropsEssence", 3085); // Magical Crops
        addOreWeight("oreMithril", 8); // Thermal Expansion
        addOreWeight("oreNickel", 2275); // Thermal Expansion
        addOreWeight("oreClathrateEnder",355);
        addOreWeight("oreClathrateGlowstone",535);
        addOreWeight("oreOlivine", 1100); // Project RED
        addOreWeight("orePlatinum", 565); // Thermal Expansion
        addOreWeight("oreRedstone", 6885); // Vanilla
        addOreWeight("oreRuby", 1100); // Project RED
        addOreWeight("oreSapphire", 1100); // Project RED
        addOreWeight("oreSilver", 6300); // Thermal Expansion, Factorization, etc.
        addOreWeight("oreSulfur", 1105); // Railcraft
        addOreWeight("oreTin", 9450); // IC2, Thermal Expansion, etc.
        addOreWeight("oreUranium", 5337); // IC2
        addOreWeight("oreVinteum", 5925); // Ars Magica
        addOreWeight("oreYellorite", 3520); // Big Reactors
        addOreWeight("oreZinc", 6485); // Flaxbeard's Steam Power
        addOreWeight("oreMythril", 6485); // Simple Ores2
        addOreWeight("oreAdamantium", 2275); // Simple Ores2
        addOreWeight("oreTungsten", 3520); // Simple Tungsten
        addOreWeight("oreOsmium", 6915); // Mekanism
        addOreWeight("oreQuartzBlack", 5535); // Actually Additions

        addOreWeight("oreQuartz", 19600); // Vanilla
        addOreWeight("oreCobalt", 500); // Tinker's Construct
        addOreWeight("oreArdite", 500); // Tinker's Construct
        addOreWeight("oreFirestone", 5); // Railcraft
        addOreWeight("oreNetherCoal", 17000); // Nether Ores
        addOreWeight("oreNetherCopper", 4700); // Nether Ores
        addOreWeight("oreNetherDiamond", 175); // Nether Ores
        addOreWeight("oreNetherEssence", 2460); // Magical Crops
        addOreWeight("oreNetherGold", 3635); // Nether Ores
        addOreWeight("oreNetherIron", 5790); // Nether Ores
        addOreWeight("oreNetherLapis", 3250); // Nether Ores
        addOreWeight("oreNetherLead", 2790); // Nether Ores
        addOreWeight("oreNetherNickel", 1790); // Nether Ores
        addOreWeight("oreNetherPlatinum", 170); // Nether Ores
        addOreWeight("oreNetherRedstone", 5600); // Nether Ores
        addOreWeight("oreNetherSilver", 1550); // Nether Ores
        addOreWeight("oreNetherSteel", 1690); // Nether Ores
        addOreWeight("oreNetherTin", 3750); // Nether Ores
        addOreWeight("oreFyrite", 1000); // Netherrocks
        addOreWeight("oreAshstone", 1000); // Netherrocks
        addOreWeight("oreDragonstone", 175); // Netherrocks
        addOreWeight("oreArgonite", 1000); // Netherrocks
        addOreWeight("oreOnyx", 500); // SimpleOres 2
        addOreWeight("oreHaditeCoal", 500); // Hadite

        initialized = true;

        for(Ingredient ingredient:oreWeightsLazy.keySet()){
            addOreWeight(ingredient,oreWeightsLazy.get(ingredient));
        }
        oreWeightsLazy.clear();
        for(ItemStack stack:oreWeightModified.keySet()){
            for (Ingredient ingredient:oreWeights.keySet()){
                if(ingredient.test(stack)){
                    weight_count-=oreWeights.get(ingredient);
                    oreWeights.put(ingredient,oreWeightModified.get(stack));
                    weight_count+=oreWeightModified.get(stack);
                }
            }
        }
        oreWeightModified.clear();
    }

    public static final Map<Ingredient, Integer> oreWeights = new HashMap<>();
    public static final Map<Ingredient, Integer> oreWeightsLazy = new HashMap<>();
    public static final Map<ItemStack, Integer> oreWeightModified = new HashMap<>();
    public static int weight_count = 0;
    public static void addOreWeight(String ore, int weight) {
        NonNullList<ItemStack> ores = OreDictionary.getOres(ore);
        for(ItemStack stack : ores) {
            Item item = stack.getItem();

            if(!(item instanceof ItemBlock)) {
                continue;
            }
            addOreWeight(Ingredient.fromStacks(stack),weight);
            break;
        }
    }
    public static void addOreWeight(Ingredient stack, int weight){
        if(!initialized){
            addOreWeightLazy(stack,weight);
            return;
        }
        oreWeights.put(stack, weight);
        weight_count += weight;
    }

    public static void addOreWeightLazy(Ingredient stack, int weight){
        oreWeightsLazy.put(stack, weight);
    }

    public static void setWeight(ItemStack stack,int weight){
        if(!initialized){
            oreWeightModified.put(stack,weight);
        }else{
            RotationCraft.logger.warning("Warning:setWeight after initialization is not recommend.");
            for (Ingredient ingredient:oreWeights.keySet()){
                if(ingredient.test(stack)){
                    weight_count-=oreWeights.get(ingredient);
                    oreWeights.put(ingredient,weight);
                    weight_count+=weight;
                }
            }
        }
    }

    public static ItemStack next(Random random) {
        List<WeightedRandom.Item> values = new ArrayList<>();
        for(Ingredient s : oreWeights.keySet()) {
            values.add(new StackRandomItem(oreWeights.get(s), s));
        }
        StackRandomItem r = ((StackRandomItem) WeightedRandom.getRandomItem(random, values,weight_count));
        if(r.ingredient!=null && r.ingredient.getMatchingStacks().length>0){
            ItemStack ore = r.ingredient.getMatchingStacks()[0];
            if(!(ore.getItem() instanceof ItemBlock)) {
                return ItemStack.EMPTY;
            }
            return ore.copy();
        }
        return ItemStack.EMPTY;
    }

    public static class StackRandomItem extends WeightedRandom.Item {

        public final Ingredient ingredient;

        public StackRandomItem(int par1, Ingredient s) {
            super(par1);
            ingredient = s;
        }

    }
}
