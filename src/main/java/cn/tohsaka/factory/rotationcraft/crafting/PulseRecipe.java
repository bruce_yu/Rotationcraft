package cn.tohsaka.factory.rotationcraft.crafting;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;

public class PulseRecipe extends CraftingPattern{
    public Ingredient iinput;
    public PulseRecipe(String input, ItemStack output) {
        this(input,output,100,400);
    }
    public PulseRecipe(String input, ItemStack output,int fluid,int worktime) {
        super(new String[]{input}, output, null, new NBTTagCompound());
        iinput = Ingredient.fromStacks(parseOreDict(input));
        this.data.setInteger("fluid",fluid);
        this.data.setInteger("worktime",worktime);
    }
    public PulseRecipe(Ingredient input, ItemStack output,int fluid,int worktime) {
        super(null, output, null, new NBTTagCompound());
        iinput = input;
        this.data.setInteger("fluid",fluid);
        this.data.setInteger("worktime",worktime);
    }
}
