package cn.tohsaka.factory.rotationcraft.crafting;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;

public class MagnetizerRecipe extends CraftingPattern{
    public final Ingredient igd;
    public final double maxCharge;
    public final double uTvia2K;
    public MagnetizerRecipe(Ingredient input, double maxCharge,double uTvia2K) {
        super(null, ItemStack.EMPTY, null, null);
        igd = input;
        this.maxCharge = maxCharge;
        this.uTvia2K = uTvia2K;
    }

    @Override
    public ItemStack getOutput() {
        ItemStack stack = igd.getMatchingStacks()[0].copy();
        NBTTagCompound tag = stack.hasTagCompound()?stack.getTagCompound():new NBTTagCompound();
        tag.setDouble("magnet",maxCharge);
        stack.setTagCompound(tag);
        return stack.copy();
    }
}
