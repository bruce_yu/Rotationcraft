package cn.tohsaka.factory.rotationcraft.crafting;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;

public class ExtractorRecipe extends CraftingPattern{
    public Ingredient igd;
    public ItemStack output;
    public ExtractorRecipe(ItemStack output,ItemStack... input){
        igd = Ingredient.fromStacks(input);
        this.output = output;
    }
    public ExtractorRecipe(NonNullList<ItemStack> input, ItemStack output){
        igd = Ingredient.fromStacks(input.toArray(new ItemStack[]{}));
        this.output = output;
    }
    public boolean test(ItemStack stack){
        return igd.test(stack);
    }
}
