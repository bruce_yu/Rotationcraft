package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.crafting.WorktableRecipe;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@GameInitializer
public class WorktableRecipes implements INeedPostInit {
    public static boolean initialized = false;
    public static List<ItemStack> stacksToRemove = new ArrayList<>();
    public static void postinit(){
        String HSLA = ItemMaterial.getItemById(1).toRecipeStr(1);
        String HSLAGEAR0 = ItemMaterial.getItemById(78).toRecipeStr(1);
        String PLATE = ItemMaterial.getItemById(0).toRecipeStr(1);
        String NULL = "NULL";

        List<CraftingPattern> patternList = new ArrayList<>();

        patternList.add(new WorktableRecipe(new String[]{"rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","NULL","NULL","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(0,6)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material1,1,0","NULL","rc:material1,1,0","rc:material1,1,0","rc:material0,1,0","rc:material1,1,0","NULL","NULL","NULL"},ItemMaterial.getStackById(2,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material0,1,0","NULL","rc:material0,1,0","rc:material78,1,0","rc:material0,1,0","NULL","rc:material0,1,0","NULL"},ItemMaterial.getStackById(5,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material0,1,0","NULL","rc:material0,1,0","rc:material5,1,0","rc:material0,1,0","NULL","rc:material0,1,0","NULL"},ItemMaterial.getStackById(6,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material17,1,0","rc:material17,1,0","rc:material17,1,0","rc:material17,1,0","rc:material5,1,0","rc:material17,1,0","rc:material17,1,0","rc:material17,1,0","rc:material17,1,0"},ItemMaterial.getStackById(7,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","NULL","rc:material1,1,0","rc:material1,1,0","NULL","NULL","NULL"},ItemMaterial.getStackById(8,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","NULL","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0"},ItemMaterial.getStackById(9,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material1,1,0","NULL","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0"},ItemMaterial.getStackById(10,2)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material10,1,0","rc:material8,1,0","rc:material10,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0"},ItemMaterial.getStackById(11,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0"},ItemMaterial.getStackById(12,2)));
        patternList.add(new WorktableRecipe(new String[]{"minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0","rc:material1,1,0","minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0"},ItemMaterial.getStackById(13,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0"},ItemMaterial.getStackById(14,3)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","minecraft:gold_ingot,1,0","NULL","rc:material1,1,0","minecraft:flint_and_steel,1,0","rc:material1,1,0","NULL","rc:material1,1,0","NULL"},ItemMaterial.getStackById(15,1)));
        patternList.add(new WorktableRecipe(new String[]{"minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0","minecraft:gold_ingot,1,0","rc:material40,1,0","rc:material0,1,0","rc:material1,1,0","NULL","NULL","NULL"},ItemMaterial.getStackById(16,4)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material0,1,0","NULL","NULL","rc:material1,1,0","NULL","NULL","rc:material77,1,0","NULL"},ItemMaterial.getStackById(17,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material1,1,0","NULL","rc:material1,1,0","rc:material78,1,0","rc:material1,1,0","NULL","rc:material1,1,0","NULL"},ItemMaterial.getStackById(18,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","rc:material13,1,0","NULL","rc:material77,1,0","NULL","rc:material13,1,0","NULL","NULL"},ItemMaterial.getStackById(19,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material36,1,0","rc:material7,1,0","rc:material36,1,0","rc:material77,1,0","rc:material36,1,0","rc:material7,1,0","rc:material36,1,0","NULL"},ItemMaterial.getStackById(20,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","rc:material69,1,0","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0"},ItemMaterial.getStackById(21,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","NULL","rc:material34,1,0","NULL"},ItemMaterial.getStackById(22,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","NULL","rc:material1,1,0","NULL"},ItemMaterial.getStackById(23,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material34,1,0","NULL","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0"},ItemMaterial.getStackById(24,2)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material16,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(25,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material40,1,0","rc:material16,1,0","rc:material1,1,0","rc:material40,1,0","rc:material40,1,0","rc:material40,1,0","NULL","NULL","NULL"},ItemMaterial.getStackById(26,4)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material77,1,0","NULL","rc:material77,1,0","rc:material5,1,0","rc:material77,1,0","NULL","rc:material77,1,0","NULL"},ItemMaterial.getStackById(27,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","NULL","rc:material0,1,0","NULL","rc:material5,1,0","NULL","rc:material0,1,0","NULL","rc:material0,1,0"},ItemMaterial.getStackById(28,1)));
        patternList.add(new WorktableRecipe(new String[]{"minecraft:leather,1,0","minecraft:leather,1,0","minecraft:leather,1,0","minecraft:leather,1,0","rc:material78,1,0","minecraft:leather,1,0","minecraft:leather,1,0","minecraft:leather,1,0","minecraft:leather,1,0"},ItemMaterial.getStackById(29,6)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","NULL","NULL","rc:material1,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(30,8)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material77,1,0","rc:material0,1,0","rc:material18,1,0","rc:material0,1,0","rc:material77,1,0","rc:material0,1,0","rc:material0,1,0"},ItemMaterial.getStackById(31,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material77,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0"},ItemMaterial.getStackById(32,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material14,1,0","rc:material14,1,0","rc:material14,1,0","rc:material6,1,0","rc:material18,1,0","rc:material6,1,0","rc:material14,1,0","rc:material14,1,0","rc:material14,1,0"},ItemMaterial.getStackById(57,1)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material37,1,0","rc:material37,1,0","rc:material37,1,0","rc:material37,1,0","rc:material9,1,0","rc:material37,1,0","rc:material37,1,0","rc:material37,1,0","rc:material37,1,0"},ItemMaterial.getStackById(59,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","minecraft:diamond,1,0","NULL","minecraft:diamond,1,0","NULL","minecraft:diamond,1,0","NULL","NULL"},ItemMaterial.getStackById(61,3)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","minecraft:diamond,1,0","NULL","minecraft:diamond,1,0","minecraft:diamond,1,0","minecraft:diamond,1,0","NULL","minecraft:diamond,1,0","NULL"},ItemMaterial.getStackById(62,2)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material62,1,0","rc:material61,1,0","rc:material61,1,0","rc:material62,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(63,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material63,1,0","rc:material61,1,0","rc:material61,1,0","rc:material63,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(64,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material63,1,0","rc:material61,1,0","rc:material61,1,0","rc:material64,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(65,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material63,1,0","rc:material61,1,0","rc:material61,1,0","rc:material65,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(66,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material64,1,0","rc:material61,1,0","rc:material61,1,0","rc:material64,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(66,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","rc:material61,1,0","NULL","minecraft:diamond,1,0","NULL","rc:material61,1,0","NULL","NULL"},ItemMaterial.getStackById(68,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","minecraft:diamond,1,0","NULL","minecraft:diamond,1,0","rc:material68,1,0","minecraft:diamond,1,0","NULL","minecraft:diamond,1,0","NULL"},ItemMaterial.getStackById(67,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","rc:material34,1,0","NULL","rc:material34,1,0","NULL","rc:material34,1,0","NULL","NULL"},ItemMaterial.getStackById(69,3)));

        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material34,1,0","NULL","rc:material34,1,0","rc:material34,1,0","rc:material34,1,0","NULL","rc:material34,1,0","NULL"},ItemMaterial.getStackById(70,2)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material70,1,0","rc:material69,1,0","rc:material69,1,0","rc:material70,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(71,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material71,1,0","rc:material69,1,0","rc:material69,1,0","rc:material71,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(72,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material71,1,0","rc:material69,1,0","rc:material69,1,0","rc:material72,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(73,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material71,1,0","rc:material69,1,0","rc:material69,1,0","rc:material73,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(74,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material72,1,0","rc:material69,1,0","rc:material69,1,0","rc:material72,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(74,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","rc:material69,1,0","NULL","rc:material34,1,0","NULL","rc:material69,1,0","NULL","NULL"},ItemMaterial.getStackById(76,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material34,1,0","NULL","rc:material34,1,0","rc:material76,1,0","rc:material34,1,0","NULL","rc:material34,1,0","NULL"},ItemMaterial.getStackById(75,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","rc:material1,1,0","NULL","rc:material1,1,0","NULL","rc:material1,1,0","NULL","NULL"},ItemMaterial.getStackById(77,3)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material1,1,0","NULL","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0","NULL","rc:material1,1,0","NULL"},ItemMaterial.getStackById(78,2)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material78,1,0","rc:material77,1,0","rc:material77,1,0","rc:material78,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(79,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material79,1,0","rc:material77,1,0","rc:material77,1,0","rc:material79,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(80,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material79,1,0","rc:material77,1,0","rc:material77,1,0","rc:material80,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(81,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material79,1,0","rc:material77,1,0","rc:material77,1,0","rc:material81,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(82,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material80,1,0","rc:material77,1,0","rc:material77,1,0","rc:material80,1,0","NULL","NULL","NULL","NULL"},ItemMaterial.getStackById(82,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material1,1,0","NULL","rc:material1,1,0","rc:material84,1,0","rc:material1,1,0","NULL","rc:material1,1,0","NULL"},ItemMaterial.getStackById(83,1)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","rc:material77,1,0","NULL","rc:material1,1,0","NULL","rc:material77,1,0","NULL","NULL"},ItemMaterial.getStackById(84,1)));

        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material77,1,0","NULL","rc:material1,1,0","NULL","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:screwer",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material77,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:blockaxls",0,8,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material61,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:blockaxls",1,8,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material69,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:blockaxls",2,8,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material78,1,0","rc:material77,1,0","rc:material0,1,0","rc:material77,1,0","NULL"},GameRegistry.makeItemStack("rc:blockcorner",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material77,1,0","rc:material78,1,0","rc:material0,1,0","rc:material78,1,0","NULL"},GameRegistry.makeItemStack("rc:blockblender",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0","rc:material77,1,0","rc:material78,1,0","rc:material0,1,0","rc:material78,1,0","rc:material70,1,0"},GameRegistry.makeItemStack("rc:blockblender",1,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:blockblender,1,0","rc:material70,1,0","NULL","NULL","NULL","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:blockblender",1,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:blockaxls,1,1","rc:blockaxls,1,1","rc:blockaxls,1,1","rc:material26,1,0","rc:material16,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:blockmeter",0,8,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material79,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox2x",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material80,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox4x",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material81,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox8x",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material82,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox16x",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material63,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox2x",1,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material64,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox4x",1,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material65,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox8x",1,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material66,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox16x",1,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material71,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox2x",2,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material72,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox4x",2,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material73,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox8x",2,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material74,1,0","NULL","NULL","rc:material2,1,0","NULL","NULL","NULL","NULL"},GameRegistry.makeItemStack("rc:gearbox16x",2,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material1,1,0","rc:material10,1,0","rc:material8,1,0","rc:material1,1,0","rc:material27,1,0","rc:material10,1,0","rc:material1,1,0","rc:material1,1,0","rc:material1,1,0"},GameRegistry.makeItemStack("rc:blockferm",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","NULL","NULL","NULL","minecraft:observer,1,0","rc:material77,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0"},GameRegistry.makeItemStack("rc:blockdc",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material10,1,0","rc:material9,1,0","rc:material10,1,0","rc:material1,1,0","rc:material15,1,0","rc:material1,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0"},GameRegistry.makeItemStack("rc:blockcomb",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material58,1,0","rc:material59,1,0","rc:material58,1,0","rc:material39,1,0","rc:material15,1,0","rc:material39,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0"},GameRegistry.makeItemStack("rc:pfe",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material10,1,0","rc:material11,1,0","rc:material10,1,0","rc:material6,1,0","rc:material61,1,0","rc:material20,1,0","rc:material0,1,0","rc:material0,1,0","rc:material0,1,0"},GameRegistry.makeItemStack("rc:blockmicro",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material58,1,0","rc:material11,1,0","rc:material58,1,0","rc:material20,1,0","rc:material59,1,0","rc:material57,1,0","rc:material36,1,0","rc:material36,1,0","rc:material36,1,0"},GameRegistry.makeItemStack("rc:blockjet",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"NULL","rc:material39,1,0","NULL","rc:material19,1,0","rc:material16,1,0","rc:material19,1,0","rc:material14,1,0","rc:material14,1,0","rc:material14,1,0"},GameRegistry.makeItemStack("rc:blockdynamo",0,1,null)));


        patternList.add(new WorktableRecipe(new String[]{"minecraft:ender_pearl","minecraft:repeater","minecraft:gold_ingot","minecraft:comparator","minecraft:noteblock","minecraft:comparator","rc:material0","rc:material0","rc:material0"},GameRegistry.makeItemStack("rc:blockac",0,1,null)));
        patternList.add(new WorktableRecipe(new String[]{"rc:material1","rc:material25","rc:material1","rc:material16","rc:blocktank","rc:material16","rc:material1","minecraft:comparator","rc:material1"},GameRegistry.makeItemStack("rc:blockenginecontroller",0,1,null)));


        patternList.add(new WorktableRecipe(new String[]{
                "NULL","rc:material77","NULL",
                "rc:material77","rc:material79","rc:material77",
                "NULL","rc:material2","NULL"
        },getStack("rc:blockworm")));

        patternList.add(new WorktableRecipe(new String[]{
                "NULL","NULL","NULL",
                "rc:material0","minecraft:glass","rc:material0",
                "rc:material1","rc:material10","rc:material1"
        },getStack("rc:blockpump")));

        patternList.add(new WorktableRecipe(new String[]{
                "minecraft:obsidian","minecraft:obsidian","minecraft:obsidian",
                "rc:material5","rc:material10","rc:material6",
                "rc:material0","rc:material0","rc:material0"
        },getStack("rc:blockpulse")));

        patternList.add(new WorktableRecipe(new String[]{
                "NULL","NULL","NULL",
                "rc:material28","rc:material84","rc:material28",
                "rc:material0","rc:material0","rc:material0"
        },getStack("rc:blockgrinder")));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material18","rc:material27","rc:material18",
                "rc:material10","minecraft:chest","rc:material10",
                "rc:material0","rc:material0","rc:material0"
        },getStack("rc:blockfract")));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","NULL","NULL",
                "rc:material0","rc:material77","rc:material77",
                "rc:material0","rc:material1","rc:material1"
        },getStack("rc:blockfriction")));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material1","minecraft:glass","rc:material1",
                "rc:material1","rc:material27","rc:material1",
                "rc:material1","rc:material77","rc:material1"
        },getStack("rc:blockfurnace")));

        patternList.add(new WorktableRecipe(new String[]{
                "NULL","NULL","NULL",
                "rc:material74","rc:material76","rc:material74",
                "NULL","rc:material2","NULL"
        },getStack("rc:highgear")));

        patternList.add(new WorktableRecipe(new String[]{
                "minecraft:planks","minecraft:hopper","minecraft:netherrack",
                "rc:material23","rc:material28","rc:material5",
                "rc:material10","minecraft:chest","rc:material10"
        },getStack("rc:blockextractor")));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material1","rc:material1","rc:material1",
                "rc:material69","rc:material29","rc:material69",
                "rc:material0","rc:material0","rc:material0"
        },getStack("rc:cvt")));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material1","rc:material36","minecraft:obsidian",
                "rc:material61","rc:material36","minecraft:obsidian",
                "rc:material0","rc:material0","rc:material0"
        },getStack("rc:blockbeminer")));


        patternList.add(new WorktableRecipe(new String[]{
                "NULL","minecraft:observer","NULL",
                "rc:material83","rc:material32","rc:material31",
                "NULL","rc:material2","NULL"
        },GameRegistry.makeItemStack("rc:blockcoil",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "NULL","minecraft:observer","NULL",
                "rc:material75","rc:material21","rc:material31",
                "NULL","rc:material2","NULL"
        },GameRegistry.makeItemStack("rc:blockcoil",1,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material14","rc:material14","rc:material14",
                "rc:material16","minecraft:chest","rc:material16",
                "rc:material36","rc:material22","rc:material36"
        },GameRegistry.makeItemStack("rc:blockvoidminer",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","rc:material18","rc:material0",
                "rc:material28","rc:material28","rc:material28",
                "rc:material0","rc:material0","rc:material0"
        },GameRegistry.makeItemStack("rc:blockwoodcutter",0,1,null)));


        patternList.add(new WorktableRecipe(new String[]{
                "NULL","rc:material0","NULL",
                "rc:material12","minecraft:bucket","rc:material5",
                "rc:material0","rc:material0","rc:material0"
        },GameRegistry.makeItemStack("rc:steamengine",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material37","NULL","rc:material37",
                "rc:material37","rc:material13","rc:material37",
                "rc:material37","rc:material12","rc:material37"
        },GameRegistry.makeItemStack("rc:blockmagnetizer",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","rc:material5","rc:material0",
                "minecraft:ender_eye","minecraft:chest","minecraft:ender_eye",
                "rc:material0","rc:material12","rc:material0"
        },GameRegistry.makeItemStack("rc:blockteleporter",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","rc:material5","rc:material0",
                "rc:material0","rc:material77","rc:material0",
                "rc:material0","rc:material80","rc:material0"
        },GameRegistry.makeItemStack("rc:blockfan",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","minecraft:ender_pearl","rc:material0",
                "rc:material77","minecraft:chest","rc:material77",
                "rc:material0","minecraft:ender_pearl","rc:material0"
        },GameRegistry.makeItemStack("rc:blockvacuum",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","rc:blockpipe","rc:material0",
                "rc:material0","rc:material5","rc:material0"
        },GameRegistry.makeItemStack("rc:blocksprinkler",0,1,null)));




        patternList.add(new WorktableRecipe(new String[]{
                "rc:material39","rc:material26","rc:material39",
                "rc:material16","rc:blockmb,1,1","rc:material16",
                "rc:material37","rc:material37","rc:material37"
        },GameRegistry.makeItemStack("rc:blockmb",2,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","rc:material0","rc:material0",
                "rc:material12","rc:blockmb","rc:material12",
                "rc:material0","rc:material0","rc:material0"
        },GameRegistry.makeItemStack("rc:blockmb",1,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material14","rc:material14","rc:material14",
                "rc:material12","rc:blocktank","rc:material12",
                "rc:material14","rc:material14","rc:material14"
        },GameRegistry.makeItemStack("rc:blockmb",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material0","rc:material0","rc:material0",
                "rc:material0","rc:blocktank","rc:material0",
                "rc:material0","rc:material0","rc:material0"
        },GameRegistry.makeItemStack("rc:blockboiler",0,1,null)));


        patternList.add(new WorktableRecipe(new String[]{
                "rc:material86","rc:material86","rc:material86",
                "rc:material86","rc:material20","rc:material86",
                "rc:material86","rc:material86","rc:material86"
        },ItemMaterial.getStackById(87)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material17","rc:material17","rc:material17",
                "rc:material17","rc:material87","rc:material17",
                "rc:material17","rc:material17","rc:material17"
        },GameRegistry.makeItemStack("rc:blockminiturbine",0,1,null)));


        patternList.add(new WorktableRecipe(new String[]{
                "rc:material55","rc:material16","rc:material55",
                "rc:material58","rc:material25","rc:material58",
                "rc:material55","rc:material16","rc:material55"
        },GameRegistry.makeItemStack("rc:blocktcu",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material58","rc:material39","rc:material58",
                "rc:material39","rc:material57","rc:material39",
                "rc:material0","rc:material0","rc:material0"
        },GameRegistry.makeItemStack("rc:blockexchanger",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "NULL","NULL","NULL",
                "rc:material77","rc:material19","rc:material77",
                "NULL","rc:material2","NULL"
        },GameRegistry.makeItemStack("rc:blockmagnetic",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "rc:material1","rc:material1","rc:material1",
                "rc:material68","rc:material20","minecraft:glass",
                "rc:material1","rc:material1","rc:material1"
        },GameRegistry.makeItemStack("rc:blocksteamturbine",0,1,null)));

        patternList.add(new WorktableRecipe(new String[]{
                "minecraft:glass_pane","rc:material14","minecraft:glass_pane",
                "rc:material14","rc:blockfriction","rc:material14",
                "minecraft:glass_pane","rc:material14","minecraft:glass_pane"
        },GameRegistry.makeItemStack("rc:blocksteamboiler",0,1,null)));




        //RecipeHelper.addShapedRecipe(new ItemStack(RotationCraft.INSTANCE.blocks.get(NAME)),"A A","ABA","ACA",'A', ItemMaterial.getStackById(37),'B', ItemMaterial.getStackById(13),'C', ItemMaterial.getStackById(2));





        initialized = true;

        List<CraftingPattern> c =  patternList.stream().filter( craftingPattern -> {
            ItemStack output = craftingPattern.getOutput();
            for(ItemStack stack:stacksToRemove){
                return output.getItem().equals(stack.getItem()) && output.getMetadata() == stack.getMetadata();
            }
            return false;
        }).collect(Collectors.toList());

        for(CraftingPattern cc : c){
            patternList.remove(cc);
        }
        stacksToRemove.clear();
        Recipes.INSTANCE.addPattern(TileWorktable.class,patternList);
    }
    private static ItemStack getStack(String str){
        return GameRegistry.makeItemStack(str,0,1,null);
    }
}
