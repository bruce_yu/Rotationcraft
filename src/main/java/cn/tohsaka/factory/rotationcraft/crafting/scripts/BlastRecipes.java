package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.crafting.BlastRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemArmorBedrock;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.items.tools.*;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBlast;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@GameInitializer
public class BlastRecipes  implements INeedPostInit {
    public static boolean initialized = false;
    public static List<ItemStack> stacksToRemove = new ArrayList<>();

    public static void postinit(){
        List<CraftingPattern> patternList = new ArrayList<>();
        patternList.add(new BlastRecipe(new String[]{"NULL","ore:logWood","minecraft:sand"},"minecraft:coal",ItemMaterial.getStackById(38),0.5F,750));

        patternList.add(new BlastRecipe(new String[]{"minecraft:gunpowder","minecraft:coal","minecraft:sand"},"ore:ingotIron",ItemMaterial.getStackById(1),0,750));
        patternList.add(new BlastRecipe(new String[]{"minecraft:gunpowder","rc:material38","minecraft:sand"},"ore:ingotIron",ItemMaterial.getStackById(1),0.5F,750));

        patternList.add(new BlastRecipe(new String[]{"minecraft:gunpowder","minecraft:tnt","minecraft:sand"},"minecraft:stone,1,3",ItemMaterial.getStackById(40),0,1200));

        patternList.add(new BlastRecipe(new String[]{"rc:material38"},"rc:material1",ItemMaterial.getStackById(39),0,750));



        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "NULL","NULL","rc:material1",
                "NULL","rc:material1","NULL",
                "rc:material1","NULL","NULL"

        },ItemMaterial.getStackById(69,8),0,1350));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "NULL","rc:material1","NULL",
                "rc:material1","rc:material1","rc:material1",
                "NULL","rc:material1","NULL",

        },ItemMaterial.getStackById(70,8),0,1350));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "NULL","NULL","rc:material77",
                "NULL","rc:material1","NULL",
                "rc:material77","NULL","NULL"

        },ItemMaterial.getStackById(76,2),0,1350));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material1","NULL","NULL",
                "NULL","NULL","NULL",
                "NULL","NULL","NULL"

        },ItemMaterial.getStackById(34,1),0,1350));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,8"},new String[]{
                "rc:material32","rc:material32","rc:material32",
                "rc:material32","rc:material32","rc:material32",
                "rc:material32","rc:material32","rc:material32"

        },ItemMaterial.getStackById(21,1),0,1350));


        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material34","rc:material34","rc:material34",
                "NULL","rc:material39","NULL",
                "NULL","rc:material39","NULL"

        }, ItemBedrockPickaxe.getEnchantStack(),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material34","rc:material34","NULL",
                "rc:material34","rc:material39","NULL",
                "NULL","rc:material39","NULL"

        }, ItemBedrockAxe.getEnchantStack(),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "NULL","rc:material34","NULL",
                "NULL","rc:material39","NULL",
                "NULL","rc:material39","NULL"

        }, ItemBedrockShovel.getEnchantStack(),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material34","rc:material34","NULL",
                "NULL","rc:material39","NULL",
                "NULL","rc:material39","NULL"

        }, ItemBedrockHoe.getEnchantStack(),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "NULL","rc:material34","NULL",
                "rc:material34","NULL","NULL",
                "NULL","NULL","NULL"

        }, ItemBedrockShears.getEnchantStack(),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "NULL","rc:material34","NULL",
                "NULL","rc:material34","NULL",
                "NULL","rc:material39","NULL"

        }, ItemBedrockSword.getEnchantStack(),0,1500));


        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material34","rc:material34","rc:material34",
                "rc:material34","NULL","rc:material34",
                "NULL","NULL","NULL"

        }, ItemArmorBedrock.getEnchantStack(EntityEquipmentSlot.HEAD),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material34","NULL","rc:material34",
                "rc:material34","rc:material34","rc:material34",
                "rc:material34","rc:material34","rc:material34"

        }, ItemArmorBedrock.getEnchantStack(EntityEquipmentSlot.CHEST),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material34","rc:material34","rc:material34",
                "rc:material34","NULL","rc:material34",
                "rc:material34","NULL","rc:material34"


        }, ItemArmorBedrock.getEnchantStack(EntityEquipmentSlot.LEGS),0,1500));

        patternList.add(new BlastRecipe(new String[]{"rc:material45,4"},new String[]{
                "rc:material34","NULL","rc:material34",
                "rc:material34","NULL","rc:material34",
                "NULL","NULL","NULL"

        }, ItemArmorBedrock.getEnchantStack(EntityEquipmentSlot.FEET),0,1500));



        initialized = true;

        List<CraftingPattern> c =  patternList.stream().filter( craftingPattern -> {
            ItemStack output = craftingPattern.getOutput();
            for(ItemStack stack:stacksToRemove){
                return output.getItem().equals(stack.getItem()) && output.getMetadata() == stack.getMetadata();
            }
            return false;
        }).collect(Collectors.toList());

        for(CraftingPattern cc : c){
            patternList.remove(cc);
        }
        stacksToRemove.clear();



        Recipes.INSTANCE.addPattern(TileBlast.class,patternList);
    }
}
