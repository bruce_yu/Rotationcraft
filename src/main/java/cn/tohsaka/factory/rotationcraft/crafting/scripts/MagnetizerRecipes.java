package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.MagnetizerRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.items.ItemUpgrade;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileMagnetizer;
import net.minecraft.item.crafting.Ingredient;

import java.util.ArrayList;
import java.util.List;

@GameInitializer
public class MagnetizerRecipes implements INeedPostInit {
    public static void postinit(){
        List<CraftingPattern> patternList = new ArrayList<>();
        patternList.add(new MagnetizerRecipe(Ingredient.fromStacks(ItemMaterial.getStackById(84)),4,0.0001));
        patternList.add(new MagnetizerRecipe(Ingredient.fromStacks(ItemMaterial.getStackById(68)),64,0.0002));
        patternList.add(new MagnetizerRecipe(Ingredient.fromStacks(ItemMaterial.getStackById(76)),256,0.0003));
        patternList.add(new MagnetizerRecipe(Ingredient.fromStacks(ItemUpgrade.getByMeta(2)),320,0.001));

        Recipes.INSTANCE.addPattern(TileMagnetizer.class,patternList);
    }
}
