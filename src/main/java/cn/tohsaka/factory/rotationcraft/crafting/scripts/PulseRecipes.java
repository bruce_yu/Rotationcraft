package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.PulseRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TilePulse;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@GameInitializer
public class PulseRecipes implements INeedPostInit {
    public static boolean initialized = false;
    public static List<ItemStack> stacksToRemove = new ArrayList<>();
    public static void postinit(){
        List<CraftingPattern> patternList = new ArrayList<>();

        patternList.add(new PulseRecipe("ore:ingotIron", ItemMaterial.getStackById(1)));
        patternList.add(new PulseRecipe("rc:material49", ItemMaterial.getStackById(37)));
        patternList.add(new PulseRecipe("minecraft:coal", ItemMaterial.getStackById(33)));
        patternList.add(new PulseRecipe("rc:material10,8", ItemMaterial.getStackById(58),200,400));
        patternList.add(new PulseRecipe("rc:material54", ItemMaterial.getStackById(36),200,800));
        patternList.add(new PulseRecipe("rc:material36,8", ItemMaterial.getStackById(55),1000,1600));
        patternList.add(new PulseRecipe("rc:material85", ItemMaterial.getStackById(86),4000,6400));
        patternList.add(new PulseRecipe("rc:material3,8", ItemMaterial.getStackById(1),100,200));
        patternList.add(new PulseRecipe("rc:material4,8", new ItemStack(Items.IRON_INGOT),100,200));

        initialized = true;

        List<CraftingPattern> c =  patternList.stream().filter( craftingPattern -> {
            ItemStack output = craftingPattern.getOutput();
            for(ItemStack stack:stacksToRemove){
                return output.getItem().equals(stack.getItem()) && output.getMetadata() == stack.getMetadata();
            }
            return false;
        }).collect(Collectors.toList());

        for(CraftingPattern cc : c){
            patternList.remove(cc);
        }
        stacksToRemove.clear();

        Recipes.INSTANCE.addPattern(TilePulse.class,patternList);
    }
}
