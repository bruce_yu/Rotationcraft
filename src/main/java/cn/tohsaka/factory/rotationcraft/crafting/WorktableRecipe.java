package cn.tohsaka.factory.rotationcraft.crafting;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;

import java.util.Arrays;

public class WorktableRecipe extends CraftingPattern{
    public NonNullList<Ingredient> ingredients = NonNullList.create();
    public WorktableRecipe(String[] input, ItemStack output, ItemStack[] omni, NBTTagCompound data,boolean shapedless) {
        super(input, output, omni, data);
        for(String s:input){
            ingredients.add(Ingredient.fromStacks(CraftingPattern.parseOreDict(s)[0]));
        }
    }
    public WorktableRecipe(Ingredient[] input, ItemStack output, ItemStack[] omni, NBTTagCompound data,boolean shapedless) {
        super(null, output, omni, data);
        NonNullList<Ingredient> ingredients = NonNullList.create();
        if(input == null || input.length==0){
            RotationCraft.logger.warning("Warning:incorrect input for worktable recipe:"+output.toString());
            return;
        }
        ingredients.addAll(Arrays.asList(input));
        while (ingredients.size()<9){
            ingredients.add(Ingredient.EMPTY);
        }
    }

    public WorktableRecipe(String[] input,ItemStack output){
        this(input,output,new ItemStack[]{},new NBTTagCompound(),false);
    }
    public WorktableRecipe(String[] input,ItemStack output,boolean shapedless){
        this(input,output,new ItemStack[]{},new NBTTagCompound(),shapedless);
    }
    public WorktableRecipe(String[] input,ItemStack output,NBTTagCompound tagCompound,boolean shapedless){
        this(input,output,new ItemStack[]{},tagCompound,shapedless);
    }

    public boolean test(IInventory inv){
        for(int i=0;i<9;i++){
            if(!ingredients.get(i).test(inv.getStackInSlot(i))){
                return false;
            }
        }
        return true;
    }
}
