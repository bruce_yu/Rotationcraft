package cn.tohsaka.factory.rotationcraft.prefab.gui.elements;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import net.minecraft.util.ResourceLocation;

public abstract class ElementButtonBase extends ElementBase {
	public static final String PATH_GFX = RotationCraft.MOD_ID+":textures/";
	public static final String PATH_GUI = PATH_GFX + "gui/";
	public static final String PATH_ELEMENTS = PATH_GUI + "elements/";

	public static final ResourceLocation HOVER = new ResourceLocation(PATH_ELEMENTS + "button_hover.png");
	public static final ResourceLocation ENABLED = new ResourceLocation(PATH_ELEMENTS + "button_enabled.png");
	public static final ResourceLocation DISABLED = new ResourceLocation(PATH_ELEMENTS + "button_disabled.png");

	public ElementButtonBase(GuiMachinebase containerScreen, int posX, int posY, int sizeX, int sizeY) {

		super(containerScreen, posX, posY, sizeX, sizeY);
	}

	@Override
	public boolean onMousePressed(int mouseX, int mouseY, int mouseButton) {

		playSound(mouseButton);
		switch (mouseButton) {
			case 0:
				onClick();
				break;
			case 1:
				onRightClick();
				break;
			case 2:
				onMiddleClick();
				break;
		}
		return true;
	}

	protected void playSound(int button) {

		if (button == 0) {
			GuiMachinebase.playClickSound(1.0F);
		}
	}

	public void onClick() {

	}

	public void onRightClick() {

	}

	public void onMiddleClick() {

	}
}