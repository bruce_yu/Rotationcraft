package cn.tohsaka.factory.rotationcraft.prefab.tile;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerSource;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IControlableEngine;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.utils.*;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

public class TileEngine extends TileInvMachine implements ITickable, IModeProvider, IModelProvider, IMeterable {
    protected RotaryPowerSource powerSource;
    public TileEngine(RotaryPower maximum){
        powerSource = new RotaryPowerSource(maximum,this){



            @Nullable
            @Override
            public World getWorld() {
                return TileEngine.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return null;
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1);
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return null;
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing.getOpposite());
            }
        };
    }
    @Override
    public void update() {
        if(world.isRemote){
            angle = AxlsUtils.calcAngle(angle,powerSource.getPower().getSpeed());
        }else{
            tick();
        }
    }
    public void tick(){

    }


    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        drawables.add(new RenderUtils.RenderItem(0xFF0000, FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),1F));
        //drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),1F));
        super.getOutBoxList(drawables);
    }

    @Override
    public void setFacing(EnumFacing facing) {
        super.setFacing(facing);
    }

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        return true;
    }
    private RotaryModelBase model;
    private String modeltex;

    @SideOnly(Side.CLIENT)
    public void renderTileEntityAt(double x, double y, double z){
        if(model==null){
            model = getModel();
            return;
        }
        if(modeltex==null || modeltex.equals("")){
            modeltex = getTexName();
        }


        {
            RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/"+modeltex+".png"));

            GlStateManager.pushMatrix();
            GlStateManager.enableRescaleNormal();
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.translate(x, y, z);
            GlStateManager.scale(1.0F, -1.0F, -1.0F);
            GlStateManager.translate(0.5F, 0.5F, 0.5F);
            GlStateManager.translate(0F,-2F,-1F);
            RenderUtils.rotate(getFacing(), 270, 90, 180, 0);
        }
        if(this.getBlockType().getLightOpacity(world.getBlockState(pos),world,pos)>0){
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        }
        beginRenderingModelInWorld();

        model.renderAll(this,null,angle,0F);

        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }

    public void beginRenderingModelInWorld(){

    }

    @Override
    public void getWailaBody(ItemStack itemStack,List<String> tooltip) {

    }

    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return null;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return null;
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side==null){
                return true;
            }
            // || facing == this.facing.rotateY().rotateY() || facing == null
            if(side==FacingTool.revert(this.facing)){
                return true;
            }
        }
        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side==null || side==FacingTool.revert(this.facing)){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerSource);
            }
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append(powerSource.getPower()+"\n");
        return sb.toString();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void onMeter(){
        addIO();
    }



    public boolean isControlable(){
        return this instanceof IControlableEngine;
    }
}
