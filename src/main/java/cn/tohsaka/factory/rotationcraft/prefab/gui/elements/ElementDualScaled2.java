package cn.tohsaka.factory.rotationcraft.prefab.gui.elements;

import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;

public class ElementDualScaled2 extends ElementBase {

	public int quantity;
	public int mode;
	public boolean background = true;
	private int texU;
	private int texV;
	public ElementDualScaled2(GuiMachinebase gui, int posX, int posY,int u,int v) {
		super(gui, posX, posY);
		texU = u;
		texV = v;
	}

	public ElementDualScaled2 setBackground(boolean background) {
		this.background = background;
		return this;
	}

	public ElementDualScaled2 setMode(int mode) {

		this.mode = mode;
		return this;
	}

	public ElementDualScaled2 setQuantity(int quantity) {

		this.quantity = quantity;
		return this;
	}

	@Override
	public void drawBackground(int mouseX, int mouseY, float gameTicks) {

		RenderHelper2.bindTexture(texture);
		switch (mode) {
			case 0://top -> bottom
				drawTexturedModalRect(posX,posY,texU,texV,sizeX,quantity);
				return;
			case 1://left -> right
				drawTexturedModalRect(posX,posY,texU,texV,quantity,sizeY);
				return;
			case 2:// vertical bottom -> top
				drawTexturedModalRect(posX, posY+(sizeY-quantity), texU, texV + (sizeY - quantity), sizeX, quantity);
				return;
		}
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {

	}
}