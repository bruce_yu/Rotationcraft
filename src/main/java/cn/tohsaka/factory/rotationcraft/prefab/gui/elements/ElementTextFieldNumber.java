package cn.tohsaka.factory.rotationcraft.prefab.gui.elements;

import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;

public class ElementTextFieldNumber extends ElementTextFieldLimited{
    public ElementTextFieldNumber(GuiMachinebase gui, int posX, int posY, int width, int height, int limit,String dafault) {
        super(gui, posX, posY, width, height, (short) limit);
        setText(dafault);
    }
    @Override
    public boolean isAllowedCharacter(char charTyped) {
        return charTyped>47 && charTyped<58;
    }
    int minValue=0;
    int maxValue=Integer.MAX_VALUE;
    public ElementTextFieldNumber setValueRange(int min,int max){
        minValue = min;
        maxValue = max;
        return this;
    }
    @Override
    protected void onCharacterEntered(boolean success) {
        if(success){
            if(this.getText().length()==0){
                return;
            }
            try {
                int i = Integer.parseInt(this.getText());
                if(i<=minValue){
                    setText2(String.valueOf(minValue));
                }
                if(i>maxValue){
                    setText2(String.valueOf(maxValue));
                }
            }catch (Exception ex){
                setText2(String.valueOf(minValue));
            }
        }
    }

    @Override
    public ElementTextField setFocused(boolean focused) {
        if(focused == false){
            if(this.getText().length()==0){
                setText2(String.valueOf(minValue));
            }
            onCharacterEntered(true);
        }
        return super.setFocused(focused);
    }
}
