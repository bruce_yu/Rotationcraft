package cn.tohsaka.factory.rotationcraft.prefab.gui.elements;

import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;

import java.util.List;

public class ElementButton extends ElementButtonBase {

	private int sheetX;
	private int sheetY;
	private int hoverX;
	private int hoverY;
	private int disabledX = 0;
	private int disabledY = 0;
	private boolean tooltipLocalized = false;
	private boolean managedClicks;
	private String tooltip;
	private String text;
	private int id;
	public ElementButton(GuiMachinebase gui, int posX, int posY, int sizeX, int sizeY, int sheetX, int sheetY, int hoverX, int hoverY, String texture) {

		super(gui, posX, posY, sizeX, sizeY);
		setGuiManagedClicks(false);
		setTexture(texture, texW, texH);
		this.sheetX = sheetX;
		this.sheetY = sheetY;
		this.hoverX = hoverX;
		this.hoverY = hoverY;
	}

	public ElementButton(GuiMachinebase gui, int posX, int posY, int sizeX, int sizeY, int sheetX, int sheetY, int hoverX, int hoverY, int disabledX, int disabledY, String texture) {

		this(gui, posX, posY, sizeX, sizeY, sheetX, sheetY, hoverX, hoverY, texture);
		this.disabledX = disabledX;
		this.disabledY = disabledY;
	}

	public ElementButton(GuiMachinebase gui, int posX, int posY, String name, int sheetX, int sheetY, int hoverX, int hoverY, int sizeX, int sizeY, String texture) {

		super(gui, posX, posY, sizeX, sizeY);
		setGuiManagedClicks(true);
		setName(name);
		setTexture(texture, texW, texH);
		this.sheetX = sheetX;
		this.sheetY = sheetY;
		this.hoverX = hoverX;
		this.hoverY = hoverY;
	}

	public ElementButton(GuiMachinebase gui, int posX, int posY, String name, int sheetX, int sheetY, int hoverX, int hoverY, int disabledX, int disabledY, int sizeX, int sizeY, String texture) {

		this(gui, posX, posY, name, sheetX, sheetY, hoverX, hoverY, sizeX, sizeY, texture);
		this.disabledX = disabledX;
		this.disabledY = disabledY;
	}

	public ElementButton setGuiManagedClicks(boolean managed) {

		this.managedClicks = managed;
		return this;
	}

	public ElementButton clearToolTip() {

		this.tooltip = null;
		return this;
	}

	public ElementButton setToolTip(String tooltip) {

		this.tooltip = tooltip;
		return this;
	}

	public ElementButton setToolTipLocalized(boolean localized) {

		this.tooltipLocalized = localized;
		return this;
	}

	public ElementButton setToolTipLocalized(String tooltip) {

		return setToolTip(tooltip).setToolTipLocalized(true);
	}
	public ElementButton setText(String t){
		text=t;
		return this;
	}
	int offset = -1;
	public ElementButton setText(String t,int o){
		text=t;
		offset = o;
		return this;
	}

	@Override
	public void drawBackground(int mouseX, int mouseY, float gameTicks) {

		GlStateManager.color(1, 1, 1, 1);
		RenderHelper2.bindTexture(texture);
		if (isEnabled()) {
			if (intersectsWith(mouseX, mouseY)) {
				drawTexturedModalRect(posX, posY, hoverX, hoverY, sizeX, sizeY);
			} else {
				drawTexturedModalRect(posX, posY, sheetX, sheetY, sizeX, sizeY);
			}
		} else {
			drawTexturedModalRect(posX, posY, disabledX, disabledY, sizeX, sizeY);
		}
		if(text!=null){
			FontRenderer fontRenderer = getFontRenderer();
			boolean u = fontRenderer.getUnicodeFlag();
			fontRenderer.setUnicodeFlag(false);
			fontRenderer.drawString(text,offset==-1?posX+6:offset,posY+5,0xFFFFFF,false);
			fontRenderer.setUnicodeFlag(u);
		}
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {

	}

	@Override
	public void addTooltip(List<String> list) {

		if (tooltip != null) {
			if (tooltipLocalized) {
				list.add(tooltip);
			} else {
				list.add(StringHelper.localize(tooltip));
			}
		}
	}

	@Override
	public void onClick() {

	}

	@Override
	public boolean onMousePressed(int x, int y, int mouseButton) {

		if (!managedClicks) {
			return super.onMousePressed(x, y, mouseButton);
		}
		if (isEnabled()) {
			playSound(mouseButton);
			if(id>-1){
				gui.handleIDButtonClick(id,mouseButton);
			}else{
				gui.handleElementButtonClick(getName(), mouseButton);
			}
			return true;
		}
		return false;
	}

	public void setSheetX(int pos) {

		sheetX = pos;
	}

	public void setSheetY(int pos) {

		sheetY = pos;
	}

	public void setHoverX(int pos) {

		hoverX = pos;
	}

	public void setHoverY(int pos) {

		hoverY = pos;
	}

	public ElementButton setDisabledX(int pos) {

		disabledX = pos;
		return this;
	}

	public ElementButton setDisabledY(int pos) {

		disabledY = pos;
		return this;
	}

	public void setActive() {

		setEnabled(true);
	}

	public void setDisabled() {

		setEnabled(false);
	}
	public ElementButton setid(int d){
		id = d;
		return this;
	}
	public int getid(){
		return id;
	}
}