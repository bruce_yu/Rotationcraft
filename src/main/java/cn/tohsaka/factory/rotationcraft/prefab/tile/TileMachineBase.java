package cn.tohsaka.factory.rotationcraft.prefab.tile;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.api.IGuiPacketHandler;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IFacing;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IUpgradable;
import cn.tohsaka.factory.rotationcraft.intergration.waila.IWailaSupport;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.proxy.GuiHandler;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by covers1624 on 20/05/2017.
 */
public abstract class TileMachineBase extends TileBase implements IFacing, RenderUtils.IOutBoxDrawable, IWailaSupport, IGuiPacketHandler {

    //The machine is currently doing stuff.
    protected boolean isActive;
    //The machine was doing stuff and a client update is pending.
    private boolean wasActive;
    //Tracks how long the machine has been off for, and then triggers an update after x time.

    private boolean sendUpdatePacket;
    public boolean fullContainerSync;
    public float angle;
    protected EnumFacing facing = EnumFacing.NORTH;

    public ArrayList<ItemStack> upgrades = new ArrayList<ItemStack>();

    /**
     * Does checks to see if a delay has passed since the machine was turned off for triggering client updates.
     */
    private void updateCheck() {
        if (wasActive) {
            wasActive = false;
            sendUpdatePacket = true;
        }
        if (sendUpdatePacket) {
            sendUpdatePacket = false;
            NetworkDispatcher.dispatchMachineUpdate(this);
        }
    }

    @Override
    public void markDirty() {
        super.markDirty();
        if(!world.isRemote){
            NetworkDispatcher.dispatchMachineUpdate(this);
        }
    }

    /**
     * Called on the server to write data to the update packet.
     *
     * @param packet The packet to write data to.
     */
    public void writeUpdateData(PacketCustom packet) {
        packet.writeBoolean(isActive);

    }

    /**
     * Called on the client to read the update data from the server.
     *
     * @param packet The packet to read data from.
     */
    public void readUpdatePacket(PacketCustom packet) {
        isActive = packet.readBoolean();
        IBlockState state = world.getBlockState(pos);
        world.notifyBlockUpdate(pos, state, state, 3);
    }

    public void writeGuiData(PacketCustom packet, boolean isFullSync){

    }

    public void readGuiData(PacketCustom packet, boolean isFullSync){

    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        if(world!=null){
            super.readFromNBT(compound);
            if(compound.hasKey("facing")){
                facing = EnumFacing.VALUES[compound.getByte("facing")];

            }
            if(compound.hasKey("metadata")){
                setMetadata(compound.getInteger("metadata"));
            }
            if(compound.hasKey("placer")){
                placer = compound.getString("placer");
            }
            if(this instanceof IUpgradable){
                ((IUpgradable)this).readUpgradeFromNBT(compound);
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setByte("facing", (byte) facing.ordinal());
        compound.setInteger("metadata",getMetadata());
        compound.setString("placer",placer);
        if(this instanceof IUpgradable){
            ((IUpgradable)this).writeUpgradeToNBT(compound);
        }
        return super.writeToNBT(compound);
    }


    public void readFromNBTNative(NBTTagCompound compound) {
        super.readFromNBT(compound);
    }

    public NBTTagCompound writeToNBTNative(NBTTagCompound compound) {
        return super.writeToNBT(compound);
    }


    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setBoolean("active", isActive);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        isActive = tag.getBoolean("active");
    }


    public void handleDataUpload(NBTTagCompound tag){

    }

    public String placer = "nobody";

    public void setPlacer(EntityPlayer p){
        placer = p.getGameProfile().getName();
        markDirty();
    }


    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        handleUpdateTag(pkt.getNbtCompound());
    }

    public NBTTagCompound getModePacket(){
        return new NBTTagCompound();
    }
    public void handleModePacket(NBTTagCompound compound){

    }

    @Override
    public EnumFacing getFacing() {
        return facing;
    }



    public boolean isActive() {
        return isActive;
    }

    @Override
    protected void setWorldCreate(World worldIn) {
        this.world = worldIn;
    }

    int metadata = 0;
    public int getMetadata(){
        return metadata;
    }
    public void setMetadata(int meta){
        metadata = meta;
    }





    protected long renderboxtime = 0L;

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        long time = world.getTotalWorldTime();
        if((time - renderboxtime)>getOutBoxLivingTicks()){
            RenderUtils.removeRender(this);
            return;
        }
    }

    public int getOutBoxLivingTicks(){
        return 50;
    }

    @Override
    public void setFacing(EnumFacing facing) {
        if(world.isRemote){
            removeIO();
        }
        this.facing = facing;
        IBlockState ib = this.world.getBlockState(this.pos);
        ib = ib.withProperty(BlockBase.FACING,facing);
        this.world.setBlockState(this.pos,ib);
        markDirty();

        if(world.isRemote){
            addIO();
        }
    }
    @SideOnly(Side.CLIENT)
    public void addIO(){
        if(!RenderUtils.containsRender(this)){
            renderboxtime = world.getTotalWorldTime();
            RenderUtils.addRender(this);
        }
    }

    @SideOnly(Side.CLIENT)
    public void removeIO(){
        if(this instanceof RenderUtils.IOutBoxDrawable){
            RenderUtils.removeRender((RenderUtils.IOutBoxDrawable)this);
        }
    }

    @Override
    public void invalidate() {
        if (world.isRemote){
            removeIO();
        }
        super.invalidate();
    }

    @Override
    public void onLoad() {
        
    }
    @Override
    public int getBlockMetadata() {
        return getMetadata();
    }

    public boolean openGui(EntityPlayer player) {
        if(this instanceof IGuiProvider){
            player.openGui(RotationCraft.MOD_ID, GuiHandler.BLOCK_GUI, world, pos.getX(), pos.getY(), pos.getZ());
            return true;
        }
        return false;
    }

    public void rotateBlock(EntityPlayer player){
        int facing = getFacing().ordinal();
        facing++;
        if(facing>1 && !allowHorizonalFacing()){
            facing = 0;
        }
        if(facing>=EnumFacing.VALUES.length){
            if(allowVerticalFacing()){
                facing = 0;
            }else{
                facing = 2;
            }
        }
        setFacing(EnumFacing.VALUES[facing]);
    }
    public boolean allowVerticalFacing(){
        return false;
    }
    public boolean allowHorizonalFacing(){
        return true;
    }

    public boolean canPlayerAccess(EntityPlayer player){
        return true;
    }

}