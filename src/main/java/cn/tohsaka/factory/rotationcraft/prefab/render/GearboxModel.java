package cn.tohsaka.factory.rotationcraft.prefab.render;

import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class GearboxModel extends ModelBase {
    protected ModelRenderer inputshaft;
    protected ModelRenderer outputshaft;
    public GearboxModel(){
        textureWidth = 128;
        textureHeight = 128;


        inputshaft = new ModelRenderer(this,0,0);
        inputshaft.addBox(-1,-1,-9,2,2,4);//mini axis
        inputshaft.setRotationPoint(0F,0F,0);


        outputshaft = new ModelRenderer(this,0,0);
        outputshaft.addBox(-1,-1,5,2,2,4);//mini axis
        outputshaft.setRotationPoint(0F,0F,0);
    }
    public void render(float size, TileGearbox te){
        for(float i=0;i<=45;i+=45){
            setRotation(inputshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-te.getInputAngle() + i)));
            inputshaft.render(size);
            setRotation(outputshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle((-te.getOutputAngle()) + i)));
            outputshaft.render(size);
        }
    }

    public float getAngle(float angle){
        return 0.0349066F + getRotation(getAbsoluteAngle((angle)));
    }
    public float getRotation(double angle) {
        return ((float) angle / (float) 180) * (float) Math.PI;
    }

    public double getAbsoluteAngle(double angle) {
        return angle % 360;
    }

    protected void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}
