package cn.tohsaka.factory.rotationcraft.prefab.block;

import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockMachineBase extends BlockBase{
    public BlockMachineBase() {
        super(Material.ROCK);
    }

    private static PropertyBool active = PropertyBool.create("active");
    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[] { FACING,active });
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        TileEntity tileEntity = worldIn.getTileEntity(pos);
        if(tileEntity==null){
            return state;
        }
        state = state.withProperty(active,((TileMachineBase)tileEntity).isActive());
        state = state.withProperty(FACING,((TileMachineBase) tileEntity).getFacing());
        return state;
    }

    @Override
    public void setDefaultState() {
        IBlockState ib = getBlockState().getBaseState();
        ib = ib.withProperty(FACING, EnumFacing.NORTH);
        ib = ib.withProperty(active, false);
        setDefaultState(ib);
    }

    public static void  setActive(BlockPos pos, World world, boolean isactive){
        IBlockState bs = world.getBlockState(pos);
        if(bs.getProperties().containsKey(active)){
            world.setBlockState(pos,bs.withProperty(active,isactive));
            world.neighborChanged(pos,bs.getBlock(),pos);
        }
    }

    @Override
    public String getUnlocalizedName() {
        return super.getUnlocalizedName();
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

}
