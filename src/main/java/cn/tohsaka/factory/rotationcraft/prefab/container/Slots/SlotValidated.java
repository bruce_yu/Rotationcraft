package cn.tohsaka.factory.rotationcraft.prefab.container.Slots;

import cn.tohsaka.factory.rotationcraft.prefab.tile.TileInvMachine;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotValidated extends Slot {

    ISlotValidator validator;

    public SlotValidated(ISlotValidator validator, IInventory inventory, int index, int x, int y) {

        super(inventory, index, x, y);
        this.validator = validator;
    }
    public SlotValidated(IInventory inventory, int index, int x, int y) {

        super(inventory, index, x, y);
        this.validator = null;
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        if(validator == null){
            return inventory.isItemValidForSlot(this.getSlotIndex(),stack);
        }
        return validator.isItemValid(stack);
    }


    public interface ISlotValidator {

        /**
         * Essentially a passthrough so an arbitrary criterion can be checked against.
         */
        boolean isItemValid(ItemStack stack);

    }
}
