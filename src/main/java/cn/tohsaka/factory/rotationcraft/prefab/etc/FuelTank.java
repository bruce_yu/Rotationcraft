package cn.tohsaka.factory.rotationcraft.prefab.etc;

import net.minecraft.nbt.NBTTagCompound;

public class FuelTank {
    private int maxCapacity;
    private int fuel;
    private String fuelName;
    private String customName = "";
    public FuelTank(int cap,String name){
        maxCapacity = cap;
        fuelName = name;
        fuel = 0;
    }
    public FuelTank setCustomName(String n){
        customName = n;
        return this;
    }
    public String getCustomName(){
        return customName;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }
    public int inc(int i){
        fuel = Math.min(maxCapacity,fuel+i);
        return fuel;
    }
    public int dec(int i){
        fuel = Math.max(fuel-i,0);
        return fuel;
    }
    public void readFromNBT(NBTTagCompound tag){
        fuel = 0;
        if(tag.hasKey("fuel_"+fuelName)){
            fuel = tag.getInteger("fuel_"+fuelName);
        }
    }
    public NBTTagCompound writeToNBT(NBTTagCompound tagCompound){
        tagCompound.setInteger("fuel_"+fuelName,fuel);
        return tagCompound;
    }
    public NBTTagCompound writeToNBT(){
        return writeToNBT(new NBTTagCompound());
    }
    public int getFreeSpace(){
        return maxCapacity - fuel;
    }
    public boolean isEmpty(){
        return fuel == 0 ;
    }
    public int getFuel(){
        return fuel;
    }
    public void setFuel(int f){
        fuel = f;
    }
}
