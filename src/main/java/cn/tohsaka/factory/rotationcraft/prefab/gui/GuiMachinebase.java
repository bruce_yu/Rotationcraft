package cn.tohsaka.factory.rotationcraft.prefab.gui;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotFalseCopy;
import cn.tohsaka.factory.rotationcraft.prefab.sound.SoundBase;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fluids.FluidStack;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GuiMachinebase extends GuiContainer {
    protected ArrayList<ElementBase> elements = new ArrayList<>();
    protected ResourceLocation texture;
    protected ResourceLocation tabTex;
    protected String NAME;
    protected boolean drawInventory = false;
    public GuiMachinebase(Container inventorySlotsIn, ResourceLocation tex) {
        super(inventorySlotsIn);
        texture = tex;
        tabTex = new ResourceLocation("rc","textures/gui/tab.png");
        if(inventorySlotsIn instanceof ContainerCore && ((ContainerCore)inventorySlotsIn).tile instanceof TileRotation){
            machine = ((TileRotation) ((ContainerCore) inventorySlotsIn).tile);
            if(machine.getMinimumPower().getWatt()<=0){
                showtab = false;
            }
        }
    }
    public boolean showtab = true;

    @Override
    public void initGui() {

        super.initGui();
        elements.clear();
    }
    protected TileRotation machine;

    public ElementBase addElement(ElementBase element) {

        elements.add(element);
        return element;
    }

    public void drawSizedModalRect(int x1, int y1, int x2, int y2, int color) {

        int temp;

        if (x1 < x2) {
            temp = x1;
            x1 = x2;
            x2 = temp;
        }
        if (y1 < y2) {
            temp = y1;
            y1 = y2;
            y2 = temp;
        }

        float a = (color >> 24 & 255) / 255.0F;
        float r = (color >> 16 & 255) / 255.0F;
        float g = (color >> 8 & 255) / 255.0F;
        float b = (color & 255) / 255.0F;
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        GlStateManager.color(r, g, b, a);

        BufferBuilder buffer = Tessellator.getInstance().getBuffer();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION);
        buffer.pos(x1, y2, this.zLevel).endVertex();
        buffer.pos(x2, y2, this.zLevel).endVertex();
        buffer.pos(x2, y1, this.zLevel).endVertex();
        buffer.pos(x1, y1, this.zLevel).endVertex();
        Tessellator.getInstance().draw();
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
    }

    public void drawSizedTexturedModalRect(int x, int y, int u, int v, int width, int height, float texW, float texH) {

        float texU = 1 / texW;
        float texV = 1 / texH;
        BufferBuilder buffer = Tessellator.getInstance().getBuffer();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        buffer.pos(x, y + height, this.zLevel).tex((u) * texU, (v + height) * texV).endVertex();
        buffer.pos(x + width, y + height, this.zLevel).tex((u + width) * texU, (v + height) * texV).endVertex();
        buffer.pos(x + width, y, this.zLevel).tex((u + width) * texU, (v) * texV).endVertex();
        buffer.pos(x, y, this.zLevel).tex((u) * texU, (v) * texV).endVertex();
        Tessellator.getInstance().draw();
    }

    public void drawScaledTexturedModelRectFromIcon(int x, int y, TextureAtlasSprite icon, int width, int height) {

        if (icon == null) {
            return;
        }
        double minU = icon.getMinU();
        double maxU = icon.getMaxU();
        double minV = icon.getMinV();
        double maxV = icon.getMaxV();

        BufferBuilder buffer = Tessellator.getInstance().getBuffer();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        buffer.pos(x, y + height, this.zLevel).tex(minU, minV + (maxV - minV) * height / 16F).endVertex();
        buffer.pos(x + width, y + height, this.zLevel).tex(minU + (maxU - minU) * width / 16F, minV + (maxV - minV) * height / 16F).endVertex();
        buffer.pos(x + width, y, this.zLevel).tex(minU + (maxU - minU) * width / 16F, minV).endVertex();
        buffer.pos(x, y, this.zLevel).tex(minU, minV).endVertex();
        Tessellator.getInstance().draw();
    }

    protected int mouseX = 0;
    protected int mouseY = 0;

    protected void updateElementInformation(){

    }
    protected List<String> tooltip = new LinkedList<>();
    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int x, int y) {
        this.updateElementInformation();
        this.drawDefaultBackground();
        GlStateManager.color(1, 1, 1, 1);
        mc.renderEngine.bindTexture(texture);

        if (xSize > 256 || ySize > 256) {
            drawSizedTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize, 512, 512);
        } else {
            drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
        }
        mouseX = x - guiLeft;
        mouseY = y - guiTop;

        GlStateManager.pushMatrix();
        GlStateManager.translate(guiLeft, guiTop, 0.0F);
        drawElements(partialTicks, false);

        //draw tab
        if(machine != null && showtab){
            mc.renderEngine.bindTexture(tabTex);
            GlStateManager.translate(-43, 8, 0.0F);
            drawTexturedModalRect(0, 0, 0, 0, 43, 150);
            drawTabElements();
        }

        GlStateManager.popMatrix();
    }

    public void drawTabElements(){
        IPowerMachine cap = CapabilityRotaryPower.getCapability(machine.getWorld(),machine.getPos(),null);
        if(cap!=null){
            RotaryPower currentPower = cap.getPower();
            RotaryPower minimumPower = machine.getMinimumPower();
            drawPowerBar(StringHelper.localize("info.rc.waila.names.power"),10,(float) (currentPower.getWatt() / minimumPower.getWatt()));
            drawPowerBar(StringHelper.localize("info.rc.waila.names.torque"),38,((float) currentPower.getTorque()) / ((float) minimumPower.getTorque()));
            drawPowerBar(StringHelper.localize("info.rc.waila.names.speed"),66,((float) currentPower.getSpeed()) / ((float) minimumPower.getSpeed()));
        }

    }

    public void addTooltips(List<String> tooltip) {

        ElementBase element = getElementAtPosition(mouseX, mouseY);

        if (element != null && element.isVisible()) {
            element.addTooltip(tooltip);
        }else{
            if(machine!=null && mouseX>=-33 && mouseX <-6){
                IPowerMachine cap = CapabilityRotaryPower.getCapability(machine.getWorld(),machine.getPos(),null);
                if(cap!=null){
                    RotaryPower currentPower = cap.getPower();
                    RotaryPower minimumPower = machine.getMinimumPower();
                    if(mouseY>=30 && mouseY <35){
                        tooltip.add(StringHelper.localize("info.rc.need")+StringHelper.localize("info.rc.waila.names.power")+": "+minimumPower.getFormattedWatt());
                        tooltip.add(StringHelper.localize("info.rc.curr")+StringHelper.localize("info.rc.waila.names.power")+": "+currentPower.getFormattedWatt());

                    }

                    if(mouseY>=58 && mouseY <63){
                        tooltip.add(StringHelper.localize("info.rc.need")+StringHelper.localize("info.rc.waila.names.torque")+": "+minimumPower.getFormattedNm());
                        tooltip.add(StringHelper.localize("info.rc.curr")+StringHelper.localize("info.rc.waila.names.torque")+": "+currentPower.getFormattedNm());
                    }

                    if(mouseY>=86 && mouseY <91){
                        tooltip.add(StringHelper.localize("info.rc.need")+StringHelper.localize("info.rc.waila.names.speed")+": "+minimumPower.getFormattedSpeed());
                        tooltip.add(StringHelper.localize("info.rc.curr")+StringHelper.localize("info.rc.waila.names.speed")+": "+currentPower.getFormattedSpeed());
                    }
                }
            }
        }
    }

    public void drawPowerBar(String text,int y,float scale){
        fontRenderer.drawString(text,9,y,0);
        drawPowerbarAt(10,12+y,Math.min(1F,scale));
    }
    public void drawPowerbarAt(int x,int y,float scale){
        GlStateManager.color(255, 255, 255, 1);
        mc.renderEngine.bindTexture(tabTex);
        drawTexturedModalRect(x, y, 82, 0, 27, 6);
        drawTexturedModalRect(x, y, 82, 7, Math.round(1+(scale*25F)), 6);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {

        if (NAME != null) {
            fontRenderer.drawString(StringHelper.localize(NAME), getCenteredOffset(StringHelper.localize(NAME)), 5, 0x404040);
        }
        if (drawInventory) {
            fontRenderer.drawString(StringHelper.localize("container.inventory"), 8, ySize - 96 + 3, 0x404040);
        }
        drawElements(0, true);
        this.renderHoveredToolTip(x-guiLeft,y-guiTop);
        addTooltips(tooltip);
        drawTooltip(tooltip);
    }

    protected void drawElements(float partialTick, boolean foreground) {

        if (foreground) {
            for (ElementBase element : elements) {
                if (element.isVisible()) {
                    element.drawForeground(mouseX, mouseY);
                }
            }
        } else {
            for (ElementBase element : elements) {
                if (element.isVisible()) {
                    element.drawBackground(mouseX, mouseY, partialTick);
                }
            }
        }
    }

    public FontRenderer getFontRenderer() {

        return fontRenderer;
    }

    protected int getCenteredOffset(String string) {

        return this.getCenteredOffset(string, xSize / 2);
    }

    protected int getCenteredOffset(String string, int xPos) {

        return ((xPos * 2) - fontRenderer.getStringWidth(string)) / 2;
    }

    public int getMouseX() {

        return mouseX;
    }

    public int getMouseY() {

        return mouseY;
    }

    public void drawFluid(int x, int y, FluidStack fluid, int width, int height) {

        if (fluid == null) {
            return;
        }
        GL11.glPushMatrix();
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        RenderHelper2.setBlockTextureSheet();
        int color = fluid.getFluid().getColor(fluid);
        RenderHelper2.setGLColorFromInt(color);
        drawTiledTexture(x, y, RenderHelper2.getTexture(fluid.getFluid().getStill(fluid)), width, height);
        GL11.glPopMatrix();
    }

    public void drawTiledTexture(int x, int y, TextureAtlasSprite icon, int width, int height) {

        int i;
        int j;

        int drawHeight;
        int drawWidth;

        for (i = 0; i < width; i += 16) {
            for (j = 0; j < height; j += 16) {
                drawWidth = Math.min(width - i, 16);
                drawHeight = Math.min(height - j, 16);
                drawScaledTexturedModelRectFromIcon(x + i, y + j, icon, drawWidth, drawHeight);
            }
        }
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }

    public void drawIcon(TextureAtlasSprite icon, int x, int y) {

        RenderHelper2.setBlockTextureSheet();
        GlStateManager.color(1, 1, 1, 1);
        drawTexturedModalRect(x, y, icon, 16, 16);
    }

    public void drawColorIcon(TextureAtlasSprite icon, int x, int y) {

        drawTexturedModalRect(x, y, icon, 16, 16);
    }

    public void drawSizedRect(int x1, int y1, int x2, int y2, int color) {

        int temp;

        if (x1 < x2) {
            temp = x1;
            x1 = x2;
            x2 = temp;
        }
        if (y1 < y2) {
            temp = y1;
            y1 = y2;
            y2 = temp;
        }

        float a = (color >> 24 & 255) / 255.0F;
        float r = (color >> 16 & 255) / 255.0F;
        float g = (color >> 8 & 255) / 255.0F;
        float b = (color & 255) / 255.0F;
        GlStateManager.disableTexture2D();
        GlStateManager.color(r, g, b, a);

        BufferBuilder buffer = Tessellator.getInstance().getBuffer();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION);
        buffer.pos(x1, y2, this.zLevel).endVertex();
        buffer.pos(x2, y2, this.zLevel).endVertex();
        buffer.pos(x2, y1, this.zLevel).endVertex();
        buffer.pos(x1, y1, this.zLevel).endVertex();
        Tessellator.getInstance().draw();
        GlStateManager.enableTexture2D();
    }

    public void bindTexture(ResourceLocation texture) {

        mc.renderEngine.bindTexture(texture);
    }

    public static final float CLICK_VOLUME = 0.3F;
    public static void playClickSound(float pitch) {
        try {
            Minecraft.getMinecraft().getSoundHandler().playSound(new SoundBase(SoundEvents.UI_BUTTON_CLICK, SoundCategory.MASTER, CLICK_VOLUME, pitch));
        }catch (Exception ex){

            ex.printStackTrace();
        }
    }
    public void handleElementButtonClick(String buttonName, int mouseButton) {

    }

    public void handleIDButtonClick(int id, int mouseButton) {

    }






    @Override
    protected void mouseClicked(int mX, int mY, int mouseButton) throws IOException {

        mX -= guiLeft;
        mY -= guiTop;

        for (int i = elements.size(); i-- > 0; ) {
            ElementBase c = elements.get(i);
            if (!c.isVisible() || !c.isEnabled() || !c.intersectsWith(mX, mY)) {
                continue;
            }
            if (c.onMousePressed(mX, mY, mouseButton)) {
                return;
            }
        }

        mX += guiLeft;
        mY += guiTop;

        super.mouseClicked(mX, mY, mouseButton);

    }

    @Override
    protected void mouseReleased(int mX, int mY, int mouseButton) {

        mX -= guiLeft;
        mY -= guiTop;

        if (mouseButton >= 0 && mouseButton <= 2) { // 0:left, 1:right, 2: middle
            for (int i = elements.size(); i-- > 0; ) {
                ElementBase c = elements.get(i);
                if (!c.isVisible() || !c.isEnabled()) { // no bounds checking on mouseUp events
                    continue;
                }
                c.onMouseReleased(mX, mY);
            }
        }
        mX += guiLeft;
        mY += guiTop;

        super.mouseReleased(mX, mY, mouseButton);
    }

    protected int lastIndex = -1;


    @Override
    protected void mouseClickMove(int mX, int mY, int lastClick, long timeSinceClick) {

        Slot slot = getSlotAtPosition(mX, mY);
        ItemStack itemstack = this.mc.player.inventory.getItemStack();

        if (this.dragSplitting && slot != null && !itemstack.isEmpty() && slot instanceof SlotFalseCopy) {
            if (lastIndex != slot.slotNumber) {
                lastIndex = slot.slotNumber;
                this.handleMouseClick(slot, slot.slotNumber, 0, ClickType.PICKUP);
            }
        } else {
            lastIndex = -1;
            super.mouseClickMove(mX, mY, lastClick, timeSinceClick);
        }
    }

    public Slot getSlotAtPosition(int xCoord, int yCoord) {

        for (int k = 0; k < this.inventorySlots.inventorySlots.size(); ++k) {
            Slot slot = this.inventorySlots.inventorySlots.get(k);

            if (this.isMouseOverSlot(slot, xCoord, yCoord)) {
                return slot;
            }
        }
        return null;
    }

    public boolean isMouseOverSlot(Slot theSlot, int xCoord, int yCoord) {
        return this.isPointInRegion(theSlot.xPos, theSlot.yPos, 16, 16, xCoord, yCoord);
    }

    @Override
    protected void keyTyped(char characterTyped, int keyPressed) throws IOException {

        for (int i = elements.size(); i-- > 0; ) {
            ElementBase c = elements.get(i);
            if (!c.isVisible() || !c.isEnabled()) {
                continue;
            }
            if (c.onKeyTyped(characterTyped, keyPressed)) {
                return;
            }
        }
        super.keyTyped(characterTyped, keyPressed);
    }

    @Override
    public void handleMouseInput() throws IOException {

        int x = Mouse.getEventX() * width / mc.displayWidth;
        int y = height - Mouse.getEventY() * height / mc.displayHeight - 1;

        mouseX = x - guiLeft;
        mouseY = y - guiTop;

        int wheelMovement = Mouse.getEventDWheel();

        if (wheelMovement != 0) {
            for (int i = elements.size(); i-- > 0; ) {
                ElementBase c = elements.get(i);
                if (!c.isVisible() || !c.isEnabled() || !c.intersectsWith(mouseX, mouseY)) {
                    continue;
                }
                if (c.onMouseWheel(mouseX, mouseY, wheelMovement)) {
                    return;
                }
            }


            if (onMouseWheel(mouseX, mouseY, wheelMovement)) {
                return;
            }
        }
        super.handleMouseInput();
    }

    protected boolean onMouseWheel(int mouseX, int mouseY, int wheelMovement) {

        return false;
    }


    protected ElementBase getElementAtPosition(int mX, int mY) {

        for (int i = elements.size(); i-- > 0; ) {
            ElementBase element = elements.get(i);
            if (element.intersectsWith(mX, mY)) {
                return element;
            }
        }
        return null;
    }
    public void drawTooltip(List<String> list) {
        drawTooltipHoveringText(list, mouseX , mouseY , fontRenderer);
        tooltip.clear();
    }
    protected void drawTooltipHoveringText(List<String> list, int x, int y, FontRenderer font) {

        if (list == null || list.isEmpty()) {
            return;
        }
        GlStateManager.disableRescaleNormal();
        GlStateManager.disableLighting();
        GlStateManager.disableDepth();
        int k = 0;

        for (String s : list) {
            int l = font.getStringWidth(s);

            if (l > k) {
                k = l;
            }
        }
        int i1 = x + 12;
        int j1 = y - 12;
        int k1 = 8;

        if (list.size() > 1) {
            k1 += 2 + (list.size() - 1) * 10;
        }
        if (i1 + k > this.width) {
            i1 -= 28 + k;
        }
        if (j1 + k1 + 6 > this.height) {
            j1 = this.height - k1 - 6;
        }
        this.zLevel = 300.0F;
        itemRender.zLevel = 300.0F;
        int l1 = -267386864;
        this.drawGradientRect(i1 - 3, j1 - 4, i1 + k + 3, j1 - 3, l1, l1);
        this.drawGradientRect(i1 - 3, j1 + k1 + 3, i1 + k + 3, j1 + k1 + 4, l1, l1);
        this.drawGradientRect(i1 - 3, j1 - 3, i1 + k + 3, j1 + k1 + 3, l1, l1);
        this.drawGradientRect(i1 - 4, j1 - 3, i1 - 3, j1 + k1 + 3, l1, l1);
        this.drawGradientRect(i1 + k + 3, j1 - 3, i1 + k + 4, j1 + k1 + 3, l1, l1);
        int i2 = 1347420415;
        int j2 = (i2 & 16711422) >> 1 | i2 & -16777216;
        this.drawGradientRect(i1 - 3, j1 - 3 + 1, i1 - 3 + 1, j1 + k1 + 3 - 1, i2, j2);
        this.drawGradientRect(i1 + k + 2, j1 - 3 + 1, i1 + k + 3, j1 + k1 + 3 - 1, i2, j2);
        this.drawGradientRect(i1 - 3, j1 - 3, i1 + k + 3, j1 - 3 + 1, i2, i2);
        this.drawGradientRect(i1 - 3, j1 + k1 + 2, i1 + k + 3, j1 + k1 + 3, j2, j2);

        for (int k2 = 0; k2 < list.size(); ++k2) {
            String s1 = list.get(k2);
            font.drawStringWithShadow(s1, i1, j1, -1);

            if (k2 == 0) {
                j1 += 2;
            }
            j1 += 10;
        }
        this.zLevel = 0.0F;
        itemRender.zLevel = 0.0F;
        GlStateManager.enableLighting();
        GlStateManager.enableDepth();
        GlStateManager.enableRescaleNormal();
    }
}
