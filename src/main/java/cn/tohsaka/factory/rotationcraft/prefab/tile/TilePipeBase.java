package cn.tohsaka.factory.rotationcraft.prefab.tile;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.fluids.Fluid;

public class TilePipeBase extends TileEntity {
    public EnumFacing[] getConnections(){
        return new EnumFacing[]{EnumFacing.NORTH,EnumFacing.SOUTH};
    }
    public Fluid getFluidType(){
        return null;
    }

    public boolean isConnectionValidForSide(EnumFacing side){
        return true;
    }
}
