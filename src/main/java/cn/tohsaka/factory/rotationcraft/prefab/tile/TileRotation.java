package cn.tohsaka.factory.rotationcraft.prefab.tile;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerMachine;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.api.thermal.IThermalMachine;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.utils.*;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;
import java.util.List;

public abstract class TileRotation extends TileInvMachine implements IThermalMachine, ITickable, IMeterable {
    public float temp = 0F;
    public int processMax=0;
    public int processRem=0;
    public boolean somthingchanged = false;
    public IPowerMachine powerMachine;
    public TileRotation(){
        super();
        powerMachine = new RotaryPowerMachine(this){

            @Nullable
            @Override
            public World getWorld() {
                return TileRotation.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return TileRotation.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return null;
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,FacingTool.getRelativePos(pos, facing, position,1),world,FacingTool.getRelativeSide(position,facing).getOpposite());
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return null;
            }

            @Override
            public void markDirty() {
                super.markDirty();
                onPowerChanged();
            }
        };
    }
    public TileRotation(FacingTool.Position position){
        this();
        this.position = position;
    }
    private FacingTool.Position position = FacingTool.Position.BACK;

    public BlockPos getSrc() {
        return FacingTool.getRelativePos(pos, facing, position,1);
    }


    @Override
    public void update() {
        if(!world.isRemote){
            updatePowerInput();
            if(world.getTotalWorldTime() % 40 == 0) {
                addTemp(TempetureUtils.diffBlockTemp(temp, world, pos));
                if (somthingchanged) {
                    NetworkDispatcher.dispatchMachineUpdate(this);
                    somthingchanged = false;
                }
            }
            if(this.canStart() && powerMachine.getPower().getTorque()>=this.getMinimumPower().getTorque() && this.powerMachine.getPower().getWatt()>=this.getMinimumPower().getWatt()){
                if(!isActive){
                    isActive = processStart();
                }else {
                    if(processRem<=0){
                        processEnd();
                        processStop();
                    }else{
                        processTick();
                    }
                }
            }else{
                processStop();
            }
        }else{
            angle = AxlsUtils.calcAngle(angle,powerMachine.getPower().getSpeed());
        }
    }
    protected void onPowerChanged(){

    }

    protected void updatePowerInput(){
        updatePowerInput(position);
    }
    protected void updatePowerInput(FacingTool.Position position){
        IPowerMachine cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, position,1),FacingTool.revert(FacingTool.getRelativeSide(position,facing)));
        if(cap!=null){
            RotaryPower powerIn = cap.getPower();
            powerMachine.setPower(powerIn);
        }else{
            powerMachine.setPower(RotaryPower.ZERO);
        }
    }

    @Override
    public void onLoad() {
        super.onLoad();
        setTemp(TempetureUtils.getBlockRelativeTemp(world,pos));
    }

    public void setSomthingchanged(){
        somthingchanged = true;
    }

    @Override
    public boolean canDamage() {
        return false;
    }

    @Override
    public float getDamagedTemp() {
        return 0;
    }

    @Override
    public void onDamage() {

    }

    @Override
    public float getTemp() {
        return temp;
    }
    @Override
    public void setTemp(Float thermal) {
        temp = thermal;
    }

    @Override
    public void addTemp(Float thermal) {
        temp += thermal;
        if(thermal>0){
            somthingchanged = true;
        }
    }

    @Override
    public boolean isTempSource() {
        return false;
    }

    @Override
    public boolean allowVerticalFacing() {
        return super.allowVerticalFacing();
    }


    public boolean isRedStonePowered(){
        return world.isBlockPowered(pos);
    }






    public abstract RotaryPower getMinimumPower();


    public boolean canStart(){
        return false;
    }
    public boolean processStart(){
        return false;
    }
    public void processTick(){

    }
    public void processEnd(){

    }
    public void processStop(){
        isActive = false;
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {
        super.writeGuiData(packet, isFullSync);
        packet.writeNBTTagCompound(writeGuiTagCompound(new NBTTagCompound()));
    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {
        super.readGuiData(packet, isFullSync);
        readGuiTagCompound(packet.readNBTTagCompound());
    }

    public NBTTagCompound writeGuiTagCompound(NBTTagCompound tag){
        tag.setInteger("processMax",processMax);
        tag.setInteger("processRem",processRem);
        tag.setBoolean("isActive",isActive);
        return tag;
    }
    public void readGuiTagCompound(NBTTagCompound tag){
        processMax = tag.getInteger("processMax");
        processRem = tag.getInteger("processRem");
        isActive = tag.getBoolean("isActive");
    }

    public int getScaledProgress(int scale)
    {
        if (!isActive || processMax <= 0 || processRem <= 0)
            return 0;

        return scale * (processMax - processRem) / processMax;
    }

    public int getProcessTickMinus(){
        return 1;
    }
    
    public int calcProcessTime(float basetime){
        RotaryPower minimumpower = getMinimumPower();
        long ratio = Math.abs(powerMachine.getPower().getSpeed() - minimumpower.getSpeed()) / minimumpower.getSpeed();
        float time = basetime - ((basetime/100)*ratio);
        return Math.max(0,Math.round(time));
    }

    public int calcProcessTime(float basetime,RotaryPower basePower){
        long ratio = Math.abs(powerMachine.getPower().getSpeed() - basePower.getSpeed()) / basePower.getSpeed();
        float time = basetime - basetime * ratio;
        return Math.max(0,Math.round(time));
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        if(this.powerMachine.getPower().getTorque()>=this.getMinimumPower().getTorque() && this.powerMachine.getPower().getWatt()>=this.getMinimumPower().getWatt()){
            tooltip.add(StringHelper.localize("info.rc.waila.names.processtime")+":"+Math.max(processMax / 20 / getProcessTickMinus(),0.05)+" "+StringHelper.localize("info.rc.waila.names.seconds"));
        }
    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {

        drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos,facing, position,1),1F));
        super.getOutBoxList(drawables);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("temp")){
            temp = compound.getFloat("temp");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setFloat("temp",temp);
        return compound;
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null){
                return true;
            }
            if(FacingTool.getRelativeSide(position,facing) == side){
                return true;
            }


            return false;
        }
        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerMachine);
            }
            if(FacingTool.getRelativeSide(position,facing) == side){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerMachine);
            }
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append(powerMachine.getPower());
        return sb.toString();
    }

    @Override
    public void onMeter() {
        addIO();
    }
}
