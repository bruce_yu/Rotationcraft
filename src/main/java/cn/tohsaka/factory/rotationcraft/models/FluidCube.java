package cn.tohsaka.factory.rotationcraft.models;


import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import org.lwjgl.opengl.GL11;

public class FluidCube {
	public static void renderFluid(float maxHeight, Fluid renderFluid, double x, double y, double z)
	{
		maxHeight = maxHeight * 0.875F;
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, z);
		RenderHelper.disableStandardItemLighting();

		GlStateManager.disableRescaleNormal();
		GlStateManager.disableBlend();

		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder buffer = tessellator.getBuffer();

		TextureAtlasSprite fluid = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(renderFluid.getStill().toString());
		fluid = fluid == null ? Minecraft.getMinecraft().getTextureMapBlocks().getMissingSprite() : fluid;

		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);

		final int rgbaColor = renderFluid.getColor();
		final int rColor = rgbaColor >> 16 & 0xFF;
		final int gColor = rgbaColor >> 8 & 0xFF;
		final int bColor = rgbaColor & 0xFF;
		final int aColor = rgbaColor >> 24 & 0xFF;
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GlStateManager.color(rColor, gColor, bColor, aColor);

		float u1 = fluid.getMinU();
		float v1 = fluid.getMinV();
		float u2 = fluid.getMaxU();
		float v2 = fluid.getMaxV();

		if (maxHeight > 0) {
			float texWidth = u2 - u1;

			// TOP
			buffer.pos(0.25, maxHeight + 0.05, 0.25).tex(u1 + 0.75 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, maxHeight + 0.05, 0.75).tex(u1 + 0.75 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.75, maxHeight + 0.05, 0.75).tex(u1 + 0.25 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.75, maxHeight + 0.05, 0.25).tex(u1 + 0.25 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();

			// NORTH
			buffer.pos(0.75, maxHeight + 0.05, 0.25).tex(u1 + 0.75 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.75, 0, 0.25).tex(u1 + 0.75 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, 0, 0.25).tex(u1 + 0.25 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, maxHeight + 0.05, 0.25).tex(u1 + 0.25 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();

			// EAST
			buffer.pos(0.25, 0, 0.75).tex(u1 + 0.75 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, maxHeight + 0.05, 0.75).tex(u1 + 0.75 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, maxHeight + 0.05, 0.25).tex(u1 + 0.25 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, 0, 0.25).tex(u1 + 0.25 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();

			// SOUTH
			buffer.pos(0.75, 0, 0.75).tex(u1 + 0.75 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.75, maxHeight + 0.05, 0.75).tex(u1 + 0.75 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, maxHeight + 0.05, 0.75).tex(u1 + 0.25 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.25, 0, 0.75).tex(u1 + 0.25 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();

			// WEST
			buffer.pos(0.75, maxHeight + 0.05, 0.75).tex(u1 + 0.75 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.75, 0, 0.75).tex(u1 + 0.75 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.75, 0, 0.25).tex(u1 + 0.25 * texWidth, v1).color(rColor, gColor, bColor, aColor).endVertex();
			buffer.pos(0.75, maxHeight + 0.05, 0.25).tex(u1 + 0.25 * texWidth, v1 + (maxHeight + 0.05) * texWidth).color(rColor, gColor, bColor, aColor).endVertex();
		}

		tessellator.draw();

		RenderHelper.enableStandardItemLighting();
		GlStateManager.popMatrix();
	}
}