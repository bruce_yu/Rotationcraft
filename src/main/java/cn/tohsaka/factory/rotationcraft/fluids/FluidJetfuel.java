package cn.tohsaka.factory.rotationcraft.fluids;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

import java.awt.*;

@GameInitializer
public class FluidJetfuel extends Fluid {
    public FluidJetfuel(String fluidName, ResourceLocation still, ResourceLocation flowing, Color color) {
        super(fluidName, still, flowing, color);
        setLuminosity(8);
        setTemperature(30);
        setBlock(null);
        FluidRegistry.registerFluid(this);
    }

    public static ResourceLocation still = new ResourceLocation(RotationCraft.MOD_ID,"fluid/jetfuel_still");
    public static ResourceLocation flow = new ResourceLocation(RotationCraft.MOD_ID,"fluid/jetfuel_flow");

    public static void init(){
        FluidJetfuel jetfuel = new FluidJetfuel("jetfuel",still,flow,new Color(0xCC3333));
        FluidRegistry.addBucketForFluid(jetfuel);
        RotationCraft.fluids.put("jetfuel",jetfuel);
    }
    @Override
    public Fluid setBlock(Block block) {
        return this;
    }
}
