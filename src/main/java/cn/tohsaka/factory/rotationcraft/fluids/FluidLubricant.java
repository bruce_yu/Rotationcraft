package cn.tohsaka.factory.rotationcraft.fluids;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

import java.awt.*;

@GameInitializer
public class FluidLubricant extends Fluid {
    public FluidLubricant(String fluidName, ResourceLocation still, ResourceLocation flowing, Color color) {
        super(fluidName, still, flowing, color);
        setLuminosity(0);
        setTemperature(30);
        setBlock(null);
        FluidRegistry.registerFluid(this);
    }

    public static ResourceLocation still = new ResourceLocation(RotationCraft.MOD_ID,"fluid/lubricant_still");
    public static ResourceLocation flow = new ResourceLocation(RotationCraft.MOD_ID,"fluid/lubricant_flow");

    public static void init(){
        FluidLubricant lubricant = new FluidLubricant("lubricant",still,flow,new Color(0xE4E18E));
        FluidRegistry.addBucketForFluid(lubricant);

        RotationCraft.fluids.put("lubricant",lubricant);
    }

    @Override
    public Fluid setBlock(Block block) {
        return this;
    }
}
